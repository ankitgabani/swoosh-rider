//
//  LoginViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 01/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SDWebImage

class LoginViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var txtEmailForgot: UITextField!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewCountrySelect: UIView!
    
    @IBOutlet weak var viewEnterPINPass: UIView!
    @IBOutlet weak var txtPIN: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var btnNewPassShow: UIButton!
    
    @IBOutlet weak var viewBGPasswordChanges: UIView!
    @IBOutlet weak var viewPopupChanegs: UIView!
    
    @IBOutlet weak var viewWrongInfo: UIView!
    @IBOutlet weak var viewWrongInfoPopup: UIView!
    
    
 
    var isShowPass = false
    
    var isShowNewPass = false
    
 //   var locationManager = CLLocationManager()
    
    var arrGetAllCountry: [SRGetAllCountryData] = [SRGetAllCountryData]()
    
    var objLat = 0.0
    var objLong = 0.0
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewWrongInfo.isHidden = true
        viewWrongInfoPopup.clipsToBounds = true
        viewWrongInfoPopup.layer.cornerRadius = 5
        
        viewBGPasswordChanges.isHidden = true
        viewPopupChanegs.clipsToBounds = true
        viewPopupChanegs.layer.cornerRadius = 12
        viewPopupChanegs.layer.maskedCorners = [.layerMinXMaxYCorner,. layerMaxXMaxYCorner] // Bottom Corner
        
        viewPopup.isHidden = false
        viewEnterPINPass.isHidden = true
        
        // Old
//        txtPhone.text = "9477589578"
//        txtPassword.text = "Asdfg@123"
        
        //New
//        txtPhone.text = "9051387915"
//        txtPassword.text = "As@12345"
        
        txtPhone.delegate = self
        txtPassword.delegate = self
        txtEmailForgot.delegate = self
        
        viewCountrySelect.isHidden = true
        viewbg.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: .black)
        
//        if (CLLocationManager.locationServicesEnabled())
//        {
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
//        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetALlCountryAPI()
    }

    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtPhone {
            txtPassword.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    //MARK:- Action Method
    
    @IBAction func clickedShowHide(_ sender: Any) {
        
        if isShowPass == true {
            isShowPass = false
            txtPassword.isSecureTextEntry = true
            btnPassword.setImage(UIImage(named: "ic_Hide"), for: .normal)
        }
        else
        {
            isShowPass = true
            txtPassword.isSecureTextEntry = false
            btnPassword.setImage(UIImage(named: "ic_Show"), for: .normal)
        }
    }
    
    @IBAction func clickedForgot(_ sender: Any) {
        
        viewbg.isHidden = false

        viewPopup.isHidden = false
        viewEnterPINPass.isHidden = true

        
        viewPopup.slideIn(from: kFTAnimationBottom, in: viewPopup.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
    }
    
    @IBAction func clickedCLose(_ sender: Any) {
        
        viewPopup.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewbg.isHidden = true
        }
        
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        if txtPhone.text == ""
        {
            self.view.makeToast("Please Enter Registered Phone No")
        }
        else if txtPassword.text == ""
        {
            self.view.makeToast("Enter Password")
        }
        else
        {
            self.view.endEditing(true)
            APIClient.sharedInstance.showIndicator()

            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.callLoginAPI()
            }
            
        }
        
    }
    
    
    @IBAction func clickedSignUp(_ sender: Any) {
    }
    
    @IBAction func clickedSelectedCountry(_ sender: Any) {
        viewCountrySelect.isHidden = false
    }
    
    @IBAction func clickedSendPin(_ sender: Any) {
        
        self.view.endEditing(true)
        
        self.txtPIN.text = ""
        self.txtNewPassword.text = ""
        
        if txtEmailForgot.text != ""
        {
            callForgotPasswordAPI()
            
        }
        else
        {
            self.view.makeToast("please enter email address.")
        }
    }
    
    @IBAction func clickedChangePass(_ sender: Any) {
        
        if txtPIN.text == "" {
            self.view.makeToast("please enter PIN")
            
        }
        else if txtNewPassword.text == "" {
            self.view.makeToast("please enter new password")
            
        }
        else
        {
            callcheckingOtpAPI()
        }
        
        
    }
    
    @IBAction func clickedCloseNewPass(_ sender: Any) {
        
        viewEnterPINPass.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewbg.isHidden = true
        }
        
    }
    
    @IBAction func clickedShowNewPass(_ sender: Any) {
        if isShowNewPass == true {
            isShowNewPass = false
            txtNewPassword.isSecureTextEntry = true
            btnNewPassShow.setImage(UIImage(named: "ic_Hide"), for: .normal)
        }
        else
        {
            isShowNewPass = true
            txtNewPassword.isSecureTextEntry = false
            btnNewPassShow.setImage(UIImage(named: "ic_Show"), for: .normal)
        }
    }
    
    @IBAction func clickedOKPopup(_ sender: Any) {
        viewBGPasswordChanges.isHidden = true
        viewWrongInfo.isHidden = true
    }
    
    
    //MARK:- TableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGetAllCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryListCell") as! CountryListCell
        
        let dicData = arrGetAllCountry[indexPath.row]
        
        let name = dicData.name ?? ""
        let sortname = dicData.sortname ?? ""
        cell.lblName.text = "\(name) (\(sortname))"
        
        let phonecode = dicData.phonecode ?? ""
        cell.lblCode.text = "+\(phonecode)"
        
        var image = dicData.image ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(IMG_BASE_URL)\(image)")
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicData = arrGetAllCountry[indexPath.row]
        
        let sortname = dicData.sortname ?? ""
        let phonecode = dicData.phonecode ?? ""
        
        self.lblCode.text = "(\(sortname)) +\(phonecode)"
        
        var image = dicData.image ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(IMG_BASE_URL)\(image)")
        imgCountry.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgCountry.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        viewCountrySelect.isHidden = true
    }
    
    
    // MARK: - API Call 
    func callLoginAPI() {
        
        let code = lblCode.text!.components(separatedBy: " ")
        let fCode = code[1] as? String
        
        let playerID = UserDefaults.standard.value(forKey: "PlayerID") as? String
        
        var strPhoneEmail = ""
        
        if ((txtPhone.text?.contains("@")) == true) {
            strPhoneEmail = txtPhone.text ?? ""
        }
        else
        {
            strPhoneEmail = "\(fCode ?? "")\(txtPhone.text ?? "")"
        }
        
        let param = ["value": strPhoneEmail,"password": txtPassword.text!,"player_id":playerID ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(LOG_IN, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let dicUser_data = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let dicData = UserLoginData(fromDictionary: dicUser_data!)
                            
                            appDelagte.saveCurrentUserData(dic: dicData)
                            appDelagte.dicLoginUserDetails = dicData
                            
                            UserDefaults.standard.set(true, forKey: "isUserLogin")
                            UserDefaults.standard.synchronize()
                            
                            self.callGetTimeZoneAPI()
                            
                        } else {
                            
                            self.viewWrongInfo.isHidden = false
                            //self.view.makeToast(data_Message)
                        }
                        
                    } else {
                    
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callForgotPasswordAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["email": self.txtEmailForgot.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(FORGOT_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.viewPopup.isHidden = true
                            self.txtEmailForgot.text = ""
                            self.viewEnterPINPass.slideIn(from: kFTAnimationBottom, in: self.viewEnterPINPass.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
                            
                        } else {
                            self.view.makeToast(data)
                        }
                        
                    } else {
                        self.view.makeToast(data)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetTimeZoneAPI() {
        
       // APIClient.sharedInstance.showIndicator()
        
        let urlParam = GET_NEAREST_TIMEZONE_USER + "\(appDelagte.objLatitude)/\(appDelagte.objLongitude)/\(appDelagte.dicLoginUserDetails?.userId ?? "")"
        
        let param = ["":""]
        
        print(urlParam)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(urlParam, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    //   APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            appDelagte.setUpSideMenu()
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetALlCountryAPI() {
        
        //     APIClient.sharedInstance.showIndicator()
        
        let param = ["":""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    //  APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetAllCountry.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetAllCountry.append(list)
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callcheckingOtpAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["email": self.txtEmailForgot.text!, "otp": self.txtPIN.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CHECHING_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data = response?["data"] as? String ?? ""

                if statusCode == 200 {
                    
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.callchangePasswordAPI()
                            
                        } else {
                            APIClient.sharedInstance.hideIndicator()
                            
                            self.view.makeToast(data)
                        }
                        
                    } else {
                        APIClient.sharedInstance.hideIndicator()
                        self.view.makeToast(data)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callchangePasswordAPI() {
        
        let param = ["email": self.txtEmailForgot.text!, "new_password": self.txtNewPassword.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CHANGE_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data = response?["data"] as? String ?? ""

                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.viewPopup.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                                self.viewbg.isHidden = true
                                self.viewBGPasswordChanges.isHidden = false
                            }
                            
                        } else {
                            self.view.makeToast(data)
                        }
                        
                    } else {
                        self.view.makeToast(data)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
