//
//  RegisterViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 01/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage
import DropDown

class RegisterViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewPopUpVeri: UIView!
    @IBOutlet weak var txtCode: UITextField!
    
    
    @IBOutlet weak var viewTblView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var viewTargetState: UIView!
    @IBOutlet weak var viewCity: UIView!
    
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewFirstCont: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtLastNAme: UITextField!
    @IBOutlet weak var viewLast: UIView!
    @IBOutlet weak var viewLastCont: NSLayoutConstraint!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewEmailCont: NSLayoutConstraint!
    
    @IBOutlet weak var imgFalge: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewPhoneCont: NSLayoutConstraint!
    @IBOutlet weak var txtPhone: UITextField!
    
    
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var viewPass: UIView!
    @IBOutlet weak var viewPassCont: NSLayoutConstraint!
    
    @IBOutlet weak var btnPAss: UIButton!
    
    var isShowPass = false
    var arrGetAllCountry: [SRGetAllCountryData] = [SRGetAllCountryData]()
    var arrGetState: [SRGetAllCountryData] = [SRGetAllCountryData]()
    var arrGetCity: [SRGetAllCountryData] = [SRGetAllCountryData]()
    
    
    let dropDownState = DropDown()
    let dropDownCity = DropDown()
    
    var objCountryID = String()
    var objStateID = String()
    var objCityID = String()
    
    
    var strOTP = ""
    var stru_ID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBg.isHidden = true
        
        txtFirstName.delegate = self
        txtLastNAme.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtPass.delegate = self
        setUpStatus(color: .black)
        
        viewTblView.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        callGetALlCountryAPI()
        // Do any additional setup after loading the view.
    }
    
    func setDropDownState() {
        
        let arrState = NSMutableArray()
        
        for objState in arrGetState {
            arrState.add(objState.name!)
        }
        
        dropDownState.dataSource = arrState as! [String]
        dropDownState.anchorView = viewTargetState
        dropDownState.direction = .bottom
        
        dropDownState.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblState.text = item
            
            let dicSate = self.arrGetState[index]
            print(dicSate.id!)
            self.objStateID = dicSate.id
            
            self.callGetCityAPI(stateId: dicSate.id)
        }
        
        dropDownState.bottomOffset = CGPoint(x: 0, y: viewTargetState.bounds.height)
        dropDownState.topOffset = CGPoint(x: 0, y: -viewTargetState.bounds.height)
        dropDownState.dismissMode = .onTap
        dropDownState.textColor = UIColor.darkGray
        dropDownState.backgroundColor = UIColor.white
        dropDownState.selectionBackgroundColor = UIColor.clear
        dropDownState.reloadAllComponents()
    }
    
    func setDropDownCity() {
        
        let arrCity = NSMutableArray()
        
        for objCity in arrGetCity {
            arrCity.add(objCity.name!)
        }
        
        dropDownCity.dataSource = arrCity as! [String]
        dropDownCity.anchorView = viewCity
        dropDownCity.direction = .bottom
        
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblCity.text = item
            
            let dicSate = self.arrGetCity[index]
            print(dicSate.id!)
            self.objCityID = dicSate.id
        }
        
        dropDownCity.bottomOffset = CGPoint(x: 0, y: viewCity.bounds.height)
        dropDownCity.topOffset = CGPoint(x: 0, y: -viewCity.bounds.height)
        dropDownCity.dismissMode = .onTap
        dropDownCity.textColor = UIColor.darkGray
        dropDownCity.backgroundColor = UIColor.white
        dropDownCity.selectionBackgroundColor = UIColor.clear
        dropDownCity.reloadAllComponents()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
        
        if textField == txtFirstName {
            viewFirstName.backgroundColor = COLOR_SWOOSH
            viewFirstCont.constant = 2
            
            viewLast.backgroundColor = UIColor.black
            viewLastCont.constant = 1
            
            viewEmail.backgroundColor = UIColor.black
            viewEmailCont.constant = 1
            
            viewPhone.backgroundColor = UIColor.black
            viewPhoneCont.constant = 1
            
            viewPass.backgroundColor = UIColor.black
            viewPassCont.constant = 1
        }
        else if textField == txtLastNAme {
            viewLast.backgroundColor = COLOR_SWOOSH
            viewLastCont.constant = 2
            
            viewFirstName.backgroundColor = UIColor.black
            viewFirstCont.constant = 1
            
            viewEmail.backgroundColor = UIColor.black
            viewEmailCont.constant = 1
            
            viewPhone.backgroundColor = UIColor.black
            viewPhoneCont.constant = 1
            
            viewPass.backgroundColor = UIColor.black
            viewPassCont.constant = 1
        }
        else if textField == txtEmail {
            viewEmail.backgroundColor = COLOR_SWOOSH
            viewEmailCont.constant = 2
            
            viewFirstName.backgroundColor = UIColor.black
            viewFirstCont.constant = 1
            
            viewLast.backgroundColor = UIColor.black
            viewLastCont.constant = 1
            
            viewPhone.backgroundColor = UIColor.black
            viewPhoneCont.constant = 1
            
            viewPass.backgroundColor = UIColor.black
            viewPassCont.constant = 1
            
        }
        else if textField == txtPhone {
            viewPass.backgroundColor = UIColor.black
            viewPassCont.constant = 1
            
            viewFirstName.backgroundColor = UIColor.black
            viewFirstCont.constant = 1
            
            viewLast.backgroundColor = UIColor.black
            viewLastCont.constant = 1
            
            viewPhone.backgroundColor = UIColor.black
            viewPhoneCont.constant = 1
            
            viewEmail.backgroundColor = UIColor.black
            viewEmailCont.constant = 1
        }
        else if textField == txtPass {
            viewPass.backgroundColor = COLOR_SWOOSH
            viewPassCont.constant = 2
            
            viewFirstName.backgroundColor = UIColor.black
            viewFirstCont.constant = 1
            
            viewLast.backgroundColor = UIColor.black
            viewLastCont.constant = 1
            
            viewPhone.backgroundColor = UIColor.black
            viewPhoneCont.constant = 1
            
            viewEmail.backgroundColor = UIColor.black
            viewEmailCont.constant = 1
        }
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName {
            txtLastNAme.becomeFirstResponder()
            return false
        } else if textField == txtLastNAme {
            txtEmail.becomeFirstResponder()
            return false
        } else if textField == txtEmail {
            txtPhone.becomeFirstResponder()
            return false
        }else if textField == txtPhone {
            txtPass.becomeFirstResponder()
            return false
        }
        
        viewPass.backgroundColor = UIColor.black
        viewPassCont.constant = 1
        
        viewFirstName.backgroundColor = UIColor.black
        viewFirstCont.constant = 1
        
        viewLast.backgroundColor = UIColor.black
        viewLastCont.constant = 1
        
        viewPhone.backgroundColor = UIColor.black
        viewPhoneCont.constant = 1
        
        viewEmail.backgroundColor = UIColor.black
        viewEmailCont.constant = 1
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    
    
    //MARK:- Action Method
    
    @IBAction func clickedSubmit(_ sender: Any) {
        
        if txtCode.text == ""
        {
            self.view.makeToast("Please enter code")
        }
        else
        {
            self.callValidateNumberAPI()
        }
        
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        viewPopUpVeri.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewBg.isHidden = true
        }
    }
    
    
    @IBAction func clickedSelectedCode(_ sender: Any) {
        viewTblView.isHidden = false
    }
    
    @IBAction func clickedPassword(_ sender: Any) {
        if isShowPass == true {
            isShowPass = false
            txtPass.isSecureTextEntry = true
            btnPAss.setImage(UIImage(named: "ic_Hide"), for: .normal)
        }
        else
        {
            isShowPass = true
            txtPass.isSecureTextEntry = false
            btnPAss.setImage(UIImage(named: "ic_Show"), for: .normal)
        }
    }
    
    @IBAction func clickedSignUp(_ sender: Any) {
        
        if txtFirstName.text == ""
        {
            self.view.makeToast("Please Enter Your First Name")
        }
        else if txtLastNAme.text == ""
        {
            self.view.makeToast("Please Enter Your Last Name")
        }
        else if txtEmail.text == ""
        {
            self.view.makeToast("Please Enter Your Email")
        }
        else if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? "")
        {
            self.view.makeToast("Please valid Enter Your Email")
        }
        else if txtPhone.text == ""
        {
            self.view.makeToast("Please Enter Your Phone No")
        }
        else if lblState.text == "" {
            self.view.makeToast("Please Select Your State")
        }
        else if lblCity.text == "" {
            self.view.makeToast("Please Select Your City")
        }
        else if txtPass.text == "" {
            self.view.makeToast("Please Enter Your Password")
        }
        else if !AppUtilites.isPasswordValid(txtPass.text ?? "")
        {
            self.view.makeToast("Please Enter valid Password")
        }
        else
        {
            callSignUPAPI()
        }
    }
    
    @IBAction func clickedTermsUse(_ sender: Any) {
    }
    
    @IBAction func clickedPrivcacy(_ sender: Any) {
        
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSelectedSate(_ sender: Any) {
        if arrGetState.count > 0 {
            dropDownState.show()
        }
    }
    
    @IBAction func clickedSelectedCity(_ sender: Any) {
        if arrGetCity.count > 0 {
            dropDownCity.show()
        }
    }
    
    //MARK:- TableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGetAllCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryListCell") as! CountryListCell
        
        let dicData = arrGetAllCountry[indexPath.row]
        
        let name = dicData.name ?? ""
        let sortname = dicData.sortname ?? ""
        cell.lblName.text = "\(name) (\(sortname))"
        
        let phonecode = dicData.phonecode ?? ""
        cell.lblCode.text = "+\(phonecode)"
        
        var image = dicData.image ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(IMG_BASE_URL)\(image)")
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicData = arrGetAllCountry[indexPath.row]
        
        let sortname = dicData.sortname ?? ""
        let phonecode = dicData.phonecode ?? ""
        
        self.lblCode.text = "(\(sortname)) +\(phonecode)"
        
        var image = dicData.image ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(IMG_BASE_URL)\(image)")
        imgFalge.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgFalge.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        callGetStateAPI(countryId: dicData.id)
        
        self.objCountryID = dicData.id
        
        viewTblView.isHidden = true
    }
    
    //MARK:- API CALL
    func callSignUPAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let code = lblCode.text!.components(separatedBy: " ")
        let fCode = code[1] as? String
        let playerID = UserDefaults.standard.value(forKey: "PlayerID") as! String
        
        let param = ["fname": self.txtFirstName.text!,"lname": self.txtLastNAme.text!,"phone": "\(fCode!)\(self.txtPhone.text!)","email": self.txtEmail.text!,"password": self.txtPass.text!,"country": self.objCountryID,"state": self.objStateID,"city": self.objCityID,"player_id": playerID,"type":"care_rider"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(SIGNUP_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["message"] as? String ?? ""
                
                APIClient.sharedInstance.hideIndicator()

                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.view.makeToast(data_Message)
                            
                            self.viewBg.isHidden = false
                            self.viewPopUpVeri.slideIn(from: kFTAnimationBottom, in: self.viewPopUpVeri.superview, duration: 0.8, delegate: self, start:Selector("temp") , stop: Selector("temp"))
                            
                            let dicData = response?.value(forKey: "data") as? NSDictionary
                            
                            let otp = response?.value(forKey: "otp") as? String
                            self.strOTP = otp ?? ""
                            
                            let u_ID = dicData?.value(forKey: "u_id") as? String
                            self.stru_ID = u_ID ?? ""
                            
                        } else {
                            
                            let dataMessage1 = response?["data"] as? String ?? ""
                            APIClient.sharedInstance.hideIndicator()
                            self.view.makeToast(dataMessage1)
                        }
                        
                    } else {
                        let dataMessage1 = response?["data"] as? String ?? ""
                        APIClient.sharedInstance.hideIndicator()
                        self.view.makeToast(dataMessage1)
                        
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetALlCountryAPI() {
        
        let param = ["":""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetAllCountry.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetAllCountry.append(list)
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetStateAPI(countryId: String) {
        
        let param = ["country_id": countryId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_STATES, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetState.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetState.append(list)
                            }
                            
                            let dicState = self.arrGetState[0]
                            self.lblState.text = dicState.name
                            
                            self.callGetCityAPI(stateId: dicState.id)
                            
                            self.setDropDownState()
                            
                        }
                        else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetCityAPI(stateId: String) {
        
        let param = ["state_id": stateId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_CITYS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetCity.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetCity.append(list)
                            }
                            
                            let dicState = self.arrGetCity[0]
                            self.lblCity.text = dicState.name
                            
                            self.setDropDownCity()
                            
                        }
                        else
                        {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callLoginAPI() {
        
        let code = lblCode.text!.components(separatedBy: " ")
        let fCode = code[1] as? String
        
        let playerID = UserDefaults.standard.value(forKey: "PlayerID") as! String
        
        let param = ["value": "\(fCode ?? "")\(txtPhone.text ?? "")","password": txtPass.text!,"player_id":playerID]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(LOG_IN, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let dicUser_data = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let dicData = UserLoginData(fromDictionary: dicUser_data!)
                            
                            appDelagte.saveCurrentUserData(dic: dicData)
                            appDelagte.dicLoginUserDetails = dicData
                            
                            UserDefaults.standard.set(true, forKey: "isUserLogin")
                            UserDefaults.standard.synchronize()
                            
                            self.callGetTimeZoneAPI()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callValidateNumberAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["otp": txtCode.text!,"u_id": stru_ID]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(VALIDATE_NUMBER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.callLoginAPI()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callGetTimeZoneAPI() {
        
        let urlParam = GET_NEAREST_TIMEZONE_USER + "\(appDelagte.objLatitude)/\(appDelagte.objLongitude)/\(appDelagte.dicLoginUserDetails?.userId ?? "")"
        
        let param = ["":""]
        
        print(urlParam)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(urlParam, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            appDelagte.setUpSideMenu()
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
