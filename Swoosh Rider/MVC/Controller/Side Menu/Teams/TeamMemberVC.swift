//
//  TeamMemberVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 15/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class TeamMemberVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblView: UITableView!
    
    var arrTeamMemberList: [SRTeamMemberListData] = [SRTeamMemberListData]()

    var objTeamId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        callTeamUserListAPI()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeamMemberList.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "TeamMemberListCell") as! TeamMemberListCell
        
        let dicData = arrTeamMemberList[indexPath.row]
        
        cell.lblName.text = dicData.athleteName ?? ""
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func callTeamUserListAPI() {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","team_id": objTeamId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_MEMBER_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrTeamMemberList.removeAll()

                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                let dicData = SRTeamMemberListData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamMemberList.append(dicData)
                            }
                            

                        } else {
                          //  self.view.makeToast(data_Message)
                            
                        }
                        
                        self.tblView.reloadData()

                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
