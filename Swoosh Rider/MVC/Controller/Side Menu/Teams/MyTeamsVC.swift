//
//  MyTeamsVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 15/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown

class MyTeamsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var viewTarget: UIView!
    
    let dropDownAthlete = DropDown()

    var arrAthleteList: [SRAthleteListData] = [SRAthleteListData]()
    var arrTeamUserList: [SRTeamUserData] = [SRTeamUserData]()


    var objAthleteID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPlaceholder.isHidden = true
        self.tblView.isHidden = false
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        callAthleteListAPI(showIndicator: true)
        
        // Do any additional setup after loading the view.
    }
    
    
    func setDropDown() {
        
        let arrState = NSMutableArray()
         
        if arrAthleteList.count > 0 {
            let objF = arrAthleteList[0]
            self.lblTeamName.text = "\(objF.firstName ?? "")  \(objF.lastName ?? "")"
            self.objAthleteID = objF.id
            self.callTeamUserListAPI(athlete_id: objF.id)
        }
        
        
        for objState in arrAthleteList {
            arrState.add("\(objState.firstName ?? "")" + " \(objState.lastName ?? "")")
        }
        
        dropDownAthlete.dataSource = arrState as! [String]
        dropDownAthlete.anchorView = viewTarget
        dropDownAthlete.direction = .bottom
        
        dropDownAthlete.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblTeamName.text = item
            
            if self.arrAthleteList.count > 0 {
                let dicSate = self.arrAthleteList[index]
                print(dicSate.id!)
                self.objAthleteID = dicSate.id
                self.callTeamUserListAPI(athlete_id: dicSate.id)
            }
        
        }
        
        dropDownAthlete.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDownAthlete.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDownAthlete.dismissMode = .onTap
        dropDownAthlete.textColor = UIColor.darkGray
        dropDownAthlete.backgroundColor = UIColor.white
        dropDownAthlete.selectionBackgroundColor = UIColor.clear
        dropDownAthlete.reloadAllComponents()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    @IBAction func clickedChooseTeam(_ sender: Any) {
        dropDownAthlete.show()
    }
    
    //MARK:- TableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeamUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "MyTeamsListCell") as! MyTeamsListCell
        
        let dicData = arrTeamUserList[indexPath.row]
        
        cell.lblName.text = dicData.teamName ?? ""
        
        cell.btnRemove.clipsToBounds = true
        cell.btnRemove.layer.cornerRadius = 4
        cell.btnRemove
            .layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner] // Right Corner
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clickedRemove(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrTeamUserList[indexPath.row]

        let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TeamMemberVC") as! TeamMemberVC
        vc.objTeamId = dicData.id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func clickedRemove(sender: AnyObject) {
        
        let dicData = arrTeamUserList[sender.tag]
        
        let alert = UIAlertController(title: nil, message: "Please confirm. Do you want to remove the team?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "No", style: .default, handler: { action in
            
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "YES", style: .default, handler: { action in
            self.callTeamRemoveUserAPI(team_id: dicData.id ?? "")
        })
        
        alert.view.tintColor = SWOOSH_FRONT
        
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    
    // MARK: - API Call
    func callAthleteListAPI(showIndicator: Bool) {
        
        if showIndicator == true {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MY_ATHLETE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrAthleteList.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRAthleteListData(fromDictionary: (obj as? NSDictionary)!)
                                
                                self.arrAthleteList.append(dicData)
                            }
                            
                            self.setDropDown()

                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callTeamUserListAPI(athlete_id: String) {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","athlete_id": athlete_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrTeamUserList.removeAll()

                        if message == "success" {
                            
                            self.viewPlaceholder.isHidden = true
                            self.tblView.isHidden = false
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamUserData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamUserList.append(dicData)
                            }

                        } else {
                          //  self.view.makeToast(data_Message)
                            
                            self.viewPlaceholder.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                        self.tblView.reloadData()

                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callTeamRemoveUserAPI(team_id: String) {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","team_id": team_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_REMOVE_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {

                        if message == "success" {
                            
                            self.callTeamUserListAPI(athlete_id: self.objAthleteID)
                            
                        } else {

                            
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}

