//
//  SideMenuViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 14/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import LGSideMenuController
import SDWebImage

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    var arrName = ["Notifications","Athletes","Teams","Care Rides","Messages","Wallet (SwooshCash)","Trip History","Promotions","Invite","Help & Feedback","Logout"]
    
    var arrImage = ["notification","ic_run","ic_Team","ic_calendar","mail","wallet","car","flyers","flyers","help","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKey, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        self.lblName.text = appDelagte.dicLoginUserDetails?.name ?? ""
        
        var image = appDelagte.dicLoginUserDetails?.photo ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: image)
        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "user-2"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.lblName.text = appDelagte.dicLoginUserDetails?.name ?? ""
        
        var image = appDelagte.dicLoginUserDetails?.photo ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: image)
        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "user-2"))
    }
    
    @IBAction func clickedEditProfile(_ sender: Any) {
        isOpenSideMenu = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let profileVC = storyBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        navigation.viewControllers = [profileVC]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        cell.lblName.text = arrName[indexPath.row]
        cell.imgMenu.image = UIImage(named: arrImage[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 1
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyAthletesVC") as! MyAthletesVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 2
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTeamsVC") as! MyTeamsVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 3
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CareRidesVC") as! CareRidesVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 4
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TeamChatVC") as! TeamChatVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 5
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 6
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TripHistoryVC") as! TripHistoryVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 7
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PromotionsVC") as! PromotionsVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 8
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else if indexPath.row == 9
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        }
        else
        {
            isOpenSideMenu = false
            UserDefaults.standard.set(false, forKey: "isUserLogin")
            UserDefaults.standard.synchronize()
            
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let homeNavigation = UINavigationController(rootViewController: home)
            homeNavigation.navigationBar.isHidden = true
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.window?.rootViewController = homeNavigation
                appDelegate.window?.makeKeyAndVisible()
            }
        }
    }
}
