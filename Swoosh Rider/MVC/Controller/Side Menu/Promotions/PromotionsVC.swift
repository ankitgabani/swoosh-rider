//
//  PromotionsVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 16/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class PromotionsVC: UIViewController {
    
    
    @IBOutlet weak var txtCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedCode(_ sender: Any) {
    }
    
    @IBAction func clickedApply(_ sender: Any) {
        self.view.endEditing(true)
        
        if txtCode.text == "" {
            self.view.makeToast("Enter a valid Promo Code.")
        }
        else
        {
            callApplyCouponAPI()
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    
    func callApplyCouponAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","coupon_code": self.txtCode.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(APPLY_COUPON, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    self.txtCode.text = ""
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.view.makeToast(data_Message)                            
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
