//
//  TransctionHiatoryBookingTableCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 25/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class TransctionHiatoryBookingTableCell: UITableViewCell {

    @IBOutlet weak var lblPayID: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblBookingID: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
