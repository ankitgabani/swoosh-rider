//
//  WalletVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 16/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Toast_Swift

class WalletVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewWalletPopupC: UIView!
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var lblWalletBalance: UILabel!
    @IBOutlet weak var txtAmlunt: UITextField!
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrWalletTransactionHistory: [SRWalletTransactionHistoryData] = [SRWalletTransactionHistoryData]()
    
    var walletID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPopup.isHidden = true
        viewWalletPopupC.clipsToBounds = true
        viewWalletPopupC.layer.cornerRadius = 12
        viewWalletPopupC.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        
        tblView.delegate = self
        tblView.dataSource = self
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if appDelagte.isSuccessWallet == true
        {
            appDelagte.isSuccessWallet = false
            viewPopup.isHidden = false
            callWalletTransactionHistoryAPI(showIndicator: false)
        }
        else
        {
            viewPopup.isHidden = true
            callWalletTransactionHistoryAPI(showIndicator: true)
        }
        callWalletBalaceAPI()
        callViewWalletBalaceAPI()
    }
    
    @IBAction func clickedOKPopup(_ sender: Any) {
        viewPopup.isHidden = true
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWalletTransactionHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dicData = arrWalletTransactionHistory[indexPath.row]

        let transaction_id = dicData.transactionId ?? ""
        
        if transaction_id.contains("pi_")
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "TransactionHistoryCell") as! TransactionHistoryCell
            
            cell.lblPayID.text = "Pay ID : \(dicData.transactionId ?? "")"
            cell.lblDateTime.text = "Date and Time : \(dicData.dateTime ?? "")"
            
            if dicData.amountCharge != "" {
                cell.lblAmount.text = "Topup Amount : CAD$\(dicData.amountCharge ?? "")"
            }
            else
            {
                cell.lblAmount.text = "Topup Amount : CAD$\(dicData.amountSpend ?? "")"
            }
            
            return cell
        }
        else
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "TransctionHiatoryBookingTableCell") as! TransctionHiatoryBookingTableCell
            
            cell.lblBookingID.text = "Trip Booking Id : \(dicData.id ?? "")"

            cell.lblPayID.text = "Transaction ID : \(dicData.transactionId ?? "")"
            cell.lblDateTime.text = "Date and Time : \(dicData.dateTime ?? "")"
            
            cell.lblAmount.text = "Trip Fair : CAD$\(dicData.amountSpend ?? "")"
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    @IBAction func clickedAddAmount(_ sender: Any) {
        self.view.hideToast()
        
        if self.txtAmlunt.text == "" {
            self.view.makeToast("Please Select Amount")
        }
        else if Int(self.txtAmlunt.text!)! < 39 {
            self.view.makeToast("Minimum allowed credit is CAD$39")
        }
        else{
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ConfirmPaymentVC") as! ConfirmPaymentVC
            vc.strAmount = self.txtAmlunt.text!
            vc.walletID = self.walletID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    // MARK: - API Call
    
    func callWalletBalaceAPI() {
        
        let param = ["lat": "\(appDelagte.objLatitude)","lang": "\(appDelagte.objLongitude)"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(WALLET_BALANCE_SETTIGNS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            
                        }
                        else
                        {
                            
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callViewWalletBalaceAPI() {
                
        let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(WALLET_BALANCE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? NSDictionary
                
                APIClient.sharedInstance.hideIndicator()

                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            let wallet_id = data_Message?.value(forKey: "wallet_id") as? String
                            let balance = data_Message?.value(forKey: "balance") as? String
                            self.lblWalletBalance.text = "CAD$\(balance ?? "")"
                            self.walletID = wallet_id ?? ""
                        }
                        else
                        {
                            self.lblWalletBalance.text = "$ 0.00"
                        }
                        
                    } else
                    {
                        
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                }
                
            } else {
                APIClient.sharedInstance.hideIndicator()

                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callWalletTransactionHistoryAPI(showIndicator: Bool) {
        
        if showIndicator == true
        {
            APIClient.sharedInstance.showIndicator()
        }
        
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(WALET_TRANSACTION_HISTORY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrWalletTransactionHistory.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRWalletTransactionHistoryData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrWalletTransactionHistory.append(dicData)
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
