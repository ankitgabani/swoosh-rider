//
//  ConfirmPaymentVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 16/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import JMMaskTextField_Swift
import Stripe
class ConfirmPaymentVC: UIViewController,responseDelegate,UITextFieldDelegate {
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var txtCard: JMMaskTextField!
    @IBOutlet weak var txtDate: JMMaskTextField!
    @IBOutlet weak var txtCvv: JMMaskTextField!
    
    @IBOutlet weak var txtPostalCode: JMMaskTextField!
    @IBOutlet weak var viewSucessPopup: UIView!
    @IBOutlet weak var lblPaymentID: UILabel!
    
    @IBOutlet weak var imgCheck: UIImageView!
    
    var objStipeToken = ""
    
    var strAmount = ""
    var walletID = ""
    
    var isChecked = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   self.callStripe_CreateAPI()
        
        self.viewSucessPopup.isHidden = true
        lblAmount.text = "CAD$\(self.strAmount)"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedOKSucuess(_ sender: Any) {
        appDelagte.isSuccessWallet = true
        viewSucessPopup.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedCheckEd(_ sender: Any)
    {
        if isChecked == true
        {
            imgCheck.image = UIImage(named: "ic_uncheckedBox")
            isChecked = false
        }
        else
        {
            imgCheck.image = UIImage(named: "ic_checkedBox")
            isChecked = true
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedPay(_ sender: Any) {
        
        if isChecked == false {
            self.view.makeToast("Please accept our terms before payment.")
        }
        else if self.txtCard.text == "" {
            self.view.makeToast("Please enter your card number")
        }
        else if self.txtDate.text == "" {
            self.view.makeToast("Please enter expiration date")
        }
        else if self.txtCvv.text == "" {
            self.view.makeToast("Please enter cvv")
        }
        else if self.txtPostalCode.text == "" {
            self.view.makeToast("Please enter Postal code")
        }
        else
        {
            setStripe()
        }
    }
    
    func setStripe() {
        
        APIClient.sharedInstance.showIndicator()
        
        // Initiate the card
        
        //var stripCard = STPCard()
        
        let stripCard: STPCardParams = STPCardParams()
        
        // Split the expiration date to extract Month & Year
        if self.txtDate.text!.isEmpty == false {
            let expirationDate = self.txtDate.text?.components(separatedBy: "/")
            let expMonth = UInt((expirationDate![0] as! NSString).intValue)
            let expYear = UInt((expirationDate![1] as! NSString).intValue)
            
            // Send the card info to Strip to get the token
            stripCard.number = self.txtCard.text
            stripCard.cvc = self.txtCvv.text
            stripCard.expMonth = expMonth
            stripCard.expYear = expYear
        }
        
        STPAPIClient.shared().createToken(withCard: stripCard, completion: { (token, error) -> Void in
            
            if error != nil {
                
                self.view.makeToast(error?.localizedDescription)
                print("error")
                print(error?.localizedDescription)
                
                APIClient.sharedInstance.hideIndicator()
                
                return
            }
            print(token?.tokenId ?? "")
            
            self.objStipeToken = token?.tokenId ?? ""
            
            self.callStripe_responseAPI()
            return
        })
    }
    
    func getCurrentMillis()->Int64{
        return  Int64(NSDate().timeIntervalSince1970 * 1000)
    }
    
    func callStripe_CreateAPI() {
        
        let currentTime = getCurrentMillis()
        
//        let array = NSMutableArray()
        let dic = NSMutableDictionary()
        dic.setValue(currentTime, forKey: "id")
        dic.setValue(100.0, forKey: "amount")
//        array.add(dic)
//
//        let param = ["currency": "card","items": array] as [String : Any]
//
//        print(param)
//
        
        let dict11 = NSMutableDictionary()
        dict11.setValue("card", forKey: "currency")
        dict11.setValue(dic, forKey: "items")
        print(dict11)
        print(BASE_URL + STRIPE_CREATE)
        WebParserWS.fetchDataWithURL(url: BASE_URL + STRIPE_CREATE as NSString, type: .TYPE_POST, ServiceName: "CreateOrder", bodyObject: dict11, delegate: self, isShowProgress: true)
        
//        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPostCrate(STRIPE_CREATE, parameters: param, completionHandler: { (response, error, statusCode) in
//
//            if error == nil {
//                print("STATUS CODE \(String(describing: statusCode))")
//                print("Response \(String(describing: response))")
//
//                let message = response?["status"] as? String ?? ""
//                let data_Message = response?["data"] as? String ?? ""
//
//                if statusCode == 200 {
//
//                    APIClient.sharedInstance.hideIndicator()
//
//                    if let responseUser = response {
//
//                        if message == "success"
//                        {
//
//                        }
//                        else
//                        {
//
//                        }
//
//                    } else {
//                        self.view.makeToast(data_Message)
//                    }
//
//                } else {
//
//                    APIClient.sharedInstance.hideIndicator()
//                    self.view.makeToast(data_Message)
//
//                }
//
//            } else {
//
//                print("Response \(String(describing: response))")
//                let message = response?["data"] as? String ?? ""
//                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
//            }
//        })
    }
    
    func callStripe_responseAPI() {
        
        let param = ["userid": appDelagte.dicLoginUserDetails?.userId ?? "","id": walletID,"amount": self.strAmount,"status":"succeed","paymentType":"card","paymentid": self.objStipeToken]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(STRIPE_RESPONSE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            self.viewSucessPopup.isHidden = false
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        print(Response)
        DispatchQueue.main.async {
            
        }
    }
}
