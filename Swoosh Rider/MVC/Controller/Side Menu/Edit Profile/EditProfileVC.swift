//
//  EditProfileVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 14/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage
import DropDown
import MobileCoreServices
import Photos
import Alamofire
import SVProgressHUD

extension Notification.Name {
    
    public static let myNotificationKey = Notification.Name(rawValue: "myNotificationKey")
}

class EditProfileVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    @IBOutlet weak var viewEditProfileBG: UIView!
    @IBOutlet weak var viewEditProfilePopup: UIView!
    @IBOutlet weak var txtCurrentPassEdit: UITextField!
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var viewTargetCountry: UIView!
    
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var viewTargetSate: UIView!
    
    @IBOutlet weak var lblCiry: UILabel!
    @IBOutlet weak var viewTargetCity: UIView!
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTitlePopup: UILabel!
    @IBOutlet weak var txtEditPop: UITextField!
    
    @IBOutlet weak var viewBgPassword: UIView!
    @IBOutlet weak var viewPopupPassword: UIView!
    @IBOutlet weak var txtCurrentPass: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    var dicUserDetails: SRUserDetailsData?
    
    var arrGetAllCountry: [SRGetAllCountryData] = [SRGetAllCountryData]()
    var arrGetState: [SRGetAllCountryData] = [SRGetAllCountryData]()
    var arrGetCity: [SRGetAllCountryData] = [SRGetAllCountryData]()
    
    var objCountryID = String()
    var objStateID = String()
    var objCityID = String()
    
    let dropDownContry = DropDown()
    let dropDownCity = DropDown()
    let dropDownState = DropDown()
    
    var isSelectedFromDrop = false
    
    var imagePicker = UIImagePickerController()
    var objFiledata = String()
    var getFileNameServer = String()
    var selectedImage = UIImage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        viewBgPassword.isHidden = true
        viewBg.isHidden = true
        viewEditProfileBG.isHidden = true
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBg.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callUserDetailsAPI()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = true
        })
    }
    
    //MARK:- Action MEthod
    @IBAction func clickedBack(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": ""]) // Notification
        appDelagte.setUpSideMenu()
    }
    
    @IBAction func clickedUploadpic(_ sender: Any) {
        choosePicture()
    }
    
    @IBAction func clickedChangePassword(_ sender: Any) {
        
        self.txtNewPassword.text = ""
        self.txtCurrentPass.text = ""
        self.txtConfirmPassword.text = ""
        viewBgPassword.isHidden = false
        
        viewPopupPassword.slideIn(from: kFTAnimationBottom, in: viewPopupPassword.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        isOpenSideMenu = false
        UserDefaults.standard.set(false, forKey: "isUserLogin")
        UserDefaults.standard.synchronize()
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = homeNavigation
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    @IBAction func clickedChooseCountry(_ sender: Any) {
        dropDownContry.show()
    }
    
    @IBAction func clickedState(_ sender: Any) {
        dropDownState.show()
        
    }
    
    @IBAction func clickedCity(_ sender: Any) {
        dropDownCity.show()
    }
    
    @IBAction func clickedFirst(_ sender: Any) {
        self.lblTitlePopup.text = "Edit First Name"
        self.txtEditPop.text = self.lblFirstName.text
        
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = false
        })
        
    }
    
    @IBAction func clickedLast(_ sender: Any) {
        self.lblTitlePopup.text = "Edit Last Name"
        self.txtEditPop.text = self.lblLastName.text
        
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = false
        })
        
    }
    
    @IBAction func clickedPhone(_ sender: Any) {
        self.lblTitlePopup.text = "Edit Phone No"
        self.txtEditPop.text = self.lblMobileNumber.text
        
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = false
        })
        
    }
    
    @IBAction func clickedEmail(_ sender: Any) {
        self.lblTitlePopup.text = "Edit Email ID"
        self.txtEditPop.text = self.lblEmail.text
        
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = false
        })
        
    }
    
    @IBAction func clickedCancelPop(_ sender: Any) {
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = true
        })
        
    }
    
    @IBAction func clickedSubmitPop(_ sender: Any) {
        
        if self.lblTitlePopup.text == "Edit First Name" {
            self.lblFirstName.text = self.txtEditPop.text
        }
        else if self.lblTitlePopup.text == "Edit Last Name" {
            self.lblLastName.text = self.txtEditPop.text
        }
        else if self.lblTitlePopup.text == "Edit Phone No" {
            self.lblMobileNumber.text = self.txtEditPop.text
        }
        else if self.lblTitlePopup.text == "Edit Email ID" {
            self.lblEmail.text = self.txtEditPop.text
        }
        
        UIView.transition(with: viewBg, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewBg.isHidden = true
        })
    }
    
    
    @IBAction func clickedClosePopup(_ sender: Any) {
        
        viewPopupPassword.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewBgPassword.isHidden = true
        }
    }
    
    @IBAction func clickedSubmitChangePass(_ sender: Any) {
        self.view.endEditing(true)
        
        if txtCurrentPass.text == "" {
            self.view.makeToast("please enter current password")
        }
        else if txtNewPassword.text == "" {
            self.view.makeToast("please enter new password")
        }
        else if txtConfirmPassword.text == "" {
            self.view.makeToast("please enter confirm password")
        }
        else if txtNewPassword.text != txtConfirmPassword.text {
            self.view.makeToast("new password and confirm password do not match")
        }
        else
        {
            callChangePassAPI()
        }
        
    }
    
    @IBAction func clickedUpdateDetails(_ sender: Any) {
        
        viewEditProfileBG.isHidden = false
        
        viewEditProfilePopup.slideIn(from: kFTAnimationBottom, in: viewEditProfilePopup.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        
    }
    
    @IBAction func clickedCloseEditProfile(_ sender: Any) {
        viewEditProfilePopup.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewEditProfileBG.isHidden = true
        }
    }
    
    @IBAction func clickedSubmitEditProfile(_ sender: Any) {
        self.view.endEditing(true)

        if appDelagte.dicLoginUserDetails?.password != self.txtCurrentPassEdit.text {
            self.view.makeToast("Password does not match !")
        }
        else
        {
            callUpdateUserAPI()
        }
        
    }
    
    
    // MARK: - Camera & Photo Picker
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = [kUTTypeImage as String]
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.view.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func choosePicture() {
        let alert  = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .overCurrentContext
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        let popoverController = alert.popoverPresentationController
        
        popoverController?.permittedArrowDirections = .up
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK:- setUp Drop Down
    func setDropDownContry() {
        
        let arrContry = NSMutableArray()
        
        for objContry in arrGetAllCountry {
            arrContry.add(objContry.name!)
        }
        
        dropDownContry.dataSource = arrContry as! [String]
        dropDownContry.anchorView = viewTargetCountry
        dropDownContry.direction = .any
        
        dropDownContry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.isSelectedFromDrop = true
            
            self.lblCountry.text = item
            
            let dicContry = self.arrGetAllCountry[index]
            print(dicContry.id!)
            self.objCountryID = dicContry.id
            
            self.callGetStateAPI(countryId: dicContry.id)
        }
        
        dropDownContry.bottomOffset = CGPoint(x: 0, y: viewTargetCountry.bounds.height)
        dropDownContry.topOffset = CGPoint(x: 0, y: -viewTargetCountry.bounds.height)
        dropDownContry.dismissMode = .onTap
        dropDownContry.textColor = UIColor.darkGray
        dropDownContry.backgroundColor = UIColor.white
        dropDownContry.selectionBackgroundColor = UIColor.clear
        dropDownContry.reloadAllComponents()
    }
    
    
    func setDropDownState() {
        
        let arrState = NSMutableArray()
        
        for objState in arrGetState {
            arrState.add(objState.name!)
        }
        
        dropDownState.dataSource = arrState as! [String]
        dropDownState.anchorView = viewTargetSate
        dropDownState.direction = .any
        
        dropDownState.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.isSelectedFromDrop = true
            
            self.lblState.text = item
            
            let dicSate = self.arrGetState[index]
            print(dicSate.id!)
            self.objStateID = dicSate.id
            
            self.callGetCityAPI(stateId: dicSate.id)
        }
        
        dropDownState.bottomOffset = CGPoint(x: 0, y: viewTargetSate.bounds.height)
        dropDownState.topOffset = CGPoint(x: 0, y: -viewTargetSate.bounds.height)
        dropDownState.dismissMode = .onTap
        dropDownState.textColor = UIColor.darkGray
        dropDownState.backgroundColor = UIColor.white
        dropDownState.selectionBackgroundColor = UIColor.clear
        dropDownState.reloadAllComponents()
    }
    
    
    func setDropDownCity() {
        
        let arrCity = NSMutableArray()
        
        for objCity in arrGetCity {
            arrCity.add(objCity.name!)
        }
        
        dropDownCity.dataSource = arrCity as! [String]
        dropDownCity.anchorView = viewTargetCity
        dropDownCity.direction = .any
        
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.isSelectedFromDrop = true
            
            self.lblCiry.text = item
            
            let dicSate = self.arrGetCity[index]
            print(dicSate.id!)
            self.objCityID = dicSate.id
        }
        
        dropDownCity.bottomOffset = CGPoint(x: 0, y: viewTargetCity.bounds.height)
        dropDownCity.topOffset = CGPoint(x: 0, y: -viewTargetCity.bounds.height)
        dropDownCity.dismissMode = .onTap
        dropDownCity.textColor = UIColor.darkGray
        dropDownCity.backgroundColor = UIColor.white
        dropDownCity.selectionBackgroundColor = UIColor.clear
        dropDownCity.reloadAllComponents()
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
                        
            let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? ""]
            
            uploadPhoto(BASE_URL + UPLOAD_IMAGE, image: image, params: param, header: ["Keydata" :KEY_DATA]) {
                            }
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
                        
            let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? ""]
            
            uploadPhoto(BASE_URL + UPLOAD_IMAGE, image: image, params: param, header: ["Keydata" :KEY_DATA]) {
                
            }
            
        }
        
     
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- API Calling
    func callUserDetailsAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(USER_DETAILS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let dicUser_data = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let dicData = SRUserDetailsData(fromDictionary: dicUser_data!)
                            self.dicUserDetails = dicData
                            
                            self.lblFirstName.text = dicData.fname ?? ""
                            self.lblLastName.text = dicData.lname ?? ""
                            self.lblMobileNumber.text = dicData.phone ?? ""
                            self.lblEmail.text = dicData.email ?? ""
                            self.objCountryID = dicData.countryId ?? ""
                            self.objStateID = dicData.stateId ?? ""
                            self.objCityID = dicData.cityId ?? ""
                            
                            var image = appDelagte.dicLoginUserDetails?.photo ?? ""
                            image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            let url = URL(string: image)
                            self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                            self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "user-2"))
                            
                            self.callGetALlCountryAPI()
                            self.callGetStateAPI(countryId: dicData.countryId ?? "")
                            self.callGetCityAPI(stateId: dicData.stateId ?? "")
                            
                        } else {
                            self.view.makeToast(data)
                        }
                        
                    } else {
                        self.view.makeToast(data)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }    
    
    func callGetALlCountryAPI() {
        
        let param = ["":""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetAllCountry.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetAllCountry.append(list)
                                
                                if self.objCountryID == list.id {
                                    self.lblCountry.text = list.name
                                }
                            }
                            
                            self.setDropDownContry()
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetStateAPI(countryId: String) {
        
        let param = ["country_id": countryId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_STATES, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetState.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetState.append(list)
                                if self.objStateID == list.id {
                                    self.lblState.text = list.name
                                }
                            }
                            
                            if self.isSelectedFromDrop == true
                            {
                                let dicState = self.arrGetState[0]
                                self.lblState.text = dicState.name
                                self.objStateID = dicState.id
                                self.callGetCityAPI(stateId: dicState.id)
                                
                            }
                            
                            
                            self.setDropDownState()
                            
                        }
                        else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callGetCityAPI(stateId: String) {
        
        let param = ["state_id": stateId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_CITYS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrGetCity.removeAll()
                            
                            let arr_data = responseUser.value(forKey: "data") as? NSArray
                            
                            for object in arr_data! {
                                let list = SRGetAllCountryData(fromDictionary: (object as? NSDictionary)!)
                                self.arrGetCity.append(list)
                                if self.objCityID == list.id {
                                    self.lblCiry.text = list.name
                                }
                            }
                            
                            if self.isSelectedFromDrop == true
                            {
                                let dicState = self.arrGetCity[0]
                                self.lblCiry.text = dicState.name
                                self.objCityID = dicState.id
                            }
                            
                            self.setDropDownCity()
                            
                        }
                        else
                        {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callUpdateUserAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","fname": self.lblFirstName.text!,"lname": self.lblLastName.text!,"phone": self.lblMobileNumber.text!,"email": self.lblEmail.text!,"password": self.txtCurrentPassEdit.text!,"country": self.objCountryID,"city": self.objCityID,"state": self.objStateID]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(UPDATE_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data = response?["message"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.txtCurrentPassEdit.text = ""
                            
                            self.viewEditProfilePopup.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                                self.viewEditProfileBG.isHidden = true
                            }
                            
                            self.callLoginAPI()
                            
                            self.view.makeToast(data)
                            
                        }
                        else
                        {
                            self.view.makeToast(data)
                        }
                        
                    } else {
                        self.view.makeToast(data)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callChangePassAPI() {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","Old_password": self.txtCurrentPass.text!,"New_password": self.txtNewPassword.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CHANGE_PASSWORD_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.viewPopupPassword.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                                self.viewBgPassword.isHidden = true
                            }
                            
                            self.view.makeToast(data)
                            
                        }
                        else
                        {
                            self.view.makeToast(data)
                        }
                        
                    } else {
                        self.view.makeToast(data)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String], completion: @escaping () -> ()) {
        
        SVProgressHUD.show()
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "photo", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
            
            let responseDict = ((data.value as AnyObject) as? NSDictionary)
            
            self.view.makeToast(responseDict?.value(forKey: "data") as? String)
            
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                print("upload success result: \(resut)")
                self.callLoginAPI()
            case .failure(let err):
                SVProgressHUD.dismiss()
                print("upload err: \(err)")
            }
        }
    }
    
    func callLoginAPI() {
        
     
        let param = ["value": appDelagte.dicLoginUserDetails?.phone ?? "","password": appDelagte.dicLoginUserDetails?.password ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(LOG_IN, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss()

                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let dicUser_data = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let dicData = UserLoginData(fromDictionary: dicUser_data!)
                            
                            appDelagte.saveCurrentUserData(dic: dicData)
                            appDelagte.dicLoginUserDetails = dicData
                            
                            UserDefaults.standard.set(true, forKey: "isUserLogin")
                            UserDefaults.standard.synchronize()
                                                        
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
