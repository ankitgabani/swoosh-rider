//
//  InviteViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 16/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

extension Notification.Name {
    public static let myNotificationInviteKey = Notification.Name(rawValue: "myNotificationInviteKey")
}

class InviteViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var imgRide: UIImageView!
    @IBOutlet weak var imgDrive: UIImageView!
    @IBOutlet weak var imgTeamManager: UIImageView!
    
    var objType = "rider"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        imgRide.image = UIImage(named: "ic_selected")
        imgDrive.image = UIImage(named: "ic_Unselected")
        imgTeamManager.image = UIImage(named: "ic_Unselected")
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName {
            txtLastName.becomeFirstResponder()
            return false
        }
        else if textField == txtLastName {
            txtEmail.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedSend(_ sender: Any) {
        if txtFirstName.text == ""
        {
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter First Name !")
            self.view.makeToast("Enter First Name !")
            
        }
        else if txtEmail.text == ""
        {
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter Email Id !")
            self.view.makeToast("Enter Email Id !")
        }
        else
        {
            callInviteAPI()
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    @IBAction func clickedRide(_ sender: Any) {
        objType = "rider"
        imgRide.image = UIImage(named: "ic_selected")
        imgDrive.image = UIImage(named: "ic_Unselected")
        imgTeamManager.image = UIImage(named: "ic_Unselected")
    }
    
    @IBAction func clickedDrive(_ sender: Any) {
        objType = "driver"
        
        imgRide.image = UIImage(named: "ic_Unselected")
        imgDrive.image = UIImage(named: "ic_selected")
        imgTeamManager.image = UIImage(named: "ic_Unselected")
    }
    
    @IBAction func clickedTeamManager(_ sender: Any) {
        objType = "team_manager"
        
        imgRide.image = UIImage(named: "ic_Unselected")
        imgDrive.image = UIImage(named: "ic_Unselected")
        imgTeamManager.image = UIImage(named: "ic_selected")
    }
    
    func callInviteAPI() {
        self.view.endEditing(true)
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? "","user_type": "rider","first_name": self.txtFirstName.text!,"last_name": self.txtLastName.text! ,"email": self.txtEmail.text!,"type": objType]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(INVITE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                                                        
                            objInvite = data_Message
                            
                            appDelagte.setUpSideMenu()
                            
                        } else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
