//
//  TeamChatMemberListCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 20/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class TeamChatMemberListCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
