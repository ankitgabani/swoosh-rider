//
//  NewMessageVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 15/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class NewMessageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewTeamMessage: UIView!
    @IBOutlet weak var viewPopMessage: UIView!
    @IBOutlet weak var txtMEssage: UITextView!
    
    var objAthleteID = ""
    var objTeamID = ""
    
    var arrTeamChatMember: [SRTeamChatMemberData] = [SRTeamChatMemberData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        self.viewTeamMessage.isHidden = true
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewTeamMessage.addGestureRecognizer(tap)
        
        viewPopMessage.clipsToBounds = true
        viewPopMessage.layer.cornerRadius = 10
        viewPopMessage.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Bottom Corner
        
        callTeamUserListAPI()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        UIView.transition(with: viewTeamMessage, duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewTeamMessage.isHidden = true
                          })
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSendMessageTeam(_ sender: Any) {
        
        UIView.transition(with: viewTeamMessage, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewTeamMessage.isHidden = false
                          })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeamChatMember.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "NewMessageListCell") as! NewMessageListCell
        
        let dicData = arrTeamChatMember[indexPath.row]
        
        cell.lblName.text = dicData.name ?? ""
        cell.lblMessage.text = dicData.userType ?? ""
        
        var image = dicData.photo ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(image)")
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "man"))
        
        cell.btnPhoneCall.tag = indexPath.row
        cell.btnPhoneCall.addTarget(self, action: #selector(clickedPhone(sender:)), for: .touchUpInside)
        
        cell.btnChat.tag = indexPath.row
        cell.btnChat.addTarget(self, action: #selector(clickedChatting(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickedPhone(sender: UIButton)
    {
        let dicData = arrTeamChatMember[sender.tag]
        dialNumber(number: dicData.phone ?? "")
    }
    
    @objc func clickedChatting(sender: UIButton)
    {
        let dicData = arrTeamChatMember[sender.tag]
        let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChattingViewController") as! ChattingViewController
        vc.objTeamID = self.objTeamID
        vc.objAthleteID = self.objAthleteID
        vc.dicTeamChatMemberData = dicData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        UIView.transition(with: viewTeamMessage, duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewTeamMessage.isHidden = true
                          })
    }
    
    @IBAction func clickedSend(_ sender: Any) {
        
        if txtMEssage.text == ""
        {
            self.view.makeToast("Enter Your Message")
        }
        else
        {
            callTeamMessageSendPI()
        }
        
        UIView.transition(with: viewTeamMessage, duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.viewTeamMessage.isHidden = true
                          })
    }
    
    
    func callTeamUserListAPI() {
        
        let param = ["team_id": objTeamID,"athlete_id": objAthleteID ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_USERS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrTeamChatMember.removeAll()
                        
                        if message == "success" {
                            
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamChatMemberData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamChatMember.append(dicData)
                            }
                            
                        } else {
                            
                        }
                        
                        self.tblView.reloadData()
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callTeamMessageSendPI() {
        
        let param = ["team_id": objTeamID ?? "","athlete_id": objAthleteID ?? "","message": self.txtMEssage.text ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(USER_MESSAGE_ALL, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.view.makeToast("Message Sent..")
                            
                        } else {
                            
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
