//
//  ChatDateHeaderView.swift
//  Swoosh Rider
//
//  Created by Gabani King on 20/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChatDateHeaderView: UIView {

    @IBOutlet weak var lblDate: UILabel!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    

}
