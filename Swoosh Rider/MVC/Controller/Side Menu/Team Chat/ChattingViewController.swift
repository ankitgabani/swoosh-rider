//
//  ChattingViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 20/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChattingViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- IBOutlet
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    var objAthleteID = ""
    var objTeamID = ""
    var objDriver_id = ""
    var objDriver_name = ""

    var dicTeamChatMemberData: SRTeamChatMemberData?
    
    var dicAll = NSMutableDictionary()
    var arrAllKeys = NSArray()
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMessage.delegate = self
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        
        if objDriver_id != ""
        {
            callOne_To_One_Chat_One_API(message: "")
            self.lblName.text = objDriver_name ?? ""
        }
        else
        {
            self.lblName.text = dicTeamChatMemberData?.name ?? ""
            
            if dicTeamChatMemberData?.userType == "athlete"
            {
                callOne_To_One_Chat_Care_RiderAPI(message: "")
            }
            else
            {
                callOne_To_One_Chat_Care_Rider_CoachAPI(message: "")
            }
        }
      
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Action Method
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSendMessage(_ sender: Any) {
        
        if self.txtMessage.text?.isEmpty == true
        {
            self.view.makeToast("Enter Your Message !")
        }
        else
        {
            if objDriver_id != ""
            {
                callOne_To_One_Chat_One_API(message: self.txtMessage.text ?? "")
            }
            else if dicTeamChatMemberData?.userType == "athlete"
            {
                callOne_To_One_Chat_Care_RiderAPI(message: self.txtMessage.text ?? "")
            }
            else
            {
                callOne_To_One_Chat_Care_Rider_CoachAPI(message: self.txtMessage.text ?? "")
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.arrAllKeys.count
    }
    
    
    //MARK:- UITableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //let key = self.dicAll.allKeys[section]
        
       // let array = self.dicAll.value(forKey: key as! String) as! NSArray
        let key = self.arrAllKeys[section] as! Date
        
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let strKey = dateFormatter.string(from: key)
        
        let array = self.dicAll.value(forKey: strKey as! String) as! NSArray
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let key = self.arrAllKeys[indexPath.section] as! Date
        
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let strKey = dateFormatter.string(from: key)
        
        let array = self.dicAll.value(forKey: strKey as! String) as! NSArray
       
        var dateFormatter2 = DateFormatter()
        
        let dicData = array.object(at: indexPath.row) as! NSDictionary
        
        if dicTeamChatMemberData?.userType == "athlete"
        {
            let status = dicData.value(forKey: "status") as? String
            
            if status == self.objAthleteID
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "SendMessageTableCell") as! SendMessageTableCell
                
                cell.lblName.text = dicData.value(forKey: "send_athlete_name") as? String
                cell.lblMessage.text = dicData.value(forKey: "message") as? String
                
                cell.lblDate.text = dicData.value(forKey: "date_time") as? String
                
                return cell
            }
            else
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "RecieveMessageTableCell") as! RecieveMessageTableCell
                
                cell.lblName.text = dicData.value(forKey: "send_athlete_name") as? String
                cell.lblMessage.text = dicData.value(forKey: "message") as? String
                
                cell.lblDate.text = dicData.value(forKey: "date_time") as? String
                
                return cell
            }
            
        }
        else if dicTeamChatMemberData?.userType == "coach"
        {
            let status = dicData.value(forKey: "status") as? String
            
            if status == "coach"
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "RecieveMessageTableCell") as! RecieveMessageTableCell
                
                cell.lblName.text = dicData.value(forKey: "athlete_name") as? String
                cell.lblMessage.text = dicData.value(forKey: "message") as? String
                
                cell.lblDate.text = dicData.value(forKey: "date_time") as? String
                
                return cell
               
            }
            else
            {
                
                let cell = tblView.dequeueReusableCell(withIdentifier: "SendMessageTableCell") as! SendMessageTableCell
                
                cell.lblName.text = dicData.value(forKey: "caoch_name") as? String
                cell.lblMessage.text = dicData.value(forKey: "message") as? String
                
                cell.lblDate.text = dicData.value(forKey: "date_time") as? String
                
                return cell
            }
            
        }
        else
        {
            let status = dicData.value(forKey: "status") as? String

            if status == "driver"
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "RecieveMessageTableCell") as! RecieveMessageTableCell

                cell.lblName.text = dicData.value(forKey: "driver_name") as? String
                cell.lblMessage.text = dicData.value(forKey: "message") as? String
                
                cell.lblDate.text = dicData.value(forKey: "date_time") as? String
                
                return cell
            }
            else if status == "user"
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "SendMessageTableCell") as! SendMessageTableCell

                cell.lblName.text = dicData.value(forKey: "user_name") as? String
                cell.lblMessage.text = dicData.value(forKey: "message") as? String
                
                cell.lblDate.text = dicData.value(forKey: "date_time") as? String
                
                return cell
            }
            else
            {
                return UITableViewCell()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("ChatDateHeaderView", owner: self, options: [:])?.first as! ChatDateHeaderView
        
        let key = self.arrAllKeys[section] as! Date
        
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let strKey = dateFormatter.string(from: key)
        
        headerView.lblDate.text = strKey
        
        return headerView
    }
    
    // MARK: - API Call
    func callOne_To_One_Chat_Care_RiderAPI(message: String) {
        
        self.txtMessage.text = ""
        
        let param = ["send_user_id": self.objAthleteID,"receive_user_id": dicTeamChatMemberData?.id ?? "","message": message]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ONE_TO_ONE_CHAT_CARE_RIDER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as! NSArray
                            
                            var array = NSMutableArray()
                            
                            for index in 0..<arrData.count{
                                
                                let dicObject = arrData.object(at: index) as! NSDictionary
                                let strDate = dicObject.value(forKey: "date_change") as! String
                                array.add(dicObject)
                                if strDate != ""{
                                    self.dicAll.setValue(array, forKey: strDate)
                                    array = NSMutableArray()
                                }
                            }
                            
                            var convertedArray: [Date] = []

                            var dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMMM dd,yyyy"
                            dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")

                            for dat in self.dicAll.allKeys {
                                let date = dateFormatter.date(from: dat as! String)
                                if let date = date {
                                    convertedArray.append(date)
                                }
                            }

                            self.arrAllKeys = convertedArray.sorted(by: { $0.compare($1) == .orderedAscending }) as NSArray
                            
                            self.tblView.reloadData()
                            
                        } else
                        {
                            self.tblView.reloadData()
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callOne_To_One_Chat_Care_Rider_CoachAPI(message: String) {
        
        self.txtMessage.text = ""
        
        let param = ["athlete_id": self.objAthleteID,"coach_id": dicTeamChatMemberData?.id ?? "","message": message,"status": "coach"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ONE_TO_ONE_CHAT_CARE_RIDER_COACH, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as! NSArray
                            
                            var array = NSMutableArray()
                            
                            for index in 0..<arrData.count{
                                
                                let dicObject = arrData.object(at: index) as! NSDictionary
                                let strDate = dicObject.value(forKey: "date_change") as! String
                                array.add(dicObject)
                                if strDate != ""{
                                    self.dicAll.setValue(array, forKey: strDate)
                                    array = NSMutableArray()
                                }
                            }
                            
                            var convertedArray: [Date] = []

                            var dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMM dd, yyyy"// yyyy-MM-dd"

                            for dat in self.dicAll.allKeys {
                                let date = dateFormatter.date(from: dat as! String)
                                if let date = date {
                                    convertedArray.append(date)
                                }
                            }

                            self.arrAllKeys = convertedArray.sorted(by: { $0.compare($1) == .orderedAscending }) as NSArray
                            
                            self.tblView.reloadData()
                            
                        } else
                        {
                            
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callOne_To_One_Chat_One_API(message: String) {
        
        self.txtMessage.text = ""
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","driver_id": objDriver_id,"message": message,"status": "user"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ONE_TO_ONE_CHAT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as! NSArray
                            
                            var array = NSMutableArray()
                            
                            for index in 0..<arrData.count{
                                
                                let dicObject = arrData.object(at: index) as! NSDictionary
                                let strDate = dicObject.value(forKey: "date_change") as! String
                                array.add(dicObject)
                                if strDate != ""{
                                    self.dicAll.setValue(array, forKey: strDate)
                                    array = NSMutableArray()
                                }
                            }
                            
                            var convertedArray: [Date] = []

                            var dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMM dd, yyyy"// yyyy-MM-dd"

                            for dat in self.dicAll.allKeys {
                                let date = dateFormatter.date(from: dat as! String)
                                if let date = date {
                                    convertedArray.append(date)
                                }
                            }

                            self.arrAllKeys = convertedArray.sorted(by: { $0.compare($1) == .orderedAscending }) as NSArray
                            
                            
                            self.tblView.reloadData()
                            
                        }
                        else
                        {
                            self.view.makeToast(data_Message)

                        }
                        
                    } else {
                        self.view.makeToast(data_Message)

                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                }
                
            } else {
                self.callOne_To_One_Chat_One_API(message: "")
            }
        })
    }
}
