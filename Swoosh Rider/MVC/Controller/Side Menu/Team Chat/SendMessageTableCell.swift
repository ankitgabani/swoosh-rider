//
//  SendMessageTableCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 20/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class SendMessageTableCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
