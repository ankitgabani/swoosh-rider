//
//  TeamChatVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 15/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage

class TeamChatVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var viewPopUpBookuing: UIView!

    @IBOutlet weak var viewBookingAlert: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewTargetAthlete: UIView!
    @IBOutlet weak var lblAthlete: UILabel!
    
    @IBOutlet weak var viewTargetTeam: UIView!
    @IBOutlet weak var lblTema: UILabel!
    
    @IBOutlet weak var imgGroup: UIImageView!
    @IBOutlet weak var lblTitleSelectTeam: UILabel!
    @IBOutlet weak var viewTeam: UIView!
    
    var arrAthleteList: [SRAthleteListData] = [SRAthleteListData]()
    var objAthleteID = ""
    var objTeamID = ""
    
    var arrTeamUserList: [SRTeamUserData] = [SRTeamUserData]()
    
    var arrTeamChatMember: [SRTeamChatMemberData] = [SRTeamChatMemberData]()

    
    let dropDown = DropDown()
    var arrAthelet = [""]
    
    let dropDownTeam = DropDown()
    var arrTeam = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopUpBookuing.clipsToBounds = true
        viewPopUpBookuing.layer.cornerRadius = 12
        viewPopUpBookuing.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        
        viewBookingAlert.isHidden = true
        
        
        tblView.delegate = self
        tblView.dataSource = self
        
        imgGroup.isHidden = true
        lblTitleSelectTeam.isHidden = true
        viewTeam.isHidden = true
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        callAthleteListAPI(showIndicator: true)
        // Do any additional setup after loading the view.
    }
    
    func setDropDown() {
        
        let arrState = NSMutableArray()
        
        if arrAthleteList.count > 0 {
           
            self.lblAthlete.text = "Please Select Athlete"
            arrState.add("Please Select Athlete")
            for objState in arrAthleteList {
                arrState.add("\(objState.firstName ?? "")" + " \(objState.lastName ?? "")")
                self.objAthleteID = objState.id
            }
        }
        
        dropDown.dataSource = arrState as! [String]
        dropDown.anchorView = viewTargetAthlete
        dropDown.direction = .any
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblAthlete.text = item
            
            if item != "Please Select Athlete" {
                self.imgGroup.isHidden = false
                self.lblTitleSelectTeam.isHidden = false
                self.viewTeam.isHidden = false
            } else {
                self.imgGroup.isHidden = true
                self.lblTitleSelectTeam.isHidden = true
                self.viewTeam.isHidden = true
            }
            
            if index != 0
            {
                let dicSate = self.arrAthleteList[index-1]
                //  print(dicSate.id!)
                self.objAthleteID = dicSate.id
                callTeamUserListAPI(athlete_id: dicSate.id)
            }
           
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTargetAthlete.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTargetAthlete.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    func setDropDownTeam() {
        
        let arrState = NSMutableArray()
        
        if arrTeamUserList.count > 0 {
            let objF = arrTeamUserList[0]
            self.lblTema.text = "\(objF.teamName ?? "")"
            self.objTeamID = objF.id
            
            callTeamUserListAPI(team_id: objF.id ?? "")
            
            for objState in arrTeamUserList {
                arrState.add("\(objState.teamName ?? "")")
            }
                        
        }
        else
        {
            self.lblTema.text = ""
            self.objTeamID = ""
        }
        
        dropDownTeam.dataSource = arrState as! [String]
        dropDownTeam.anchorView = viewTargetTeam
        dropDownTeam.direction = .any
        dropDownTeam.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblTema.text = item
            
            if self.arrTeamUserList.count > 0 {
                let dicSate = self.arrTeamUserList[index]
                //    print(dicSate.id!)
                self.objTeamID = dicSate.id
                
                callTeamUserListAPI(team_id: dicSate.id ?? "")
            }
        }
        dropDownTeam.bottomOffset = CGPoint(x: 0, y: viewTargetTeam.bounds.height)
        dropDownTeam.topOffset = CGPoint(x: 0, y: viewTargetTeam.bounds.height)
        dropDownTeam.dismissMode = .onTap
        dropDownTeam.textColor = UIColor.darkGray
        dropDownTeam.backgroundColor = UIColor.white
        dropDownTeam.selectionBackgroundColor = UIColor.clear
        dropDownTeam.reloadAllComponents()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedOKBooking(_ sender: Any) {
        viewBookingAlert.isHidden = true

    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    @IBAction func clickedMessageTeam(_ sender: Any) {
        if objTeamID == "" {
            self.view.makeToast("Please Select Team From the list")
        }
        else
        {
           
        }
    }
    
    
    @IBAction func clickedAddNewMsg(_ sender: Any) {
        if objTeamID == "" {
            self.view.makeToast("Please Select Team From the list")
        }
        else
        {
            let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NewMessageVC") as! NewMessageVC
            vc.objTeamID = self.objTeamID
            vc.objAthleteID = self.objAthleteID
            self.navigationController?.pushViewController(vc, animated: true)
        }
      
    }
    
    @IBAction func clickedSelecteAthlete(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func clickedSelectTeam(_ sender: Any) {
        dropDownTeam.show()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeamChatMember.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "TeamChatMemberListCell") as! TeamChatMemberListCell
        
        let dicData = arrTeamChatMember[indexPath.row]
        
        cell.lblName.text = dicData.name ?? ""
        cell.lblDate.text = dicData.dateTime ?? ""
        cell.lblMessage.text = dicData.message ?? ""
        
        var image = dicData.photo ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(image)")
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "man"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrTeamChatMember[indexPath.row]
        let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChattingViewController") as! ChattingViewController
        vc.objTeamID = self.objTeamID
        vc.objAthleteID = self.objAthleteID
        vc.dicTeamChatMemberData = dicData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- API Calling
    func callAthleteListAPI(showIndicator: Bool) {
        
        if showIndicator == true {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MY_ATHLETE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrAthleteList.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRAthleteListData(fromDictionary: (obj as? NSDictionary)!)
                                
                                self.arrAthleteList.append(dicData)
                            }
                            
                            self.setDropDown()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callTeamUserListAPI(athlete_id: String) {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","athlete_id": athlete_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrTeamUserList.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamUserData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamUserList.append(dicData)
                            }
                            
                            self.setDropDownTeam()
                            
                        } else {
                            
                            self.viewBookingAlert.isHidden = false
                            self.objTeamID = ""
                            self.arrTeamChatMember.removeAll()
                            self.tblView.reloadData()
                            self.arrTeamUserList.removeAll()
                            self.setDropDownTeam()
                            
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callTeamUserListAPI(team_id: String) {
        
        let param = ["team_id": team_id ?? "","athlete_id": objAthleteID ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_CHAT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrTeamChatMember.removeAll()

                        if message == "success" {
                            
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamChatMemberData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamChatMember.append(dicData)
                            }

                        } else {
                          
                        }
                        
                        self.tblView.reloadData()

                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
