//
//  CareRideInforAcceptCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 28/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CareRideInforAcceptCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lblBookingID: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblAthlete: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblclub: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnView: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
