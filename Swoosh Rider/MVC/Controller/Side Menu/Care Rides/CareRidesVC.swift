//
//  CareRidesVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 15/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class CareRidesVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var arrCareBooking: [SRCareBookingData] = [SRCareBookingData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate  = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        tblView.register(UINib(nibName: "CareRidesInfoTableCell", bundle: nil), forCellReuseIdentifier: "CareRidesInfoTableCell")
        tblView.register(UINib(nibName: "CareRideInforAcceptCell", bundle: nil), forCellReuseIdentifier: "CareRideInforAcceptCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callViewCareBookingAPI(showIndicator: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCareBooking.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dicData = arrCareBooking[indexPath.row]
        
        if dicData.status == "pending"
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "CareRidesInfoTableCell") as! CareRidesInfoTableCell
            
            cell.lbName.text = dicData.driverName ?? ""
            
            var image = dicData.driverPhoto ?? ""
            image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "\(image)")
            cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            
            cell.lblBookingID.text = dicData.bookingId ?? ""
            cell.lblDateTime.text = dicData.dateTime ?? ""
            cell.lblFrom.text = dicData.fromLocation ?? ""
            cell.lblTo.text = dicData.toLocation ?? ""
            cell.lblAthlete.text = "\(dicData.athleteName ?? "") (\(dicData.eventDateTime ?? ""))"
            cell.lblTeam.text = dicData.teamName ?? ""
            cell.lblclub.text = dicData.clubName ?? ""
            
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(clickedCancel(sender:)), for: .touchUpInside)
            
            return cell
        }
        else
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "CareRideInforAcceptCell") as! CareRideInforAcceptCell
            
            cell.lbName.text = dicData.driverName ?? ""
            
            var image = dicData.driverPhoto ?? ""
            image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "\(image)")
            cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            
            cell.lblBookingID.text = dicData.bookingId ?? ""
            cell.lblDateTime.text = dicData.dateTime ?? ""
            cell.lblFrom.text = dicData.fromLocation ?? ""
            cell.lblTo.text = dicData.toLocation ?? ""
            cell.lblAthlete.text = "\(dicData.athleteName ?? "") (\(dicData.eventDateTime ?? ""))"
            cell.lblTeam.text = dicData.teamName ?? ""
            cell.lblclub.text = dicData.clubName ?? ""
            
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(clickedCancel(sender:)), for: .touchUpInside)

            
            cell.btnCall.tag = indexPath.row
            cell.btnCall.addTarget(self, action: #selector(clickedCall(sender:)), for: .touchUpInside)

            
            cell.btnChat.tag = indexPath.row
            cell.btnChat.addTarget(self, action: #selector(clickedChat(sender:)), for: .touchUpInside)

            
            cell.btnView.tag = indexPath.row
            cell.btnView.addTarget(self, action: #selector(clickedView(sender:)), for: .touchUpInside)
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dicData = arrCareBooking[indexPath.row]
        
        if dicData.status == "pending"
        {
            return 345
        }
        else
        {
            return 300
        }
    }
    
    @objc func clickedCancel(sender: UIButton)
    {
        let dicData = arrCareBooking[sender.tag]
        
        callViewCareBookingCancelAPI(booking_id: dicData.bookingId ?? "")
    }
    
    @objc func clickedCall(sender: UIButton)
    {
        let dicData = arrCareBooking[sender.tag]
        dialNumber(number: dicData.phone ?? "")
    }
    
    @objc func clickedChat(sender: UIButton)
    {
        let dicData = arrCareBooking[sender.tag]
        let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChattingViewController") as! ChattingViewController
        vc.objDriver_id = dicData.driverId ?? ""
        vc.objDriver_name = dicData.driverName ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickedView(sender: UIButton)
    {
        let dicData = arrCareBooking[sender.tag]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        appDelagte.isFromCareRideViewMenu = true
        appDelagte.isFromCareRideViewMenu_BookingID = dicData.bookingId ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    func callViewCareBookingAPI(showIndicator: Bool) {
        
        if showIndicator == true
        {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(VIEW_CARE_BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrCareBooking.removeAll()
                        self.viewNoData.isHidden = true
                        self.tblView.isHidden = false

                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRCareBookingData(fromDictionary: (obj as? NSDictionary)!)
                                
                                if dicData.status == "pending" || dicData.status == "accept"
                                {
                                    self.arrCareBooking.append(dicData)
                                }
                            }
                            
                            if self.arrCareBooking.count == 0
                            {
                                self.viewNoData.isHidden = false
                                self.tblView.isHidden = true
                            }
                            else
                            {
                                self.viewNoData.isHidden = true
                                self.tblView.isHidden = false
                            }
                            
                        }
                        else
                        {
                            self.viewNoData.isHidden = false
                            self.tblView.isHidden = true
                            self.view.makeToast(data_Message)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callViewCareBookingCancelAPI(booking_id: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","booking_id": booking_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CARE_BOOKING_CANCEL, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                                                
                        if message == "success" {
                            
                            self.callViewCareBookingAPI(showIndicator: false)
                            self.view.makeToast(data_Message)

                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                                                
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
