//
//  HelpViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 16/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import MessageUI

class HelpViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var txtEnterHere: UITextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    
    
    var objPhone = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.isHidden = true
        viewPopUp.layer.cornerRadius = 12
        viewPopUp.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        callContactInfoAPI()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedEmail(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients([self.lblEmail.text!])
            composeVC.setSubject("")
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
        else
        {
            
            let alert = UIAlertController(title: "Swoosh Rider", message: "Your Mail service is not configured.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func clickedOkPopup(_ sender: Any) {
        self.viewBg.isHidden = true
    }
    
    
    @IBAction func clickedPhone(_ sender: Any) {
        
        guard let url = URL(string: "telprompt://\(self.objPhone)"),
            UIApplication.shared.canOpenURL(url) else {
                return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
    }
    
    @IBAction func clickedSubmit(_ sender: Any) {
        
        if txtEnterHere.text == ""
        {
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter Your Query !")
            self.view.makeToast("Enter Your Query !")
        }
        else
        {
            self.view.endEditing(true)
            callSubmitQueryUserAPI()
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    func callContactInfoAPI() {
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CONTACT_INFO, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            let dicData = response?.value(forKey: "data") as? NSDictionary
                            
                            self.lblEmail.text = dicData?.value(forKey: "email") as? String
                            
                            self.lblPhone.text = dicData?.value(forKey: "phone") as? String
                            
                            self.objPhone = dicData?.value(forKey: "phone_no") as? String ?? ""
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callSubmitQueryUserAPI() {
        
        APIClient.sharedInstance.showIndicator()

        let param = ["query": self.txtEnterHere.text!,"User_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(SUBMIT_QUERY_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.txtEnterHere.text = ""
                        
                        if message == "success" {
                            
                            self.viewBg.isHidden = false
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
