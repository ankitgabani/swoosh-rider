//
//  MyAthletesListCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 14/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class MyAthletesListCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
