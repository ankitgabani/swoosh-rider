//
//  EditMyAthletesVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 14/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage
import MobileCoreServices
import Photos
import GooglePlaces
import CoreLocation
import Alamofire

class EditMyAthletesVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var viewMainSearch: UIView!
    @IBOutlet weak var txtSearchLocation: UITextField!
    @IBOutlet weak var tblViewLocation: UITableView!
    
    @IBOutlet weak var viewUpdatedBG: UIView!
    @IBOutlet weak var viewUpdatePopUp: UIView!
    
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    
    var dicAthleteData: SRAthleteListData?
    
    var imagePicker = UIImagePickerController()
    var objFiledata = String()
    var getFileNameServer = String()
    var selectedImage = UIImage()

    var resultsArray = [PlaceData]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    var objStrCity: String?
    var objStrCuntry: String?
    var objStrPincode: String?
    var objStrState: String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        txtSearchLocation.delegate = self
        
        tblViewLocation.delegate = self
        tblViewLocation.dataSource = self
        
        txtSearchLocation.delegate = self
        viewMainSearch.isHidden = true
        
        viewUpdatedBG.isHidden = true
        viewUpdatePopUp.clipsToBounds = true
        viewUpdatePopUp.layer.cornerRadius = 12
        viewUpdatePopUp.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner

        
        self.txtEmail.text = dicAthleteData?.email ?? ""
        self.txtPhone.text = dicAthleteData?.phone ?? ""
        self.txtLocation.text = dicAthleteData?.address ?? ""
        
        var image = dicAthleteData?.image ?? ""
        image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "https://swooshride.com/\(image)")
        imgPic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgPic.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedClearSearchLocation(_ sender: Any) {
           self.txtSearchLocation.text = ""
           self.resultsArray.removeAll()
           self.tblViewLocation.reloadData()
           
           if self.txtSearchLocation.text == "" {
               self.viewMainSearch.isHidden = true
           }
           
       }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedChoosePhoto(_ sender: Any) {
        choosePicture()
    }
    
    @IBAction func clickedUpdate(_ sender: Any) {
        
        if txtEmail.text == ""
        {
            self.view.makeToast("enter email Id !")
        }
        else if txtPhone.text == "" {
            self.view.makeToast("enter phone No !")

        }
        else if txtLocation.text == "" {
            self.view.makeToast("enter Address!")
        }
        else
        {
            callAthleteEDITAPI()
        }
        
    }
    
    @IBAction func clickedRemoveLocation(_ sender: Any) {
        self.txtLocation.text = ""
    }
    @IBAction func clickedOKPopup(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedAddNewAddress(_ sender: Any) {
        txtSearchLocation.becomeFirstResponder()
        viewMainSearch.isHidden = false
    }
    
    @IBAction func clickedEditAddress(_ sender: UITextField) {
       
    }
    
    
    // MARK: - Camera & Photo Picker
       func openCamera()
       {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
           {
               
               imagePicker.sourceType = UIImagePickerController.SourceType.camera
               imagePicker.allowsEditing = true
               imagePicker.mediaTypes = [kUTTypeImage as String]
               self.present(imagePicker, animated: true, completion: nil)
           }
           else
           {
               
               self.view.makeToast("You don't have camera")
           }
       }
       
       func openGallary()
       {
           
           imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
           imagePicker.allowsEditing = true
           imagePicker.mediaTypes = ["public.image"]
           self.present(imagePicker, animated: true, completion: nil)
       }
       
       func choosePicture() {
           let alert  = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
           alert.modalPresentationStyle = .overCurrentContext
           alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
               self.openCamera()
           }))
           alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
               self.openGallary()
           }))
           
           alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
           
           let popoverController = alert.popoverPresentationController
           
           popoverController?.permittedArrowDirections = .up
           
           self.present(alert, animated: true, completion: nil)
       }
    
    func callAthleteEDITAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["id": dicAthleteData?.id ?? "","email": txtEmail.text!,"phone": txtPhone.text!,"address": txtLocation.text!,"photo": self.objFiledata]
                
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(UPDATE_ATHLETE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.viewUpdatedBG.isHidden = false
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImage = image
            imgPic.image = image
            
            let data = image.pngData()
            let strBase64:String = (data?.base64EncodedString())!
            
            self.objFiledata = strBase64
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImage = image
            imgPic.image = image
            
            let data = image.pngData()
            let strBase64:String = (data?.base64EncodedString())!
            
            self.objFiledata = strBase64
            
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension EditMyAthletesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultsArray != nil{
            return resultsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewLocation.dequeueReusableCell(withIdentifier: "SearchLocationCell") as! SearchLocationCell
        
        let objPlaceData = resultsArray[indexPath.row]
        cell.lblTitleName.text = objPlaceData.firstName
        cell.lblSubTitleName.text = objPlaceData.secondName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objPlaceData = resultsArray[indexPath.row]
        self.txtLocation.text = "\(objPlaceData.firstName ?? ""), \(objPlaceData.secondName ?? "")"
        
        objStrCity = objPlaceData.strCity ?? ""
        objStrCuntry = objPlaceData.strCuntry ?? ""
        objStrPincode = objPlaceData.strPincode ?? ""
        objStrState = objPlaceData.strState ?? ""
        
        self.viewMainSearch.isHidden = true
        self.txtSearchLocation.text = ""
        self.resultsArray.removeAll()
        self.tblViewLocation.reloadData()
        
    }
}

extension EditMyAthletesVC : GMSAutocompleteFetcherDelegate{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.resultsArray.removeAll()
        for prediction in predictions {
            var placeDataObj = PlaceData()
            if let prediction = prediction as GMSAutocompletePrediction?{
                
                let placeId = prediction.placeID
                
                let placeClient = GMSPlacesClient()
                
                placeClient.lookUpPlaceID(prediction.placeID) { (place, error) -> Void in
                    if let error = error {
                        //show error
                        return
                    }
                    
                    if let place = place {
                        place.coordinate.longitude //longitude
                        place.coordinate.latitude
                        placeDataObj.firstName = prediction.attributedPrimaryText.string
                        placeDataObj.secondName = prediction.attributedSecondaryText?.string
                        placeDataObj.lattitude = place.coordinate.latitude
                        placeDataObj.longitude = place.coordinate.longitude
                        placeDataObj.country_name = place.formattedAddress
                        placeDataObj.strCity = place.name
                        placeDataObj.strState = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
                        placeDataObj.strCuntry = place.addressComponents?.first(where: { $0.type == "country" })?.name
                        placeDataObj.strPincode = place.addressComponents?.first(where: { $0.type == "postal_code" })?.name
                        
                    } else {
                        //show error
                    }
                    self.resultsArray.append(placeDataObj)
                    DispatchQueue.main.async {
                        if self.resultsArray.count == 0 {
                            //self.viewNoLocation.isHidden = false
                            self.tblViewLocation.isHidden = true
                        }
                        else
                        {
                            //self.viewNoLocation.isHidden = true
                            print(333333)
                            
                            if self.txtSearchLocation.text == "" {
                                self.tblViewLocation.isHidden = true
                            }
                            else
                            {
                                self.tblViewLocation.isHidden = false
                            }
                            
                        }
                        self.tblViewLocation.reloadData()
                    }
                }
            }
            
        }
        
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error.localizedDescription)
    }
    
    
}



extension EditMyAthletesVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            //            let updatedText = text.replacingCharacters(in: textRange,
            //                                                       with: string)
            //            print("Text: \(updatedText)")
            //            print("Range: \(range)&\(text)")
            let mergeString = "\(textField.text!)\(string)"
            var finalString = ""
            if string != ""{
                finalString = mergeString
            }else{
                finalString = String(mergeString.dropLast())
            }
            print("FinalString\(finalString)")
            if finalString == ""{
                resultsArray = [PlaceData]()
                //                gmsFetcher.sourceTextHasChanged("")
                DispatchQueue.main.async {
                    if self.resultsArray.count == 0 {
                        //  self.viewNoLocation.isHidden = false
                        
                        print(111111)
                        DispatchQueue.main.async {
                            self.tblViewLocation.isHidden = true
                        }
                    }
                    else
                    {
                        //  self.viewNoLocation.isHidden = true
                        print(2222222)
                        
                        self.tblViewLocation.isHidden = false
                    }
                    self.tblViewLocation.reloadData()
                }
                
            }else{
                gmsFetcher.sourceTextHasChanged(finalString)
            }
            
        }
        else
        {
            
        }
        
        return true
    }
}
