//
//  CareTableViewCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 18/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CareTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBookingID: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewRateing: FloatRatingView!
    
    @IBOutlet weak var lblFromLocation: UILabel!
    
    @IBOutlet weak var lblToLocation: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
