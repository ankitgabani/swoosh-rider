//
//  CancelledInstantCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 29/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CancelledInstantCell: UITableViewCell {
    
    @IBOutlet weak var mainiew: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgStatus: UIImageView!
    
    @IBOutlet weak var lblBookingID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFromAddress: UILabel!
    @IBOutlet weak var lblToAddress: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStausName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainiew.layer.cornerRadius = 6
        mainiew.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
