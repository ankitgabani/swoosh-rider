//
//  TripHistoryVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 16/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class TripHistoryVC: UIViewController {
    
    @IBOutlet weak var viewHidening: UIView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    
    @IBOutlet weak var lblRideID: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblTripType: UILabel!
    @IBOutlet weak var lblTripTime: UILabel!
    
    
    @IBOutlet weak var lblAddressFrom: UILabel!
    @IBOutlet weak var lblAddressTo: UILabel!
    
    
    @IBOutlet weak var btnCare: UIButton!
    @IBOutlet weak var btnInstant: UIButton!
    
    @IBOutlet weak var viewCare: UIView!
    @IBOutlet weak var viewIntant: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrCareBooking: [SRCareBookingData] = [SRCareBookingData]()
    var arrTripInstant: [SRTripInstantData] = [SRTripInstantData]()
    
    
    var dicUserTripDetails: SRUserTripDetailsData?
    
    var currentSelectedTab = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.contentInset = UIEdgeInsets(top: 10,left: 0,bottom: 0,right: 0)
        
        viewBG.isHidden = true
        
        
        tblView.delegate = self
        tblView.dataSource = self
        
        viewCare.isHidden = false
        viewIntant.isHidden = true
        
        btnCare.setTitleColor(SWOOSH_FRONT, for: .normal)
        btnInstant.setTitleColor(UIColor.white, for: .normal)
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        tblView.register(UINib(nibName: "CareTableViewCell", bundle: nil), forCellReuseIdentifier: "CareTableViewCell")
        
        tblView.register(UINib(nibName: "CancelledInstantCell", bundle: nil), forCellReuseIdentifier: "CancelledInstantCell")
        
        tblView.register(UINib(nibName: "CompletedInstantCell", bundle: nil), forCellReuseIdentifier: "CompletedInstantCell")
        
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewHidening.addGestureRecognizer(tap)
        viewHidening.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        viewPopUp.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewBG.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callViewCareBookingAPI()
        
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK:- Action Method
    @IBAction func clickedCare(_ sender: Any) {
        currentSelectedTab = 1
        btnCare.setTitleColor(SWOOSH_FRONT, for: .normal)
        btnInstant.setTitleColor(UIColor.white, for: .normal)
        callViewCareBookingAPI()
        viewCare.isHidden = false
        viewIntant.isHidden = true
    }
    
    @IBAction func clickedInstant(_ sender: Any) {
        currentSelectedTab = 2
        callTripInstantAPI()
        btnCare.setTitleColor(UIColor.white, for: .normal)
        btnInstant.setTitleColor(SWOOSH_FRONT, for: .normal)
        
        viewCare.isHidden = true
        viewIntant.isHidden = false
        
        self.tblView.reloadData()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        viewPopUp.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewBG.isHidden = true
        }
    }
    
}

extension TripHistoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentSelectedTab == 1
        {
            return arrCareBooking.count
        }
        else
        {
            return arrTripInstant.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if currentSelectedTab == 1
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "CareTableViewCell") as! CareTableViewCell
            
            let dicData = arrCareBooking[indexPath.row]
            
            cell.lblBookingID.text = "Care Ride Booking (ID: \(dicData.showbookingId ?? ""))"
            
            cell.lblDateTime.text = dicData.dateTime ?? ""
            
            cell.lblCost.text = "CAD$\(dicData.cost ?? "")"
            
            cell.lblDriverName.text = dicData.driverName ?? ""
            
            if dicData.status == "deny"
            {
                cell.lblStatus.text = "REJECTED".uppercased()
                cell.lblStatus.textColor = UIColor(red: 151/255, green: 0/255, blue: 0/255, alpha: 1)
                cell.lblStatus.layer.borderColor = UIColor(red: 151/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
            }
            else
            {
                cell.lblStatus.text = "\(dicData.status ?? "")".uppercased()
                cell.lblStatus.textColor = UIColor(red: 0/255, green: 146/255, blue: 0/255, alpha: 1)
                cell.lblStatus.layer.borderColor = UIColor(red: 0/255, green: 146/255, blue: 0/255, alpha: 1).cgColor
            }
            
            cell.viewRateing.rating = Double(dicData.driverRating ?? "")!
            
            cell.lblFromLocation.text = dicData.fromLocation ?? ""
            
            cell.lblToLocation.text = dicData.toLocation ?? ""
            
            return cell
        }
        else
        {
            let dicData = arrTripInstant[indexPath.row]
            
            if dicData.status == "cancelled"
            {
                
                let cell = tblView.dequeueReusableCell(withIdentifier: "CancelledInstantCell") as! CancelledInstantCell
                
                var image = dicData.driverPhoto ?? ""
                image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url = URL(string: "\(image)")
                cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                
                cell.lblBookingID.text = dicData.bookingId ?? ""
                
                cell.lblDate.text = dicData.bookingDate ?? ""
                
                cell.lblTime.text = dicData.bookingTime ?? ""
                
                cell.lblFromAddress.text = "From : \(dicData.startLocation ?? "")"
                
                cell.lblToAddress.text = "To : \(dicData.endLocation ?? "")"
                
                cell.lblPrice.text = dicData.estimatedFare ?? ""
                
                if dicData.status == "auto_cancelled" || dicData.status == "cancelled"
                {
                   // cell.lblStausName.text = "CANCELLED"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "Cancelled")

                }
                else if dicData.status == "completed"
                {
                    //cell.lblStausName.text = "COMPLETE"
                    cell.imgStatus.image = UIImage(named: "completed")
                    cell.imgStatus.isHidden = false

                }
                else if dicData.status == "New Booking" {
                  ///  cell.lblStausName.text = ""
                    cell.imgStatus.isHidden = true
                }
                
                return cell
            }
            else if dicData.status == "driver_accepted"
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "CompletedInstantCell") as! CompletedInstantCell
                
                var image = dicData.driverPhoto ?? ""
                image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url = URL(string: "\(image)")
                cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                
                cell.lblBookingID.text = dicData.bookingId ?? ""
                
                cell.lblDate.text = dicData.bookingDate ?? ""
                
                cell.lblTime.text = dicData.bookingTime ?? ""
                
                cell.lblFromAddress.text = "From : \(dicData.startLocation ?? "")"
                
                cell.lblToAddress.text = "To : \(dicData.endLocation ?? "")"
                
                cell.lblPrice.text = dicData.estimatedFare ?? ""
                
                cell.lblStartDate.text = dicData.startDate ?? ""
                
                cell.lblEndDate.text = dicData.endDate ?? ""
                
                cell.lblStartTime.text = dicData.startTime ?? ""
                
                cell.lblEndTime.text = dicData.endTime ?? ""
                
                if dicData.status == "New Booking"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = true
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true

                }
                else if dicData.status == "driver_accepted"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = false
                    cell.btnViewMap.isHidden = false
                    cell.btnStartTrip.isHidden = true
                }
                else
                {
                    cell.btnCancel.isHidden = true
                    cell.btnShare.isHidden = true
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true
                }
                
                if dicData.status == "auto_cancelled" || dicData.status == "cancelled"
                {
                    //cell.lblStatusName.text = "CANCELLED"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "Cancelled")
                }
                else if dicData.status == "completed"
                {
                    //cell.lblStatusName.text = "COMPLETE"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "completed")

                }
                else if dicData.status == "New Booking" {
                   // cell.lblStatusName.text = ""
                    cell.imgStatus.isHidden = true
                }
                else if dicData.status == "driver_accepted" {
                    cell.imgStatus.image = UIImage(named: "ic_booked")
                    cell.imgStatus.isHidden = false
                }
                
                cell.btnCancel.tag = indexPath.row
                cell.btnCancel.addTarget(self, action: #selector(clickedCancel(sender:)), for: .touchUpInside)
                
                cell.btnViewMap.tag = indexPath.row
                cell.btnViewMap.addTarget(self, action: #selector(clickedViewMap(sender:)), for: .touchUpInside)
                
                cell.btnShare.tag = indexPath.row
                cell.btnShare.addTarget(self, action: #selector(clickedShare(sender:)), for: .touchUpInside)

                return cell
            }
            else if dicData.status == "reaching" || dicData.status == "ongoing"
            {
                let cell = tblView.dequeueReusableCell(withIdentifier: "CompletedInstantCell") as! CompletedInstantCell
                
                var image = dicData.driverPhoto ?? ""
                image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url = URL(string: "\(image)")
                cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                
                cell.lblBookingID.text = dicData.bookingId ?? ""
                
                cell.lblDate.text = dicData.bookingDate ?? ""
                
                cell.lblTime.text = dicData.bookingTime ?? ""
                
                cell.lblFromAddress.text = "From : \(dicData.startLocation ?? "")"
                
                cell.lblToAddress.text = "To : \(dicData.endLocation ?? "")"
                
                cell.lblPrice.text = dicData.estimatedFare ?? ""
                
                cell.lblStartDate.text = dicData.startDate ?? ""
                
                cell.lblEndDate.text = dicData.endDate ?? ""
                
                cell.lblStartTime.text = dicData.startTime ?? ""
                
                cell.lblEndTime.text = dicData.endTime ?? ""
                
                if dicData.status == "New Booking"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = true
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true
                }
                else if dicData.status == "driver_accepted"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = false
                    cell.btnViewMap.isHidden = false
                    cell.btnStartTrip.isHidden = true
                }
                else if dicData.status == "reaching"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = false
                    cell.btnViewMap.isHidden = false
                    cell.btnStartTrip.isHidden = false
                }
                else if dicData.status == "ongoing"
                {
                    cell.btnCancel.isHidden = true
                    cell.btnShare.isHidden = false
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true
                }
                else
                {
                    cell.btnCancel.isHidden = true
                    cell.btnShare.isHidden = true
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true
                }
                
                if dicData.status == "auto_cancelled" || dicData.status == "cancelled"
                {
                    //cell.lblStatusName.text = "CANCELLED"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "Cancelled")
                }
                else if dicData.status == "completed"
                {
                    //cell.lblStatusName.text = "COMPLETE"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "completed")

                }
                else if dicData.status == "New Booking" {
                   // cell.lblStatusName.text = ""
                    cell.imgStatus.isHidden = true
                }
                else if dicData.status == "driver_accepted" || dicData.status == "reaching" || dicData.status == "ongoing" {
                    cell.imgStatus.image = UIImage(named: "ic_booked")
                    cell.imgStatus.isHidden = false
                }
                
                cell.btnCancel.tag = indexPath.row
                cell.btnCancel.addTarget(self, action: #selector(clickedCancel(sender:)), for: .touchUpInside)
                
                cell.btnViewMap.tag = indexPath.row
                cell.btnViewMap.addTarget(self, action: #selector(clickedViewMap(sender:)), for: .touchUpInside)
                
                cell.btnShare.tag = indexPath.row
                cell.btnShare.addTarget(self, action: #selector(clickedShare(sender:)), for: .touchUpInside)

                cell.btnStartTrip.tag = indexPath.row
                cell.btnStartTrip.addTarget(self, action: #selector(clickedStartTrip(sender:)), for: .touchUpInside)

                return cell
            }
            else
            {
                
                let cell = tblView.dequeueReusableCell(withIdentifier: "CompletedInstantCell") as! CompletedInstantCell
                
                var image = dicData.driverPhoto ?? ""
                image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url = URL(string: "\(image)")
                cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                
                cell.lblBookingID.text = dicData.bookingId ?? ""
                
                cell.lblDate.text = dicData.bookingDate ?? ""
                
                cell.lblTime.text = dicData.bookingTime ?? ""
                
                cell.lblFromAddress.text = "From : \(dicData.startLocation ?? "")"
                
                cell.lblToAddress.text = "To : \(dicData.endLocation ?? "")"
                
                cell.lblPrice.text = dicData.estimatedFare ?? ""
                
                cell.lblStartDate.text = dicData.startDate ?? ""
                
                cell.lblEndDate.text = dicData.endDate ?? ""
                
                cell.lblStartTime.text = dicData.startTime ?? ""
                
                cell.lblEndTime.text = dicData.endTime ?? ""
                
                if dicData.status == "New Booking"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = true
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true
                }
                else if dicData.status == "driver_accepted"
                {
                    cell.btnCancel.isHidden = false
                    cell.btnShare.isHidden = false
                    cell.btnViewMap.isHidden = false
                    cell.btnStartTrip.isHidden = true
                }
                else
                {
                    cell.btnCancel.isHidden = true
                    cell.btnShare.isHidden = true
                    cell.btnViewMap.isHidden = true
                    cell.btnStartTrip.isHidden = true
                }
                
                if dicData.status == "auto_cancelled" || dicData.status == "cancelled"
                {
                    //cell.lblStatusName.text = "CANCELLED"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "Cancelled")
                }
                else if dicData.status == "completed"
                {
                    //cell.lblStatusName.text = "COMPLETE"
                    cell.imgStatus.isHidden = false
                    cell.imgStatus.image = UIImage(named: "completed")

                }
                else if dicData.status == "New Booking" {
                   // cell.lblStatusName.text = ""
                    cell.imgStatus.isHidden = true
                }
                else if dicData.status == "driver_accepted" {
                    cell.imgStatus.image = UIImage(named: "ic_booked")
                    cell.imgStatus.isHidden = false
                }
                
                cell.btnCancel.tag = indexPath.row
                cell.btnCancel.addTarget(self, action: #selector(clickedCancel(sender:)), for: .touchUpInside)
                
                cell.btnViewMap.tag = indexPath.row
                cell.btnViewMap.addTarget(self, action: #selector(clickedViewMap(sender:)), for: .touchUpInside)

                cell.btnShare.tag = indexPath.row
                cell.btnShare.addTarget(self, action: #selector(clickedShare(sender:)), for: .touchUpInside)
                
                cell.btnStartTrip.tag = indexPath.row
                cell.btnStartTrip.addTarget(self, action: #selector(clickedStartTrip(sender:)), for: .touchUpInside)

                return cell
            }
            
        }
        
    }
    
    @objc func clickedCancel(sender: UIButton) {
        let dicData = arrTripInstant[sender.tag]
        
        let alert = UIAlertController(title: "", message: "Are you sure, You wanted to Cancel that Booking?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "No", style: .default, handler: { action in
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            self.callCancelTripAPI(bookingId: dicData.bookingId ?? "")
            
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
    
    @objc func clickedViewMap(sender: UIButton) {
        
        let dicData = arrTripInstant[sender.tag]

        appDelagte.objStart_lat_Map = dicData.startLat ?? ""
        appDelagte.objStart_lang_Map = dicData.startLang ?? ""

        appDelagte.objEnd_lat_Map = dicData.endLat ?? ""
        appDelagte.objEnd_lang_Map = dicData.endLang ?? ""
        appDelagte.isViewMap = true
        
        appDelagte.setUpSideMenu()
    }
    
    @objc func clickedStartTrip(sender: UIButton) {
        
        let dicData = arrTripInstant[sender.tag]

        callStartTripAPI(bookingId: dicData.bookingId ?? "", driverId: dicData.driverId ?? "")
    }
    
    @objc func clickedShare(sender: UIButton) {
        
        let dicData = arrTripInstant[sender.tag]

        //let image = UIImage(named: "swoosh_logo_2")
        
        let image = UIImage(named: "car")
        
        let objDes = "hi there, this is \(dicData.driverName ?? ""), I am using SWOOSH ride from \(dicData.startLocation ?? "") to\n\(dicData.endLocation ?? "")\n on \(dicData.bookingDate ?? "") \(dicData.bookingTime ?? "")\n with \(dicData.driverName ?? "")\nContact No: \(dicData.driverPhone ?? "")\nNumber plate: \(dicData.driverLPlate ?? "")"

        let vc = UIActivityViewController(activityItems: [image!, objDes], applicationActivities: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = vc.popoverPresentationController {
                let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                let barButton = UIBarButtonItem(customView: btnShare)
                popoverController.barButtonItem = barButton
            }
        }
        self.present(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if currentSelectedTab == 1
        {
            return 185
        }
        else
        {
            let dicData = arrTripInstant[indexPath.row]
            
            if dicData.status == "cancelled"
            {
                return 280
            }
            else
            {
                return 372
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if currentSelectedTab == 1
        {
            let dicData = arrCareBooking[indexPath.row]
            
            if dicData.status != "deny"
            {
                callUserTripDetailsAPI(bookingId: dicData.bookingId)
                
                viewBG.isHidden = false
                viewPopUp.isHidden = false
                
                viewPopUp.slideIn(from: kFTAnimationBottom, in: viewPopUp.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
            }
            
        }
        
    }
    
    //MARK:- API Calling
    func callViewCareBookingAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(VIEW_CARE_BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrCareBooking.removeAll()
                        self.arrTripInstant.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRCareBookingData(fromDictionary: (obj as? NSDictionary)!)
                                
                                if dicData.status == "deny" || dicData.status == "completed"
                                {
                                    self.arrCareBooking.append(dicData)
                                }
                            }
                            
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    
    func callTripInstantAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(USER_TRIP_INSTANT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrCareBooking.removeAll()
                        self.arrTripInstant.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTripInstantData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTripInstant.append(dicData)
                            }
                            
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callUserTripDetailsAPI(bookingId: String) {
        
        let param = ["booking_id": bookingId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(USER_TRIP_DETAILS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            if arrData!.count > 0 {
                                let dicData = SRUserTripDetailsData(fromDictionary: arrData![0] as! NSDictionary)
                                
                                self.lblRideID.text = "Ride ID : \(dicData.bookingId ?? "")"
                                self.lblDate.text = "\(dicData.eventName ?? "") (\(dicData.bookingDate ?? ""))"
                                self.lblName.text = dicData.athleteName ?? ""
                                
                                self.lblCost.text = "$\(dicData.totalFare ?? "")"
                                self.lblTripTime.text = "\(dicData.startTime ?? "") - \(dicData.endTime ?? "")"
                                
                                self.lblAddressFrom.text = dicData.startLocation ?? ""
                                self.lblAddressTo.text = dicData.endLocation ?? ""
                            }
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callStartTripAPI(bookingId: String,driverId: String) {
        
        let param = ["booking_id": bookingId,"driver_id": driverId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(START_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                           
                            appDelagte.setUpSideMenu()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callCancelTripAPI(bookingId: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["booking_id": bookingId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CANCEL_REQUEST_INSTANT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                           
                            appDelagte.objStart_lat_Map = ""
                            appDelagte.objStart_lang_Map = ""

                            appDelagte.objEnd_lat_Map = ""
                            appDelagte.objEnd_lang_Map = ""
                            
                            appDelagte.isViewMap = false
                            
                            objInvite = data_Message
                            appDelagte.setUpSideMenu()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
