//
//  DriverListTableCell.swift
//  Swoosh Rider
//
//  Created by Mac MIni M1 on 27/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class DriverListTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var imgCheckUncheck: UIImageView!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
