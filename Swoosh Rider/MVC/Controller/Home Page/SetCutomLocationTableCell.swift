//
//  SetCutomLocationTableCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 19/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class SetCutomLocationTableCell: UITableViewCell {

    @IBOutlet weak var lblLName: UILabel!                                                                                                                       
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
