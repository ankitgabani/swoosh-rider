//
//  SOSViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 07/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class SOSViewController: UIViewController {
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var mainViewBg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        setGradientBackground()
        self.mainViewBg.isHidden = true
        
        viewTop.clipsToBounds = true
        viewTop.layer.cornerRadius = 8
        viewTop.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        mainViewBg.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        UIView.transition(with: mainViewBg, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.mainViewBg.isHidden = true
        })
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 245.0/255.0, green: 157.0/255.0, blue: 14.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 0.0/255.0, green: 119.0/255.0, blue: 181.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.viewTop.bounds
        
        self.viewTop.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @IBAction func clickedOK(_ sender: Any) {
        UIView.transition(with: mainViewBg, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.mainViewBg.isHidden = true
            self.navigationController?.popViewController(animated: true)
        })
        
    }
    
    
    @IBAction func clickedSendSos(_ sender: Any) {
        
        callSosRiderAPI()
        
    }
    
    
    // MARK: - API Call
    func callSosRiderAPI() {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","address":"\(appDelagte.objFullAddress)","lat":"\(appDelagte.objLatitude)","lang": "\(appDelagte.objLongitude)"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(SOS_RIDER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            UIView.transition(with: self.mainViewBg, duration: 0.5, options: .transitionCrossDissolve, animations: {
                                self.mainViewBg.isHidden = false
                            })
                            
                        } else {
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
