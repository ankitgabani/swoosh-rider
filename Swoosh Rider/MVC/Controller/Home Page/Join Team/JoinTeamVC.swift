//
//  JoinTeamVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 03/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage

class JoinTeamVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var viewBgJoinRequest: UIView!
    @IBOutlet weak var viewJoinRequestPopUp: UIView!
    
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var mainViewBg: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSport: UILabel!
    @IBOutlet weak var lblClubs: UILabel!
    @IBOutlet weak var lblTemas: UILabel!
    
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var lblAthlete: UILabel!
    
    let dropDown = DropDown()
    var arrAthelet: [SRSportsTypeData] = [SRSportsTypeData]()
    
    var arrAthleteList: [SRAthleteListData] = [SRAthleteListData]()
    
    var objAthleteID = ""
    
    var objTeamID = ""
    
    var strSportName = ""
    var strClubName = ""
    var strTeamName = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBgJoinRequest.isHidden = true
        viewJoinRequestPopUp.clipsToBounds = true
        viewJoinRequestPopUp.layer.cornerRadius = 12
        viewJoinRequestPopUp.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner

        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        mainViewBg.isUserInteractionEnabled = true
        mainViewBg.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.lblSport.text = "Sport : \(self.strSportName)"
        self.lblClubs.text = "Club/Organization : \(self.strClubName)"
        self.lblTemas.text = "Team : \(self.strTeamName)"

        callAthleteListAPI(showIndicator: false)
        
        callSportsTypeAPI()
        
        if selectedTeamFrom == true {
            mainViewBg.isHidden = false
            selectedTeamFrom = false
            self.lblTitle.text = "Sports & Events Categories"
        }
        else {
            mainViewBg.isHidden = true
            selectedTeamFrom = false
            self.lblTitle.text = "Categories - Sports & Activities"
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        mainViewBg.isHidden = true
        selectedTeamFrom = false
    }
    
    @IBAction func clickedOnJoinRequest(_ sender: Any) {
        appDelagte.setUpSideMenu()
    }
    
    
    func setDropDown() {
        
        let arrState = NSMutableArray()
        
        if arrAthleteList.count > 0 {
            let objF = arrAthleteList[0]
            self.lblAthlete.text = "\(objF.firstName ?? "")  \(objF.lastName ?? "")"
            self.objAthleteID = objF.id
            
            var image = objF.image ?? ""
            image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "\(IMG_BASE_URL)\(image)")
            self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
         }
        
        for objState in arrAthleteList {
            arrState.add("\(objState.firstName ?? "")" + " \(objState.lastName ?? "")")
            self.objAthleteID = objState.id
        }
        
        dropDown.dataSource = arrState as! [String]
        dropDown.anchorView = viewTarget
        dropDown.direction = .any
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblAthlete.text = item
            
            if self.arrAthleteList.count > 0 {
                let dicSate = self.arrAthleteList[index]
                print(dicSate.id!)
                self.objAthleteID = dicSate.id
                
                var image = dicSate.image ?? ""
                image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url = URL(string: "\(IMG_BASE_URL)\(image)")
                self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            }
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelagte.setUpSideMenu()
      //  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSelectAthelet(_ sender: Any) {
        dropDown.show()
        
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        mainViewBg.isHidden = true
        selectedTeamFrom = false
    }
    
    @IBAction func clickedReqestToJoin(_ sender: Any) {
        self.callAddTeamsUserAPI()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAthelet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell") as! CategoriesTableViewCell
        
        let dicData = arrAthelet[indexPath.row]
        
        cell.lblName.text = dicData.sportsType ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrAthelet[indexPath.row]

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClubsOrganizationsVC") as! ClubsOrganizationsVC
        vc.sportsID = dicData.id
        vc.strSportName = dicData.sportsType ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - API Call
    func callSportsTypeAPI() {
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(SPORTS_TYPE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrAthelet.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRSportsTypeData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrAthelet.append(dicData)
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callAddTeamsUserAPI() {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "", "athlete_id": objAthleteID, "team_id": objTeamID]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ADD_TEAM_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                                                                                   
                            self.viewBgJoinRequest.isHidden = false

                            self.mainViewBg.isHidden = true
                            selectedTeamFrom = false

                        } else {
                            
                            self.view.makeToast(data_Message)
                            self.mainViewBg.isHidden = true
                            selectedTeamFrom = false
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callAthleteListAPI(showIndicator: Bool) {
        
        if showIndicator == true {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MY_ATHLETE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrAthleteList.removeAll()

                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRAthleteListData(fromDictionary: (obj as? NSDictionary)!)
                                
                                self.arrAthleteList.append(dicData)
                            }
                            
                            self.setDropDown()

                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
}


