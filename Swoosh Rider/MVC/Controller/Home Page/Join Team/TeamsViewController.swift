//
//  TeamsViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 09/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class TeamsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    
    var sportsID = ""
    var clubID = ""
    
    var strSportName = ""
    var strClubName = ""

    var arrTeam: [SRTeamListData] = [SRTeamListData]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        callTeamListAPI()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: HomeViewController.self) {
                    break
                }
            }
            
            self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
            self.navigationController?.popToViewController(controller, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell") as! TeamsTableViewCell
        
        let dicData = arrTeam[indexPath.row]
        
        cell.lblName.text = dicData.teamName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrTeam[indexPath.row]

        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "JoinTeamVC") as? JoinTeamVC {
            controller.objTeamID = dicData.id
            controller.strClubName = self.strClubName
            controller.strSportName = self.strSportName
            controller.strTeamName = dicData.teamName
            let viewcontrollers = self.navigationController?.viewControllers
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: JoinTeamVC.self) {
                    break
                }
            }
            
            selectedTeamFrom = true
            self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
            self.navigationController?.popToViewController(controller, animated: true)
        }
        
    }
    
    
    // MARK: - API Call
    func callTeamListAPI() {
        
      //  APIClient.sharedInstance.showIndicator()

        let param = ["club": clubID ?? "","sports_type": sportsID ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_LIST_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrTeam.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamListData(fromDictionary: (obj as? NSDictionary)!)
                                
                                if dicData.status == "active" {
                                    self.arrTeam.append(dicData)
                                }
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                          //  self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}


