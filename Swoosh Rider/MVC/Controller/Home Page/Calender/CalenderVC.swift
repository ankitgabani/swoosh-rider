//
//  CalenderVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 03/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown

class CalenderVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- IBOutlet
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var viewTarget: UIView!
    
    
    let dropDown = DropDown()
    
    var arrTeamUserList: [SRTeamUserCalenderData] = [SRTeamUserCalenderData]()
    var arrEventCalenderList: [SREventCalenderData] = [SREventCalenderData]()

    
    var objTeamID = ""
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        callTeamUserListAPI()
        
        // Do any additional setup after loading the view.
    }
    
    
    func setDropDown() {
        
        let arrState = NSMutableArray()
        
        if arrTeamUserList.count > 0 {
            let objF = arrTeamUserList[0]
            self.lblTeamName.text = "\(objF.teamName ?? "")"
            self.objTeamID = objF.id
            self.callEventCalenderListAPI(team_id: objF.id)
        }
        
        
        for objState in arrTeamUserList {
            arrState.add(objState.teamName ?? "")
        }
        
        dropDown.dataSource = arrState as! [String]
        dropDown.anchorView = viewTarget
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblTeamName.text = item
            
            if self.arrTeamUserList.count > 0 {
                let dicSate = self.arrTeamUserList[index]
                print(dicSate.id!)
                self.objTeamID = dicSate.id
                  self.callEventCalenderListAPI(team_id: dicSate.id)
            }
            
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Action Method
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSelecteTeam(_ sender: Any) {
        dropDown.show()
    }
    
    
    //MARK:- TableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEventCalenderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "CalenderCell") as! CalenderCell
        
        let dicData = arrEventCalenderList[indexPath.row]
        
        cell.lblMonth.text = dicData.eventMonth ?? ""
        cell.lblDate.text = dicData.eventDate ?? ""
        cell.lblDay.text = dicData.eventDay ?? ""
        
        cell.lblTime.text = "\(dicData.startTime ?? "") - \(dicData.endTime ?? "")"
        cell.lblAthelteName.text = dicData.athleteName ?? ""
        cell.lblEventName.text = dicData.eventName ?? ""
        cell.lblLocation.text = dicData.location ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("CalenderHeaderView", owner: self, options: [:])?.first as! CalenderHeaderView
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalenderEventVC") as! CalenderEventVC
        vc.objData = arrEventCalenderList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    // MARK: - API Call
    func callTeamUserListAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_USER_CALENDER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrTeamUserList.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamUserCalenderData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamUserList.append(dicData)
                            }
                            
                            self.setDropDown()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callEventCalenderListAPI(team_id: String) {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","team_id": team_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(EVENT_CALENDER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrEventCalenderList.removeAll()

                        if message == "success" {
                            
                            self.tblView.isHidden = false
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SREventCalenderData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrEventCalenderList.append(dicData)
                            }
                            

                        } else {
                          //  self.view.makeToast(data_Message)
                            self.tblView.isHidden = true
                            
                        }
                        
                        
                        self.tblView.reloadData()

                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
