//
//  CalenderCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 13/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CalenderCell: UITableViewCell {

    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblAthelteName: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
