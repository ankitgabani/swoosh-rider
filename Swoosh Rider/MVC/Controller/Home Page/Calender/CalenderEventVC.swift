//
//  CalenderEventVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 14/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CalenderEventVC: UIViewController {
    
    
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblAthleteNAme: UILabel!
    
    
    @IBOutlet weak var btnGoing: UIButton!
    @IBOutlet weak var btnMaybe: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    @IBOutlet weak var lblArriveTime: UILabel!
    
    @IBOutlet weak var lblLocaion: UILabel!
    
    var objData: SREventCalenderData?
    
    var objStatus = "going"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpStatus(color: SWOOSH_BACK_BACK)
        
        lblEventName.text = objData?.eventName ?? ""
        
        lblDate.text = "\(objData?.eventDay ?? "") \(objData?.eventMonth ?? "") \(objData?.eventDate ?? ""), \(objData?.year2 ?? "")"
        
        lblTime.text = "\(objData?.startTime ?? "") - \(objData?.endTime ?? "")"
        
        lblAthleteNAme.text = objData?.athleteName ?? ""
        
        lblArriveTime.text = objData?.arrivalTime ?? ""
        
        
        lblLocaion.text = "\(objData?.location ?? "")\n\n\(objData?.locationName ?? "")"
        

        // Do any additional setup after loading the view.
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btno(_ sender: Any) {
        objStatus = "no"
    }
    
    @IBAction func btnGoing(_ sender: Any) {
         objStatus = "going"
    }
    
    @IBAction func btnMaybe(_ sender: Any) {
        objStatus = "maybe"
    }
    
    
    @IBAction func clickedBookRideNow(_ sender: Any) {
        callNotiCoachAPI()
    }
    
    
    func callNotiCoachAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","event_id": objData?.eventId ?? "","event_date": objData?.eventDate ?? "","event_time": lblDate.text!,"status": objStatus,"athlete_id": objData?.athleteId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(NOTIFY_COACH, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    
                    if let responseUser = response {

                        if message == "success" {
                            self.view.makeToast(data_Message)

                        } else {
                            self.view.makeToast(data_Message)
                        }
                       
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
