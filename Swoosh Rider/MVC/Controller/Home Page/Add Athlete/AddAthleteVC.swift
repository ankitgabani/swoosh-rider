//
//  AddAthleteVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 03/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import Alamofire

struct PlaceData{
    var firstName : String?
    var secondName : String?
    var lattitude : Double?
    var longitude : Double?
    var country_name : String?
    var strCity : String?
    var strCuntry : String?
    var strPincode : String?
    var strState : String?
    
    init() {
        firstName = ""
        secondName = ""
        lattitude = 0
        longitude = 0
        country_name = ""
        strCity = ""
        strCuntry = ""
        strPincode = ""
        strState = ""
    }
}

class AddAthleteVC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var viewMainSearch: UIView!
    @IBOutlet weak var txtSearchLocation: UITextField!
    @IBOutlet weak var tblViewLocation: UITableView!
    
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewAddPopup: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtLocation: UITextField!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    
    var imagePicker = UIImagePickerController()
    var selectedImage = UIImage()
    
    let datePicker = UIDatePicker()
    
    var strGender = ""
    var objFiledata = ""
    
    var resultsArray = [PlaceData]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    var objStrCity: String?
    var objStrCuntry: String?
    var objStrPincode: String?
    var objStrState: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMainSearch.isHidden = true
        tblViewLocation.delegate = self
        tblViewLocation.dataSource = self
        
        txtSearchLocation.delegate = self
        
        self.viewBG.isHidden = true
        viewAddPopup.clipsToBounds = true
        viewAddPopup.layer.cornerRadius = 12
        viewAddPopup.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        
        imagePicker.delegate = self
        
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Action Method
    @IBAction func clickedClearSearchLocation(_ sender: Any) {
        self.txtSearchLocation.text = ""
        self.resultsArray.removeAll()
        self.tblViewLocation.reloadData()
        
        if self.txtSearchLocation.text == "" {
            self.viewMainSearch.isHidden = true
        }
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedChoosePhoto(_ sender: Any) {
        openGallary()
    }
    
    @IBAction func clickedAddNewLocation(_ sender: Any) {
        txtSearchLocation.becomeFirstResponder()
        viewMainSearch.isHidden = false
    }
    
    @IBAction func clickedLocation(_ sender: UITextField) {
    }
    
    @IBAction func clickedRemovedLocation(_ sender: Any) {
        txtLocation.text = ""
    }
    
    @IBAction func clickedChooseDate(_ sender: Any) {
    }
    
    
    @IBAction func clickedDateSelected(_ sender: UITextField) {
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        datePicker.maximumDate = Date()
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "CANCEL", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        lblDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    @IBAction func clickedMale(_ sender: Any) {
        
        if btnMale.backgroundColor == UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1) {
            strGender = ""
            btnMale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnMale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            
            btnFemale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnFemale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            btnOther.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnOther.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
        }
        else
        {
            strGender = "male"
            btnMale.backgroundColor = UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1)
            btnMale.setTitleColor(UIColor.white, for: .normal)
            
            btnFemale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnFemale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            btnOther.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnOther.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
        }
        
    }
    
    @IBAction func clickedFemale(_ sender: Any) {
        
        if btnFemale.backgroundColor == UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1)
        {
            strGender = ""
            btnFemale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnFemale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            
            btnMale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnMale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            btnOther.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnOther.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
        }
        else
        {
            strGender = "female"
            btnFemale.backgroundColor = UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1)
            btnFemale.setTitleColor(UIColor.white, for: .normal)
            
            btnMale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnMale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            btnOther.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnOther.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
        }
        
        
    }
    
    @IBAction func clickedOther(_ sender: Any) {
        
        if btnOther.backgroundColor == UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1)
        {
            strGender = ""
            btnOther.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnOther.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            
            btnFemale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnFemale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            btnMale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnMale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            
        }
        else
        {
            strGender = "other"
            btnOther.backgroundColor = UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1)
            btnOther.setTitleColor(UIColor.white, for: .normal)
            
            btnFemale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnFemale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
            btnMale.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            btnMale.setTitleColor(UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1), for: .normal)
        }
        
    }
    
    @IBAction func clickedSubmit(_ sender: Any) {
        
        
        if txtFirstName.text == "" {
            self.view.makeToast("Enter Name !")
        }
        else if txtLastName.text == "" {
            self.view.makeToast("Enter Last Name !")
        }
        else if txtEmail.text == "" {
            self.view.makeToast("Enter Email Id !")
        }
        else if !AppUtilites.isValidEmail(testStr: txtEmail.text!) {
            self.view.makeToast("Invalid Email")
        }
        else if txtPhone.text == "" {
            self.view.makeToast("Enter Phone No !")
        }
        else if txtPhone.text?.count != 10 {
            self.view.makeToast("Enter 10-digit number")
        }
        else if txtLocation.text == "" {
            self.view.makeToast("Enter Address !")
        }
        else if strGender == "" {
            self.view.makeToast("Enter Select Gender")
        }
        else
        {
            callAddAthleteAPI()
        }
        
        
    }
    
    @IBAction func clickedOKPopup(_ sender: Any) {
        self.viewBG.isHidden = true
        appDelagte.setUpSideMenu()
    }
    
    func openGallary()
    {
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
            
            let data = image.pngData()
            let strBase64:String = (data?.base64EncodedString())!
            
            self.objFiledata = strBase64
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
            
            let data = image.pngData()
            let strBase64:String = (data?.base64EncodedString())!
            
            self.objFiledata = strBase64
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - API Call
    func callAddAthleteAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","first_name": txtFirstName.text!,"last_name": txtLastName.text!,"email": txtEmail.text!,"phone": txtPhone.text!,"address": txtLocation.text!,"country": objStrCuntry ?? "","state": objStrState ?? "","city": objStrCity ?? "","pincode": objStrPincode ?? "","birthday": self.lblDate.text!,"gender": self.strGender,"photo": self.objFiledata]
        
        //    print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CREATE_ATHLETE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.viewBG.isHidden = false
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
}

extension AddAthleteVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultsArray != nil{
            return resultsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewLocation.dequeueReusableCell(withIdentifier: "SearchLocationCell") as! SearchLocationCell
        
        let objPlaceData = resultsArray[indexPath.row]
        cell.lblTitleName.text = objPlaceData.firstName
        cell.lblSubTitleName.text = objPlaceData.secondName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objPlaceData = resultsArray[indexPath.row]
        self.txtLocation.text = "\(objPlaceData.firstName ?? ""), \(objPlaceData.secondName ?? "")"
        
        objStrCity = objPlaceData.strCity ?? ""
        objStrCuntry = objPlaceData.strCuntry ?? ""
        objStrPincode = objPlaceData.strPincode ?? ""
        objStrState = objPlaceData.strState ?? ""
        
        self.viewMainSearch.isHidden = true
        self.txtSearchLocation.text = ""
        self.resultsArray.removeAll()
        self.tblViewLocation.reloadData()
    }
}

extension AddAthleteVC : GMSAutocompleteFetcherDelegate{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.resultsArray.removeAll()
        self.tblViewLocation.reloadData()
        for prediction in predictions {
            var placeDataObj = PlaceData()
            if let prediction = prediction as GMSAutocompletePrediction?{
                
                let placeId = prediction.placeID
                
                let placeClient = GMSPlacesClient()
                
                placeClient.lookUpPlaceID(prediction.placeID) { (place, error) -> Void in
                    if let error = error {
                        //show error
                        return
                    }
                    
                    if let place = place {
                        place.coordinate.longitude //longitude
                        place.coordinate.latitude
                        placeDataObj.firstName = prediction.attributedPrimaryText.string
                        placeDataObj.secondName = prediction.attributedSecondaryText?.string
                        placeDataObj.lattitude = place.coordinate.latitude
                        placeDataObj.longitude = place.coordinate.longitude
                        placeDataObj.country_name = place.formattedAddress
                        placeDataObj.strCity = place.name
                        placeDataObj.strState = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
                        placeDataObj.strCuntry = place.addressComponents?.first(where: { $0.type == "country" })?.name
                        placeDataObj.strPincode = place.addressComponents?.first(where: { $0.type == "postal_code" })?.name
                        
                    } else {
                        //show error
                    }
                    self.resultsArray.append(placeDataObj)
                    DispatchQueue.main.async {
                        if self.resultsArray.count == 0 {
                            //self.viewNoLocation.isHidden = false
                            self.tblViewLocation.isHidden = true
                        }
                        else
                        {
                            //self.viewNoLocation.isHidden = true
                            print(333333)
                            
                            if self.txtSearchLocation.text == "" {
                                self.tblViewLocation.isHidden = true
                            }
                            else
                            {
                                self.tblViewLocation.isHidden = false
                            }
                            
                        }
                    }
                    self.tblViewLocation.reloadData()
                }
            }
            
        }
        
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error.localizedDescription)
    }
    
    
}



extension AddAthleteVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            //            let updatedText = text.replacingCharacters(in: textRange,
            //                                                       with: string)
            //            print("Text: \(updatedText)")
            //            print("Range: \(range)&\(text)")
            let mergeString = "\(textField.text!)\(string)"
            var finalString = ""
            if string != ""{
                finalString = mergeString
            }else{
                finalString = String(mergeString.dropLast())
            }
            print("FinalString\(finalString)")
            if finalString == ""{
                resultsArray = [PlaceData]()
                //                gmsFetcher.sourceTextHasChanged("")
                DispatchQueue.main.async {
                    if self.resultsArray.count == 0 {
                        //  self.viewNoLocation.isHidden = false
                        
                        print(111111)
                        DispatchQueue.main.async {
                            self.tblViewLocation.isHidden = true
                        }
                    }
                    else
                    {
                        //  self.viewNoLocation.isHidden = true
                        print(2222222)
                        
                        self.tblViewLocation.isHidden = false
                    }
                    self.tblViewLocation.reloadData()
                }
                
            }else{
                gmsFetcher.sourceTextHasChanged(finalString)
            }
            
        }
        else
        {
            
            
        }
        
        return true
    }
}
