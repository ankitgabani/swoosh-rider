//
//  SearchLocationCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 01/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class SearchLocationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblSubTitleName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
