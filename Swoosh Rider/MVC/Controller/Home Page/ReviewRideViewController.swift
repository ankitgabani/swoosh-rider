//
//  ReviewRideViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 29/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ReviewRideViewController: UIViewController {
    
    @IBOutlet weak var viewRateing: FloatRatingView!
    @IBOutlet weak var lblDateTime: UILabel!
    
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var lblPickAddress: UILabel!
    @IBOutlet weak var lblDropAddress: UILabel!
    
    var booking_id = ""
    var objPickAddress = ""
    var obkDropAddress = ""
    var objDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPickAddress.numberOfLines = 0
        lblDropAddress.numberOfLines = 0

        viewRateing.isHidden = false
        viewRateing.type = .halfRatings
        appDelagte.isYourTripCompleted = false
        // Do any additional setup after loading the view.
    }
     
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedMore(_ sender: Any) {
    }
    
    @IBAction func clickedRate(_ sender: Any) {
        callTeamUserListAPI()
    }
    @IBOutlet weak var clickedHidePopu: UIView!
    
    @IBAction func clickedOKOK(_ sender: Any) {
        viewRateing.isHidden = true
    }
    
    
    func callTeamUserListAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["booking_id": booking_id,"rating": "\(viewRateing.rating ?? 0.0)"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(USER_FEEDBACK, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.view.makeToast(data_Message)
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
