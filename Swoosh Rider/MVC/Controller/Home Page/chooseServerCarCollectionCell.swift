//
//  chooseServerCarCollectionCell.swift
//  Swoosh Rider
//
//  Created by Mac MIni M1 on 23/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class chooseServerCarCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    
    
}
