//
//  HomeViewController.swift
//  Swoosh Rider
//
//  Created by Gabani King on 02/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import MapKit
import CoreLocation
import GoogleMaps
import CoreLocation
import Alamofire
import MaterialActivityIndicator
import SDWebImage
import DLRadioButton

class HomeViewController: UIViewController,GMSMapViewDelegate, DriverLocationDelegate{
    
    @IBOutlet weak var viewBottomStartTrip: UIView!
    @IBOutlet weak var viewBottomStartTripHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewSearchingForDriver: UIView!
    //MARK:- IBOutlet
    @IBOutlet weak var btnCancelDriverAccptedZZ: UIImageView!
    @IBOutlet weak var imgCancelDriverAcceptedZZ: UIButton!
    
    @IBOutlet weak var viewDriverOnTheWay: UIView!
    @IBOutlet weak var viewWallletBal: UIView!
    
    
    @IBOutlet weak var viewTripStartedPopUp: UIView!
    
    @IBOutlet weak var viewDriverOnWay: UIView!
    
    @IBOutlet weak var viewCancelBottom: UIView!
    @IBOutlet weak var viewChatBottom: UIView!
    @IBOutlet weak var viewCallBottom: UIView!
    @IBOutlet weak var viewMapBottom: UIView!
    
    
    @IBOutlet weak var viewDownBottomSide: UIView!
    
    @IBOutlet weak var viewBooking: UIView!
    @IBOutlet weak var viewBooked: UIView!
    
    @IBOutlet weak var viewSelectTripBookDriver: UIView!
    @IBOutlet weak var btnOneWayTrip: DLRadioButton!
    @IBOutlet weak var btnRoundTrip: DLRadioButton!
    @IBOutlet weak var lblSelectVehicle: UILabel!
    @IBOutlet weak var viewTargetSeat: UIView!
    
    @IBOutlet weak var lblMonthPick: UILabel!
    @IBOutlet weak var lblDatePick: UILabel!
    @IBOutlet weak var lblTimePick: UILabel!
    @IBOutlet weak var lblAddressPick: UILabel!
    
    @IBOutlet weak var lblMonthDrop: UILabel!
    @IBOutlet weak var lblDateDrop: UILabel!
    @IBOutlet weak var lblTimeDrop: UILabel!
    @IBOutlet weak var lblAddressDrop: UILabel!
    
    @IBOutlet weak var lblEstimateAmount: UILabel!
    @IBOutlet weak var lblWalletBalance: UILabel!
    @IBOutlet weak var txtSearchDriver: UITextField!
    @IBOutlet weak var tblViewDriverList: UITableView!
    
    
    @IBOutlet weak var viewSelectedTrip: UIView!
    @IBOutlet weak var viewSelectedDriverInfo: UIView!
    @IBOutlet weak var imgSelectedDriverPhotoSS: UIImageView!
    @IBOutlet weak var lblSelectedDriverNameSS: UILabel!
    @IBOutlet weak var lblSelectedRateSS: UILabel!
    @IBOutlet weak var lblSelectedAgeSS: UILabel!
    @IBOutlet weak var lblSelectedVehicleTypeSS: UILabel!
    @IBOutlet weak var lblSelectedVehicleYearSS: UILabel!
    @IBOutlet weak var lblSelectedPlateSS: UILabel!
    @IBOutlet weak var imgSelectedVehiclePhpotp: UIImageView!
    
    
    
    var objCareEventListData: SRCareEventListData?
    var isCurrentPerfectAddress = true
    var isOneWayTrip = "event"
    
    @IBOutlet weak var tblViewEventList: UITableView!
    @IBOutlet weak var tblViewHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var btnTripStatusDD: UIButton!
    @IBOutlet weak var lblUpdateTimeDD: UILabel!
    @IBOutlet weak var lblUpdateLocationDD: UILabel!
    @IBOutlet weak var lblPaymentOptionDD: UILabel!
    @IBOutlet weak var lblBookingTypeDD: UILabel!
    @IBOutlet weak var lblPickupDD: UILabel!
    @IBOutlet weak var lblDestiDD: UILabel!
    @IBOutlet weak var lblEstiPriceDD: UILabel!
    
    @IBOutlet weak var lblCarNameDD: UILabel!
    @IBOutlet weak var lblSeaterDD: UILabel!
    @IBOutlet weak var lblRedMIDD: UILabel!
    @IBOutlet weak var lblPlatedDD: UILabel!
    @IBOutlet weak var lblStarDD: UILabel!
    @IBOutlet weak var imgDriverPhotoDD: UIImageView!
    @IBOutlet weak var lblDriverNameDD: UILabel!
    
    @IBOutlet weak var viewBookingDriverAccpted: UIView!
    @IBOutlet weak var viewBookingAccptedByDriverHieghtConts: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewBookingSucess: UIView!
    @IBOutlet weak var viewBookingAccepted: UIView!
    
    @IBOutlet weak var viewPayementMethod: UIView!
    @IBOutlet weak var lblEstimatedAmount: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var discountHieghtConts: NSLayoutConstraint!
    @IBOutlet weak var viewWalletPay: UIView!
    @IBOutlet weak var lblWalltPay: UILabel!
    
    @IBOutlet weak var viewChooseCarSelectedPopUp: UIView!
    @IBOutlet weak var collectionViewChooseCar: UICollectionView!
    
    @IBOutlet weak var viewLoaderFirstTime: UIView!
    @IBOutlet weak var viewNotServiceAvailble: UIView!
    
    @IBOutlet weak var viewPopUpBookuing: UIView!
    @IBOutlet weak var viewBookingalert: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblNameTitle: UILabel!
    
    @IBOutlet weak var viewBookCareRide: UIView!
    @IBOutlet weak var viewTargetAthlete: UIView!
    @IBOutlet weak var lblAthlete: UILabel!
    
    @IBOutlet weak var viewTargetTeam: UIView!
    @IBOutlet weak var lblTeam: UILabel!
    
    @IBOutlet weak var txtPickupLocation: UITextField!
    
    @IBOutlet weak var viewSetDentination: UIView!
    @IBOutlet weak var lblAway: UILabel!
    @IBOutlet weak var lblWhereAddress: UILabel!
    @IBOutlet weak var lblWalletAmount: UILabel!
    @IBOutlet weak var lblPickUpAddress1: UILabel!
    @IBOutlet weak var lblDropAddress1: UILabel!
    
    @IBOutlet weak var lblWallletAmount2: UILabel!
    @IBOutlet weak var lblPickUpAddress2: UILabel!
    @IBOutlet weak var lblDropAddress2: UILabel!
    @IBOutlet weak var lblAway2: UILabel!
    @IBOutlet weak var lblWherrAddress2: UILabel!
        
    var arrEventDataList: [SRCareEventListData] = [SRCareEventListData]()
    var arrVehicleSeatList: [SRVehicleSeatData] = [SRVehicleSeatData]()
    var arrDriverDeatilsList: [SRDriverDeatilsListData] = [SRDriverDeatilsListData]()
    var objDriverDeatils: SRDriverDeatilsListData?

    
    
    private let indicator = MaterialActivityIndicatorView()
    
    var arrAthleteList: [SRAthleteListData] = [SRAthleteListData]()
    var objAthleteID = ""
    var objTeamID = ""
    
    var arrTeamUserList: [SRTeamUserData] = [SRTeamUserData]()
    
    var arrSelectCarList: [SRSelectCarData] = [SRSelectCarData]()
    var dicSelectedCarData: SRSelectCarData?
    
    var isSelectedCar = false
    
    var dicInstantBookingList: SRInstantBookingData?
    
    let dropDown = DropDown()
    var arrAthelet = [""]
    
    let dropDownTeam = DropDown()
    var arrTeam = [""]
    
    let dropDownVehicleSeat = DropDown()
    var arrVehicleSeat = [""]
    var objVehicleSeat = "1"
    
    var objLatitude = ""
    var objLongitude = ""
    
    var objPickUpLocation_Lat = ""
    var objPickUpLocation_Long = ""
    
    var objDropLocation_Lat = ""
    var objDropsLocation_Long = ""
    
    var objEstimatedAmount = ""
    
    var dicsetDriverDetails = NSDictionary()
    
    var selectedDriver = -1
    
    var locationManager = CLLocationManager()
    let marker: GMSMarker = GMSMarker() // Allocating Marker
    
    
    let sectionInsetsSlider = UIEdgeInsets(top: 0.0,
                                           left: 0.0,
                                           bottom: 0.0,
                                           right: 0.0)
    
    var flowLayoutSlider: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsetsSlider.left * (1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1
        
        _flowLayout.itemSize = CGSize(width: 150, height: 184)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        return _flowLayout
    }
    
    var isDriverOnTheWay = false
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBottomStartTrip.isHidden = true
        viewBottomStartTripHieghtCont.constant = 0
        
        viewSearchingForDriver.isHidden = true
        viewTripStartedPopUp.isHidden = true
        viewDriverOnWay.isHidden = true
        viewDownBottomSide.isHidden = true
        viewBookingAccepted.isHidden = true
        viewBookingDriverAccpted.isHidden = true
        viewDriverOnTheWay.isHidden = true
        
        self.viewBooking.isHidden = false
        self.viewBooked.isHidden = true
        
        self.txtSearchDriver.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)

        viewSelectedDriverInfo.isHidden = true
        viewSelectTripBookDriver.isHidden = true
        btnOneWayTrip.isSelected = true
        btnRoundTrip.isSelected = false
        
        
        tblViewDriverList.delegate = self
        tblViewDriverList.dataSource = self
        
        tblViewEventList.delegate = self
        tblViewEventList.dataSource = self
        
        
        viewBookingSucess.isHidden = true
        
        collectionViewChooseCar.delegate = self
        collectionViewChooseCar.dataSource = self
        self.collectionViewChooseCar.collectionViewLayout = flowLayoutSlider
        
        viewChooseCarSelectedPopUp.isHidden = true
        self.viewNotServiceAvailble.isHidden = true
        self.viewPayementMethod.isHidden = true
        
        viewPopUpBookuing.clipsToBounds = true
        viewPopUpBookuing.layer.cornerRadius = 12
        viewPopUpBookuing.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        
        viewBookingalert.isHidden = true
        
        self.txtPickupLocation.text = ""
        self.viewSetDentination.isHidden = true
        
        self.mapView.clear()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        self.mapView.delegate = self
        
        let dateComponents = Calendar.current.dateComponents([.hour], from: Date())
        var greetingString = String()
        
        if let hour = dateComponents.hour {
            switch hour {
            case 0..<12:
                greetingString = "Good Morning"
            case 12..<17:
                greetingString = "Good Afternoon"
            default:
                greetingString = "Good Evening"
            }
        }
        
        if let name = appDelagte.dicLoginUserDetails?.name.components(separatedBy: " ") {
            let objName = name[0] as? String
            
            self.lblNameTitle.text = "\(greetingString), \(objName ?? "")"
        }
        
        if isOpenSideMenu == true {
            isOpenSideMenu = false
            self.sideMenuController?.showLeftView()
        }
        
        viewBookCareRide.isHidden = true
        
        setUpStatus(color: SWOOSH_BACK)
        
        if objInvite != ""
        {
            self.view.makeToast(objInvite)
            objInvite = ""
        }
        
        if appDelagte.isViewMap == true
        {
            
            self.objPickUpLocation_Lat =  appDelagte.objStart_lat_Map
            self.objPickUpLocation_Long = appDelagte.objStart_lang_Map
            
            self.objDropLocation_Lat = appDelagte.objEnd_lat_Map
            self.objDropsLocation_Long = appDelagte.objEnd_lang_Map
            showRoute()
        }
        
        callViewWalletBalaceAPI()
        callVehicleSeatAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setDriverAllDetail(notification:)), name: NSNotification.Name(rawValue: "setDriverDetails"), object: nil)
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .down
        self.viewBookingDriverAccpted.addGestureRecognizer(swipeDown)
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {

        callViewDriverListEventAPI(keyword: textfield.text ?? "")
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
            case .down:
                print("Swiped down")
                
                if appDelagte.isFromCareRideViewMenu == false
                {
                    self.viewBookingDriverAccpted.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                        self.viewBookingDriverAccpted.isHidden = true
                    }
                }
              
            case .left:
                print("Swiped left")
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if appDelagte.isFromCareRideViewMenu == true
        {
            self.viewDownBottomSide.isHidden = true
            self.viewBookingDriverAccpted.isHidden = false
            callBooking_data_API()
        }
        
    }
    
    func setupActivityIndicatorView() {
        viewLoaderFirstTime.addSubview(indicator)
        indicator.color = UIColor.darkGray
        setupActivityIndicatorViewConstraints()
    }
    
    func setupActivityIndicatorViewConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: viewLoaderFirstTime.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: viewLoaderFirstTime.centerYAnchor).isActive = true
    }
    
    @objc func setDriverAllDetail(notification:NSNotification)
    {
        let dicDriver = notification.object as! NSDictionary

        if appDelagte.isDriverOnTheWay == true
        {
            self.viewBottomStartTrip.isHidden = true
            self.viewBottomStartTripHieghtCont.constant = 0
            
            DriverOnTheWay_Action()

        }
        else if appDelagte.isDriverisArried == true
        {
            DriverOnArrived_Action()
            viewBottomStartTrip.isHidden = false
            viewBottomStartTripHieghtCont.constant = 75
        }
        else if appDelagte.isYourTripStarted == true
        {
            self.viewBottomStartTrip.isHidden = true
            self.viewBottomStartTripHieghtCont.constant = 0
            
            DriverOnTrip_Started_Action()
        }
        else if appDelagte.isYourTripCompleted == true
        {
            self.viewBottomStartTrip.isHidden = true
            self.viewBottomStartTripHieghtCont.constant = 0
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewRideViewController") as! ReviewRideViewController
            vc.booking_id = dicDriver.value(forKey: "booking_id") as! String
            vc.objPickAddress = dicDriver.value(forKey: "pick_address") as! String
            vc.obkDropAddress = dicDriver.value(forKey: "drop_address") as! String
            vc.objDate = "\(dicDriver.value(forKey: "booking_date") as! String)\n(\(dicDriver.value(forKey: "booking_time") as! String))"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            self.viewBottomStartTrip.isHidden = true
            self.viewBottomStartTripHieghtCont.constant = 0
            
            self.viewSearchingForDriver.isHidden = true
            btnTripStatusDD.setTitle("BOOKED", for: .normal)
            viewBookingAccepted.isHidden = false
        }
        
        btnCancelDriverAccptedZZ.isHidden = true
        imgCancelDriverAcceptedZZ.isHidden = true
        
        viewBookingDriverAccpted.isHidden = false

        
        self.dicsetDriverDetails = dicDriver
        
        appDelagte.objStart_lat_Map = dicDriver.value(forKey: "pick_lat") as! String
        appDelagte.objStart_lang_Map = dicDriver.value(forKey: "pick_lang") as! String
        
        appDelagte.objEnd_lat_Map = dicDriver.value(forKey: "drop_lat") as! String
        appDelagte.objEnd_lang_Map = dicDriver.value(forKey: "drop_lang") as! String
        
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = df.string(from: date)
        
        self.lblUpdateTimeDD.text = "Update Time: \(dateString)"
        
        let pickUp_Lat = dicDriver.value(forKey: "pick_lat") as? String
        let pickUp_Long = dicDriver.value(forKey: "pick_lang") as? String
        
        self.lblUpdateLocationDD.text = "Update Location: \(Double(pickUp_Lat ?? "")?.round(to: 7) ?? 0.0) / \(Double(pickUp_Long ?? "")?.round(to: 7) ?? 0.0)"
        
        let payment_method = dicDriver.value(forKey: "payment_method") as? String
        self.lblPaymentOptionDD.text = "\(payment_method ?? "") (SwooshCash)"
        
        let booking_type = dicDriver.value(forKey: "booking_type") as? String
        self.lblBookingTypeDD.text = "\(booking_type ?? "")"
        
        let pick_address = dicDriver.value(forKey: "pick_address") as? String
        self.lblPickupDD.text = "\(pick_address ?? "")"
        
        let drop_address = dicDriver.value(forKey: "drop_address") as? String
        self.lblDestiDD.text = "\(drop_address ?? "")"
        
        let estimated_fare = dicDriver.value(forKey: "estimated_fare") as? String
        self.lblEstiPriceDD.text = "\(estimated_fare ?? "")"
        
        let driver_car = dicDriver.value(forKey: "driver_car") as? String
        self.lblCarNameDD.text = "\(driver_car ?? "")"
        
        let driver_vehicle_seats = dicDriver.value(forKey: "driver_vehicle_seats") as? String
        self.lblSeaterDD.text = "\(driver_vehicle_seats ?? "")"
        
        let driver_name = dicDriver.value(forKey: "driver_name") as? String
        self.lblDriverNameDD.text = "\(driver_name ?? "")"
        
        let driver_caryear = dicDriver.value(forKey: "driver_caryear") as? String
        let driver_car_color = dicDriver.value(forKey: "driver_car_color") as? String
        let driver_carmodel = dicDriver.value(forKey: "driver_carmodel") as? String
        
        self.lblRedMIDD.text = "[ \(driver_caryear ?? "") \(driver_car_color ?? "") \(driver_carmodel ?? "")]"
        
        let driver_license_plate = dicDriver.value(forKey: "driver_license_plate") as? String
        self.lblPlatedDD.text = "\(driver_license_plate ?? "")"
        
        let driver_rating = dicDriver.value(forKey: "driver_rating") as? String
        self.lblStarDD.text = "\(driver_rating ?? "")"
        
        var image = dicDriver.value(forKey: "driver_photo") as? String
        image = image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(image ?? "")")
        imgDriverPhotoDD.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgDriverPhotoDD.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
    }
    
    @objc func setDriverAllDetailFromCareRideMenu(dicDriver: NSDictionary)
    {

        btnCancelDriverAccptedZZ.isHidden = false
        imgCancelDriverAcceptedZZ.isHidden = false
        
        self.dicsetDriverDetails = dicDriver
        btnTripStatusDD.setTitle("View", for: .normal)
        appDelagte.objStart_lat_Map = dicDriver.value(forKey: "pick_lat") as! String
        appDelagte.objStart_lang_Map = dicDriver.value(forKey: "pick_lang") as! String
        
        appDelagte.objEnd_lat_Map = dicDriver.value(forKey: "drop_lat") as! String
        appDelagte.objEnd_lang_Map = dicDriver.value(forKey: "drop_lang") as! String
        
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = df.string(from: date)
        
        self.lblUpdateTimeDD.text = "Update Time: \(dateString)"
        
        let pickUp_Lat = dicDriver.value(forKey: "pick_lat") as? String
        let pickUp_Long = dicDriver.value(forKey: "pick_lang") as? String
        
        self.lblUpdateLocationDD.text = "Update Location: \(Double(pickUp_Lat ?? "")?.round(to: 7) ?? 0.0) / \(Double(pickUp_Long ?? "")?.round(to: 7) ?? 0.0)"
        
        let payment_method = dicDriver.value(forKey: "payment_method") as? String
        self.lblPaymentOptionDD.text = "\(payment_method ?? "") (SwooshCash)"
        
        let booking_type = dicDriver.value(forKey: "booking_type") as? String
        self.lblBookingTypeDD.text = "\(booking_type ?? "")"
        
        let pick_address = dicDriver.value(forKey: "pick_address") as? String
        self.lblPickupDD.text = "\(pick_address ?? "")"
        
        let drop_address = dicDriver.value(forKey: "drop_address") as? String
        self.lblDestiDD.text = "\(drop_address ?? "")"
        
        let estimated_fare = dicDriver.value(forKey: "estimated_fare") as? String
        self.lblEstiPriceDD.text = "\(estimated_fare ?? "")"
        
        let driver_car = dicDriver.value(forKey: "driver_car") as? String
        self.lblCarNameDD.text = "\(driver_car ?? "")"
        
        let driver_vehicle_seats = dicDriver.value(forKey: "driver_vehicle_seats") as? String
        self.lblSeaterDD.text = "\(driver_vehicle_seats ?? "")"
        
        let driver_name = dicDriver.value(forKey: "driver_name") as? String
        self.lblDriverNameDD.text = "\(driver_name ?? "")"
        
        let driver_caryear = dicDriver.value(forKey: "driver_caryear") as? String
        let driver_car_color = dicDriver.value(forKey: "driver_car_color") as? String
        let driver_carmodel = dicDriver.value(forKey: "driver_carmodel") as? String
        
        self.lblRedMIDD.text = "[ \(driver_caryear ?? "") \(driver_car_color ?? "") \(driver_carmodel ?? "")]"
        
        let driver_license_plate = dicDriver.value(forKey: "driver_license_plate") as? String
        self.lblPlatedDD.text = "\(driver_license_plate ?? "")"
        
        let driver_rating = dicDriver.value(forKey: "driver_rating") as? String
        self.lblStarDD.text = "\(driver_rating ?? "")"
        
        var image = dicDriver.value(forKey: "driver_photo") as? String
        image = image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(image ?? "")")
        imgDriverPhotoDD.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgDriverPhotoDD.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
    }
    
    func showRoute()
    {
        var origin = ""
        var destination = ""
        
        origin = "\(self.objPickUpLocation_Lat),\(self.objPickUpLocation_Long)"
        destination = "\(self.objDropLocation_Lat),\(self.objDropsLocation_Long)"
        
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(PROVIDE_API_KEY)"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                DispatchQueue.main.async {
                    
                    do{
                        
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                        let routes = json["routes"] as! NSArray
                        self.mapView.clear()
                        
                        let sourceView = UIView()
                        sourceView.frame = CGRect.init(x: 0, y: 0, width: 22, height: 22)
                        sourceView.layer.cornerRadius = 11
                        sourceView.backgroundColor = UIColor(red: 0/255, green: 282/25, blue: 69/255, alpha: 1)
                        
                        let markerSource = GMSMarker()
                        markerSource.position = CLLocationCoordinate2D.init(latitude: Double(self.objPickUpLocation_Lat) ?? 0.0, longitude: Double(self.objPickUpLocation_Long) ?? 0.0)
                        markerSource.map = nil
                        markerSource.iconView = sourceView
                        markerSource.map = self.mapView
                        
                        let destinationView = UIView()
                        destinationView.frame = CGRect.init(x: 0, y: 0, width: 22, height: 22)
                        destinationView.backgroundColor = UIColor(red: 252/255, green: 0/25, blue: 90/255, alpha: 1)
                        
                        let markerDestination = GMSMarker()
                        markerDestination.position = CLLocationCoordinate2D.init(latitude: Double(self.objDropLocation_Lat) ?? 0.0, longitude: Double(self.objDropsLocation_Long) ?? 0.0)
                        markerDestination.map = nil
                        markerDestination.iconView = destinationView
                        markerDestination.map = self.mapView
                        
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 10
                            polyline.strokeColor = UIColor.orange
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            
                            polyline.map = self.mapView
                        }
                        
                    }catch let error as NSError{
                        print("error:\(error)")
                    }
                }
            }
        }).resume()
    }
    
    //MARK:- setDropDown
    func setDropDownVehicleSeat() {
        
        let arrState = NSMutableArray()
        
        if arrVehicleSeatList.count > 0 {
            arrState.add("All Vehicle")
            for objState in arrVehicleSeatList {
                
                if objState.status == "active"
                {
                    arrState.add("\(objState.seats ?? "")")
                }
            }
        }
        
        dropDownVehicleSeat.dataSource = arrState as! [String]
        dropDownVehicleSeat.anchorView = viewTargetSeat
        dropDownVehicleSeat.direction = .any
        dropDownVehicleSeat.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            self.lblAthlete.text = item
            
            if index != 0
            {
                let dicSate = self.arrVehicleSeatList[index-1]
                self.objVehicleSeat = dicSate.id
                self.lblSelectVehicle.text = item
            }
            else
            {
                self.objVehicleSeat = "1"
                self.lblSelectVehicle.text = "All Vehicle"
            }
        }
        dropDownVehicleSeat.bottomOffset = CGPoint(x: 0, y: viewTargetSeat.bounds.height)
        dropDownVehicleSeat.topOffset = CGPoint(x: 0, y: viewTargetSeat.bounds.height)
        dropDownVehicleSeat.dismissMode = .onTap
        dropDownVehicleSeat.textColor = UIColor.darkGray
        dropDownVehicleSeat.backgroundColor = UIColor.white
        dropDownVehicleSeat.selectionBackgroundColor = UIColor.clear
        dropDownVehicleSeat.reloadAllComponents()
    }
    
    func setDropDown() {
        
        let arrState = NSMutableArray()
        
        if arrAthleteList.count > 0 {
            let objF = arrAthleteList[0]
            self.lblAthlete.text = "\(objF.firstName ?? "")  \(objF.lastName ?? "")"
            self.objAthleteID = objF.id
            
            for objState in arrAthleteList {
                arrState.add("\(objState.firstName ?? "")" + " \(objState.lastName ?? "")")
                self.objAthleteID = objState.id
            }
            
            callTeamUserListAPI(athlete_id: objF.id)
        }
        
        dropDown.dataSource = arrState as! [String]
        dropDown.anchorView = viewTargetAthlete
        dropDown.direction = .any
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            self.lblAthlete.text = item
            
            if self.arrAthleteList.count > 0 {
                let dicSate = self.arrAthleteList[index]
                //  print(dicSate.id!)
                self.objAthleteID = dicSate.id
                
                callTeamUserListAPI(athlete_id: dicSate.id)
                
            }
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTargetAthlete.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTargetAthlete.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    func setDropDownTeam() {
        
        let arrState = NSMutableArray()
        
        if arrTeamUserList.count > 0 {
            let objF = arrTeamUserList[0]
            self.lblTeam.text = "\(objF.teamName ?? "")"
            self.objTeamID = objF.id
            
            for objState in arrTeamUserList {
                arrState.add("\(objState.teamName ?? "")")
            }
            
            callEventUserAPI()
            
        }
        else
        {
            self.lblTeam.text = ""
            self.objTeamID = ""
        }
        
        dropDownTeam.dataSource = arrState as! [String]
        dropDownTeam.anchorView = viewTargetTeam
        dropDownTeam.direction = .any
        dropDownTeam.selectionAction = { [unowned self] (index: Int, item: String) in
            //   print("Selected item: \(item) at index: \(index)")
            self.lblTeam.text = item
            
            if self.arrTeamUserList.count > 0 {
                let dicSate = self.arrTeamUserList[index]
                //    print(dicSate.id!)
                self.objTeamID = dicSate.id
                
                self.callEventUserAPI()
            }
            
        }
        dropDownTeam.bottomOffset = CGPoint(x: 0, y: viewTargetTeam.bounds.height)
        dropDownTeam.topOffset = CGPoint(x: 0, y: viewTargetTeam.bounds.height)
        dropDownTeam.dismissMode = .onTap
        dropDownTeam.textColor = UIColor.darkGray
        dropDownTeam.backgroundColor = UIColor.white
        dropDownTeam.selectionBackgroundColor = UIColor.clear
        dropDownTeam.reloadAllComponents()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func DriverOnTheWay_Action()
    {
        isDriverOnTheWay = true
        viewDriverOnTheWay.isHidden = false
        
        viewCancelBottom.isHidden = true
        viewChatBottom.isHidden = false
        viewCallBottom.isHidden = false
        viewMapBottom.isHidden = false
        btnTripStatusDD.setTitle("Driver On Way", for: .normal)
    }
    
    func DriverOnArrived_Action()
    {
        viewDriverOnWay.isHidden = false
        
        viewCancelBottom.isHidden = true
        viewChatBottom.isHidden = false
        viewCallBottom.isHidden = false
        viewMapBottom.isHidden = false
        btnTripStatusDD.setTitle("Driver On Way", for: .normal)
    }
    
    func DriverOnTrip_Started_Action()
    {
        viewDriverOnWay.isHidden = true
        viewTripStartedPopUp.isHidden = false

        viewCancelBottom.isHidden = true
        viewChatBottom.isHidden = true
        viewCallBottom.isHidden = false
        viewMapBottom.isHidden = false
        btnTripStatusDD.setTitle("On Way", for: .normal)
    }
        
    //MARK:- Action Method
    
    @IBAction func clickedStartTripBottom(_ sender: Any) {
        
        let book_id = self.dicsetDriverDetails.value(forKey: "booking_id") as? String
        let driver_id = self.dicsetDriverDetails.value(forKey: "driver_id") as? String
        callStartTripAPI(bookingId: book_id ?? "", driverId: driver_id ?? "")
    }
    
    @IBAction func clickedTripStartedPopup(_ sender: Any) {
        viewTripStartedPopUp.isHidden = true
        appDelagte.isYourTripStarted = false
    }
    
    @IBAction func clickedDriverArrried(_ sender: Any) {
        viewDriverOnWay.isHidden = true
        appDelagte.isDriverisArried = false

    }
    
    @IBAction func clickedOnDriverOnTheWay(_ sender: Any)
    {
        viewDriverOnTheWay.isHidden = true
        appDelagte.isDriverOnTheWay = false
    }
    
    // Bottom to top
    @IBAction func clickedUnhideShowTopPopupFromView(_ sender: Any)
    {
        self.viewDownBottomSide.isHidden = true
        viewBookingDriverAccpted.slideIn(from: kFTAnimationBottom, in: viewBookingDriverAccpted.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
    }
    
    // tpo to bottom
    @IBAction func clickedUnHideShowBottomPopupFromView(_ sender: Any)
    {
        if appDelagte.isFromCareRideViewMenu == true
        {
            self.viewBookingDriverAccpted.slideOut(to: kFTAnimationBottom, in: self.viewBookingDriverAccpted.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.viewDownBottomSide.isHidden = false
            }
        }
        else
        {
            self.viewBookingDriverAccpted.slideOut(to: kFTAnimationBottom, in: self.viewBookingDriverAccpted.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        }
        
    }
    
    @IBAction func clickedHideMainDriverAccPopupFromView(_ sender: Any)
    {
        self.viewDownBottomSide.isHidden = true
        self.viewBookingDriverAccpted.isHidden = true
        appDelagte.isFromCareRideViewMenu = false
    }
    
    @IBAction func clickedInstantBookNow(_ sender: Any)
    {
        if selectedDriver == -1
        {
            self.view.makeToast("Not booked, Select driver")
        }
        else
        {
            callCare_BookingAPI()
        }
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        self.viewSelectedTrip.isHidden = false
        viewSelectedDriverInfo.isHidden = true
        self.viewSelectTripBookDriver.isHidden = true

    }
    
    @IBAction func clickedBackDriverProfile(_ sender: Any) {
        viewSelectedDriverInfo.isHidden = true
        self.viewSelectedTrip.isHidden = false
        self.viewSelectTripBookDriver.isHidden = false
    }
    
    @IBAction func clickedRefresh(_ sender: Any) {
        
        if isCurrentPerfectAddress == true
        {
            isCurrentPerfectAddress = false
            self.lblAddressPick.text = self.txtPickupLocation.text ?? ""
            self.lblAddressDrop.text = self.objCareEventListData?.location ?? ""
        }
        else
        {
            isCurrentPerfectAddress = true
            self.lblAddressPick.text = self.objCareEventListData?.location ?? ""
            self.lblAddressDrop.text = self.txtPickupLocation.text ?? ""
        }
        
    }
    
    @IBAction func clickedSelectedVehicleDrop(_ sender: Any) {
        dropDownVehicleSeat.show()
    }
    
    @IBAction func clickedHideSelectedTrip(_ sender: Any) {
        self.viewSelectTripBookDriver.isHidden = true
    }
    
    @IBAction func clickedOneWayTrip(_ sender: Any)
    {
        isOneWayTrip = "event"
        btnOneWayTrip.isSelected = true
        btnRoundTrip.isSelected = false
    }
    
    @IBAction func clickedRoundTrip(_ sender: Any)
    {
        isOneWayTrip = "home"
        btnOneWayTrip.isSelected = false
        btnRoundTrip.isSelected = true
    }
    
    
    @IBAction func clickedCancelBookedDD(_ sender: Any)
    {
        let alert = UIAlertController(title: "", message: "Are you sure, You wanted to Cancel that Booking?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "No", style: .default, handler: { action in
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            let booking_id = self.dicsetDriverDetails.value(forKey: "booking_id") as! String
            
            self.callCancelTripAPI(bookingId: booking_id)
            
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    @IBAction func clickedChatWithDriverDD(_ sender: Any)
    {
        let driver_id = self.dicsetDriverDetails.value(forKey: "driver_id") as! String
        let driver_name = self.dicsetDriverDetails.value(forKey: "driver_name") as! String
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChattingViewController") as! ChattingViewController
        vc.objDriver_id = driver_id
        vc.objDriver_name = driver_name
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedCallToDriverDD(_ sender: Any)
    {
        let phoneCall = dicsetDriverDetails.value(forKey: "driver_phone") as! String
        dialNumber(number: phoneCall)
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    
    @IBAction func clickedViewMapDriverDD(_ sender: Any) {
                
        if appDelagte.isFromCareRideViewMenu == false
        {
            self.viewBookingDriverAccpted.slideOut(to: kFTAnimationBottom, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                self.viewBookingDriverAccpted.isHidden = true
            }
        }
        
        self.objPickUpLocation_Lat = dicsetDriverDetails.value(forKey: "pick_lat") as! String
        self.objPickUpLocation_Long = dicsetDriverDetails.value(forKey: "pick_lang") as! String
        
        self.objDropLocation_Lat = dicsetDriverDetails.value(forKey: "drop_lat") as! String
        self.objDropsLocation_Long = dicsetDriverDetails.value(forKey: "drop_lang") as! String
        
        showRoute()
    }
    
    @IBAction func clickedBookingAccepted(_ sender: Any) {
        viewBookingAccepted.isHidden = true
    }
    
    @IBAction func clickedHideSucuess(_ sender: Any) {
        viewBookingSucess.isHidden = true
    }
    
    func onDriverLocationReady(pickUpLocation_Name: String, pickUpLocation_Lat: String, pickUpLocation_Long: String, dropLocation_Name: String, dropLocation_Lat: String, dropsLocation_Long: String) {
        
        setupActivityIndicatorView()
        
        indicator.startAnimating()
        
        self.viewSetDentination.isHidden = false
        self.lblPickUpAddress1.text = pickUpLocation_Name
        self.lblDropAddress1.text = dropLocation_Name
        self.lblWhereAddress.text = dropLocation_Name
        
        self.lblPickUpAddress2.text = pickUpLocation_Name
        self.lblDropAddress2.text = dropLocation_Name
        self.lblWherrAddress2.text = dropLocation_Name
        
        
        self.objPickUpLocation_Lat = pickUpLocation_Lat
        self.objPickUpLocation_Long = pickUpLocation_Long
        
        self.objDropLocation_Lat = dropLocation_Lat
        self.objDropsLocation_Long = dropsLocation_Long
        
        
        let myLocation = CLLocation(latitude: Double(pickUpLocation_Lat)!, longitude: Double(pickUpLocation_Long)!)
        
        //My Next Destination
        let myNextDestination = CLLocation(latitude: Double(dropLocation_Lat)!, longitude: Double(dropsLocation_Long)!)
        
        //Finding my distance to my next destination (in km)
        let distance = myLocation.distance(from: myNextDestination) / 1000
        
        self.lblAway.text = "\(distance.round(to:2)) km"
        self.lblAway2.text = "\(distance.round(to:2)) km"
        
        self.showRoute()
        
        callSelectCarAPI(pick_lat1: pickUpLocation_Lat, pick_lang1: pickUpLocation_Long, drop_lat1: dropLocation_Lat, drop_lang1: dropsLocation_Long, pick_address1: pickUpLocation_Name, drop_address1: dropLocation_Name)
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        self.viewSetDentination.isHidden = true
    }
    
    @IBAction func clickedHideNotService(_ sender: Any) {
        self.mapView.clear()
        self.viewNotServiceAvailble.isHidden = true
        self.viewSetDentination.isHidden = true
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        self.mapView.delegate = self
    }
    
    @IBAction func clickedChooseSelectedCar(_ sender: Any) {
        self.viewNotServiceAvailble.isHidden = true
        self.viewSetDentination.isHidden = true
        self.viewChooseCarSelectedPopUp.isHidden = true
    }
    
    @IBAction func clickedConfirmBoookingChooseCAr(_ sender: Any) {
        
        if isSelectedCar == true
        {
            let dicData = dicSelectedCarData
            callInstantBookingAPI(car_id: dicData?.cartypeTableId ?? "")
        }
        else
        {
            let dicData = arrSelectCarList[0]
            dicSelectedCarData = dicData
            callInstantBookingAPI(car_id: dicData.cartypeTableId ?? "")
        }
    }
    
    @IBAction func clickedChooseWalletPay(_ sender: Any) {
        self.viewWalletPay.backgroundColor = UIColor(red: 245/255, green: 157/255, blue: 15/255, alpha: 1)
        self.lblWalltPay.textColor = UIColor.white
    }
    
    @IBAction func clickedBackFromPayememtMethod(_ sender: Any) {
        self.viewNotServiceAvailble.isHidden = true
        self.viewSetDentination.isHidden = true
        self.viewChooseCarSelectedPopUp.isHidden = false
        self.viewPayementMethod.isHidden = true
    }
    
    @IBAction func clickedSendRequest(_ sender: Any)
    {
        if self.lblWalltPay.textColor == UIColor.white
        {
            callBalanceCheckAPI()
        }
        else
        {
            self.view.makeToast("Please Select Payment Type")
        }
    }
    
    
    @IBAction func clickedTeam(_ sender: Any) {
        dropDownTeam.show()
    }
    
    @IBAction func clickedSelecteAthlete(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func clickedBookCareRide(_ sender: Any) {
        
        callAthleteListAPI(showIndicator: false)
        self.txtPickupLocation.text = appDelagte.objCurrentAddress
        viewBookCareRide.isHidden = false
    }
    
    @IBAction func clickedClearLocation(_ sender: Any) {
        self.txtPickupLocation.text = ""
    }
    
    @IBAction func clickedHome(_ sender: Any) {
        viewBookCareRide.isHidden = true
    }
    
    
    @IBAction func clickedInstantRide(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetDestinationVC") as! SetDestinationVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedPhoneCall(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedAddAthlete(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAthleteVC") as! AddAthleteVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedJoinTeam(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "JoinTeamVC") as! JoinTeamVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedCalender(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedMenu(_ sender: Any) {
    }
    
    @IBAction func clickedOKBooking(_ sender: Any) {
        viewBookingalert.isHidden = true
        
    }
    
    //MARK:- API Calling
    
    func callCancelTripAPI(bookingId: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["booking_id": bookingId]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CANCEL_REQUEST_INSTANT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.viewBookingDriverAccpted.isHidden = true
                            self.viewBookingAccepted.isHidden = true
                            appDelagte.isViewMap = false
                            
                            appDelagte.objStart_lat_Map = ""
                            appDelagte.objStart_lang_Map = ""
                            
                            appDelagte.objEnd_lat_Map = ""
                            appDelagte.objEnd_lang_Map = ""
                            
                            self.mapView.clear()
                            
                            if CLLocationManager.locationServicesEnabled(){
                                self.locationManager.delegate = self
                                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                                self.locationManager.requestWhenInUseAuthorization()
                                self.locationManager.requestAlwaysAuthorization()
                                self.locationManager.startUpdatingLocation()
                            }
                            self.mapView.delegate = self
                            
                            self.view.makeToast(data_Message)
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callAthleteListAPI(showIndicator: Bool) {
        
        if showIndicator == true {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MY_ATHLETE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrAthleteList.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRAthleteListData(fromDictionary: (obj as? NSDictionary)!)
                                
                                self.arrAthleteList.append(dicData)
                            }
                            
                            self.setDropDown()
                            
                        } else {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callViewWalletBalaceAPI() {
        
        let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(WALLET_BALANCE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? NSDictionary
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            let wallet_id = data_Message?.value(forKey: "wallet_id") as? String
                            let balance = data_Message?.value(forKey: "balance") as? String
                            self.lblWallletAmount2.text = "Wallet Amount: CAD$\(balance ?? "")"
                            self.lblWalletAmount.text = "Wallet Amount: CAD$\(balance ?? "")"
                            self.lblWalletBalance.text = "Wallet CAD$\(balance ?? "")"
                            self.viewWallletBal.backgroundColor = UIColor(red: 1/255, green: 128/255, blue: 27/255, alpha: 1)
                            
                        }
                        else
                        {
                            self.lblWallletAmount2.text = "Wallet Amount: $ 0.00"
                            self.lblWalletAmount.text = "Wallet Amount: $ 0.00"
                            self.lblWalletBalance.text = "Low $, Fund Wallet"
                            self.viewWallletBal.backgroundColor = UIColor(red: 136/255, green: 4/255, blue: 15/255, alpha: 1)
                        }
                        
                    } else
                    {
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callTeamUserListAPI(athlete_id: String) {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","athlete_id": athlete_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TEAM_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrTeamUserList.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRTeamUserData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrTeamUserList.append(dicData)
                            }
                            
                            self.setDropDownTeam()
                            
                        } else {
                            
                            self.viewBookingalert.isHidden = false
                            self.objTeamID = ""
                            self.arrTeamUserList.removeAll()
                            self.setDropDownTeam()
                            
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callEventUserAPI() {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","team_id": objTeamID]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(EVENT_USER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            self.arrEventDataList.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRCareEventListData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrEventDataList.append(dicData)
                            }
                            
                            DispatchQueue.main.async {
                                if self.arrEventDataList.count < 3
                                {
                                    self.tblViewHieghtCont.constant = CGFloat(self.arrEventDataList.count * 90)
                                }
                                else
                                {
                                    self.tblViewHieghtCont.constant = CGFloat(2 * 90)
                                }
                            }
                            
                            self.tblViewEventList.reloadData()
                        }
                        else
                        {
                            self.arrEventDataList.removeAll()
                            self.tblViewEventList.reloadData()
                            DispatchQueue.main.async {
                                self.tblViewHieghtCont.constant = CGFloat(self.arrEventDataList.count * 90)
                            }
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.arrEventDataList.removeAll()
                        self.tblViewEventList.reloadData()
                        DispatchQueue.main.async {
                            self.tblViewHieghtCont.constant = CGFloat(self.arrEventDataList.count * 90)
                        }
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callSelectCarAPI(pick_lat1: String,pick_lang1: String,drop_lat1: String,drop_lang1: String,pick_address1: String,drop_address1: String) {
        
        let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? "","pick_lat": pick_lat1,"pick_lang": pick_lang1,"drop_lat": drop_lat1,"drop_lang": drop_lang1,"ride_type":"1","pick_address": pick_address1,"drop_address": drop_address1]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(SELECT_CAR, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    self.indicator.stopAnimating()
                    
                    self.arrSelectCarList.removeAll()
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            self.viewNotServiceAvailble.isHidden = true
                            self.viewSetDentination.isHidden = true
                            self.viewChooseCarSelectedPopUp.isHidden = false
                            self.viewPayementMethod.isHidden = true
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                let dicData = SRSelectCarData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrSelectCarList.append(dicData)
                            }
                            
                            self.collectionViewChooseCar.reloadData()
                            
                        }
                        else
                        {
                            self.viewNotServiceAvailble.isHidden = false
                        }
                        
                    } else {
                        self.viewNotServiceAvailble.isHidden = false
                    }
                    
                } else {
                    
                    self.indicator.stopAnimating()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                self.indicator.stopAnimating()
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callInstantBookingAPI(car_id: String) {
        
        let param = ["id": appDelagte.dicLoginUserDetails?.userId ?? "","pick_lat": self.objPickUpLocation_Lat,"pick_lang": self.objPickUpLocation_Long,"drop_lat": self.objDropLocation_Lat ,"drop_lang": self.objDropsLocation_Long ,"booking_type":"1","car_id": car_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(INSTANT_BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            self.viewNotServiceAvailble.isHidden = true
                            self.viewSetDentination.isHidden = true
                            self.viewChooseCarSelectedPopUp.isHidden = true
                            self.viewPayementMethod.isHidden = false
                            
                            let arrData = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let dicData = SRInstantBookingData(fromDictionary: (arrData as? NSDictionary)!)
                            self.dicInstantBookingList = dicData
                            
                            self.callWalletDiscountAPI(fare: dicData.totalPrice ?? 0.0)
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast(data_Message)
                    }
                    
                } else
                {
                    
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callWalletDiscountAPI(fare: Double) {
        
        let param = ["lat": self.objDropLocation_Lat,"lang":  self.objDropsLocation_Long,"fare": "\(fare)"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(WALLET_DISCOUNT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            let dicData = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let discount = dicData?.value(forKey: "discount") as? Double
                            let amount = dicData?.value(forKey: "amount") as? Double
                            self.lblEstimatedAmount.text = "Estimated Amount: CAD$\(amount?.round(to: 2) ?? 0.0)"
                            self.objEstimatedAmount = "\(amount?.round(to: 2) ?? 0.0)"
                            
                            if discount == 0
                            {
                                self.discountHieghtConts.constant = 0
                                self.lblDiscount.isHidden = true
                            }
                            else
                            {
                                self.discountHieghtConts.constant = 16
                                self.lblDiscount.isHidden = false
                            }
                            
                        }
                        else
                        {
                            
                        }
                        
                    }
                    
                } else {
                    
                    self.indicator.stopAnimating()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                self.indicator.stopAnimating()
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callBalanceCheckAPI() {
        
        let param = ["lat": self.objPickUpLocation_Lat,"lang": self.objPickUpLocation_Long,"id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(BALANCE_CHECK, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            self.callInstantBookingSubmitAPI()
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                    }
                    
                } else {
                    
                    self.indicator.stopAnimating()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                self.indicator.stopAnimating()
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callInstantBookingSubmitAPI() {
        
        let car_type = dicSelectedCarData?.carType ?? ""
        let ride_type = dicSelectedCarData?.rideType ?? ""
        let city_table_id = dicSelectedCarData?.cityTableId ?? ""
        let ridetable_table_id = dicSelectedCarData?.ridetypeTableId ?? ""
        let config_table_id = dicSelectedCarData?.configTableId ?? ""
        let usertype_table_id = dicSelectedCarData?.usertypeTableId ?? ""
        
        let start_loc = dicSelectedCarData?.startLoc ?? ""
        let end_loc = dicSelectedCarData?.endLoc ?? ""
        
        let base_fare = dicInstantBookingList?.baseFare ?? 0
        let dis_fem = dicInstantBookingList?.femaleRiderDiscount ?? ""
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "", "start_lat": self.objPickUpLocation_Lat,"start_lang":  self.objPickUpLocation_Long,"end_lat": self.objDropLocation_Lat,"end_lang": self.objDropsLocation_Long,"car_type": car_type,"ride_type": ride_type,"city_table_id": city_table_id,"ridetable_table_id": ridetable_table_id,"config_table_id": config_table_id,"usertype_table_id": usertype_table_id,"payment_method":"wallet","booking_device": "mobile","cartable_table_id":"","start_loc": start_loc,"end_loc": end_loc,"base_fare": "\(self.objEstimatedAmount)","dis_fem": dis_fem]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(INSTANT_BOOKING_SUBMIT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if message == "success"
                        {
                            self.viewSetDentination.isHidden = true
                            self.viewChooseCarSelectedPopUp.isHidden = true
                            self.viewPayementMethod.isHidden = true
                            self.viewBookingSucess.isHidden = false
                            self.viewSearchingForDriver.isHidden = false
                            self.view.makeToast("Booking Confirmed!")
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                    }
                    
                } else {
                    
                    self.indicator.stopAnimating()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                self.indicator.stopAnimating()
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    
    func callVehicleSeatAPI() {
        
        let param = ["":""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(VEHICLE_SEATS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrVehicleSeatList.removeAll()
                        
                        if message == "success" {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                
                                let dicData = SRVehicleSeatData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrVehicleSeatList.append(dicData)
                            }
                            
                            self.setDropDownVehicleSeat()
                            
                        } else {
                            
                            self.viewBookingalert.isHidden = false
                            self.objVehicleSeat = "1"
                            self.arrVehicleSeatList.removeAll()
                            self.setDropDownVehicleSeat()
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callViewDriverListEventAPI(keyword: String) {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","event_id": objCareEventListData?.eventId ?? "","keyword": keyword,"car_type":"\(objVehicleSeat)","lat": "\(appDelagte.objLatitude)","lang":"\(appDelagte.objLongitude)","oneway_type": isOneWayTrip]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(VIEW_DRIVER_LIST_EVENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrDriverDeatilsList.removeAll()

                        if message == "success" {

                            let arrData = responseUser.value(forKey: "data") as? NSArray

                            for obj in arrData! {
                                let dicData = SRDriverDeatilsListData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrDriverDeatilsList.append(dicData)
                            }

                            self.tblViewDriverList.reloadData()

                        } else {

                            self.arrDriverDeatilsList.removeAll()
                            self.tblViewDriverList.reloadData()
                            
                            let windows = UIApplication.shared.windows
                            windows.last?.makeToast(data_Message)
                            self.view.makeToast(data_Message)
                        }

                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callCare_BookingAPI() {
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? "","event_id": objCareEventListData?.eventId ?? "","driver_id": objDriverDeatils?.dId ?? "","location":appDelagte.objCurrentAddress ?? "","lat": "\(appDelagte.objLatitude ?? 0.0)","lang": "\(appDelagte.objLongitude ?? 0.0)","payment_method": "wallet","booking_device":"Mobile","car_type": "\(objVehicleSeat ?? "")","short_name":"","athlete_id":"\(self.objAthleteID ?? "")","oneway_type": isOneWayTrip]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CARE_BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        self.arrVehicleSeatList.removeAll()
                        
                        if message == "success" {
                            
                            self.viewBooking.isHidden = true
                            self.viewBooked.isHidden = false

                        } else {
                            self.view.makeToast(data_Message)
                            
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callBooking_data_API() {
        
        let param = ["booking_id": appDelagte.isFromCareRideViewMenu_BookingID]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(BOOKING_DATA, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {

                        if message == "success"
                        {
                            let dicData = responseUser.value(forKey: "data") as? NSDictionary
                            self.setDriverAllDetailFromCareRideMenu(dicDriver: dicData!)
                        }
                        else
                        {
                            self.view.makeToast(data_Message)
                        }

                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callStartTripAPI(bookingId: String,driverId: String) {
        
        let param = ["booking_id": bookingId,"driver_id": driverId]
        APIClient.sharedInstance.showIndicator()
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(START_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {

                            self.viewBottomStartTrip.isHidden = true
                            self.viewBottomStartTripHieghtCont.constant = 0
                            
                        } else
                        {
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func getAddressFromLatLong(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(lon)&key=\(PROVIDE_API_KEY)"
        
        AF.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                let responseJson = response.value! as! NSDictionary
                
                if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                    if results.count > 0 {
                        if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                            let address = results[0]["formatted_address"] as? String
                            let arrAddress = address?.components(separatedBy: ",") as! NSArray
                            //  print(address)
                            
                            appDelagte.objFullAddress = address ?? ""
                            
                            appDelagte.objCurrentAddress = address ?? ""
                            
                            if address!.count > 0{
                                
                                if arrAddress.count >= 2{
                                    let address1 = arrAddress.object(at: 0) as! String
                                    let address2 = arrAddress.object(at: 1) as! String
                                    
                                    // self.txtAddress1.text = "\(address1), \(address2)"
                                }
                                else{
                                    let address1 = arrAddress.object(at: 0) as! String
                                    // self.txtAddress1.text = "\(address1)"
                                }
                            }
                            
                            var locality = ""
                            var country = ""
                            for component in addressComponents {
                                if let temp = component.object(forKey: "types") as? [String] {
                                    for objTemp in temp{
                                        if locality == ""{
                                            if (objTemp == "locality") {
                                                locality = (component["long_name"] as? String)!
                                                //   print(locality)
                                                
                                            }
                                        }
                                        if country == ""{
                                            if (objTemp == "country") {
                                                country = (component["long_name"] as? String)!
                                                //  print(country)
                                                
                                            }
                                        }
                                        
                                    }
                                    
                                }
                            }
                            
                            if country != ""{
                                // self.txtCountry.text = country
                                
                            }
                            if locality != ""{
                                // self.txtCity.text = locality
                            }
                            
                        }
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

//MARK:- UICollection View
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrSelectCarList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewChooseCar.dequeueReusableCell(withReuseIdentifier: "chooseServerCarCollectionCell", for: indexPath) as! chooseServerCarCollectionCell
        
        let dicData = arrSelectCarList[indexPath.row]
        
        var imgCar = dicData.carImage ?? ""
        imgCar = imgCar.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: imgCar)
        cell.imgCar.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgCar.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        let carName = dicData.carName ?? ""
        cell.lblName.text = carName
        
        let estimatedFare = dicData.estimatedFare ?? 0.0
        cell.lblFare.text = "(CAD$\(estimatedFare))"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isSelectedCar = true
        dicSelectedCarData = arrSelectCarList[indexPath.row]
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblViewEventList
        {
            return arrEventDataList.count
        }
        else
        {
            return arrDriverDeatilsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblViewEventList
        {
            let cell = tblViewEventList.dequeueReusableCell(withIdentifier: "SelectEventListTableCell") as! SelectEventListTableCell
            
            let dicData = arrEventDataList[indexPath.row]
            
            cell.lblDay.text = dicData.day ?? ""
            
            cell.lblTime.text = "\(dicData.startTime ?? "") - \(dicData.endTime ?? "")"
            
            cell.lblDate.text = dicData.date ?? ""
            
            cell.lblMopnth.text = dicData.month ?? ""
            
            cell.lblShortDes.text = dicData.eventName ?? ""
            
            cell.lblAddress.text = dicData.location ?? ""
            
            return cell
        }
        else
        {
            let cell = tblViewDriverList.dequeueReusableCell(withIdentifier: "DriverListTableCell") as! DriverListTableCell
            
            let dicData = arrDriverDeatilsList[indexPath.row]
            
            cell.lblName.text = "\(dicData.name ?? "") (\(dicData.age ?? "") yrs)"
            cell.lblDetails.text = "\(dicData.vehicleColor ?? "") \(dicData.vehicleMake ?? "") \(dicData.vehicleType ?? ""), \(dicData.vehicleSeats ?? "")"
            cell.lblRate.text = dicData.rating ?? ""
            
            var image = dicData.photo ?? ""
            image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "\(image)")
            cell.imgDriver.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgDriver.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            
            
            if indexPath.row == selectedDriver
            {
                cell.imgCheckUncheck.image = UIImage(named: "ic_checkedBox")
            }
            else
            {
                cell.imgCheckUncheck.image = UIImage(named: "ic_uncheckedBox")
            }
            
            cell.btnCheckUncheck.tag = indexPath.row
            cell.btnCheckUncheck.addTarget(self, action: #selector(clickedSelctedDriver(sender:)), for: .touchUpInside)
            
            return cell
        }
        
        
    }
    
    @objc func clickedSelctedDriver(sender: UIButton)
    {
        let dicData = arrDriverDeatilsList[sender.tag]
        objDriverDeatils = dicData
        selectedDriver = sender.tag
        self.tblViewDriverList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblViewEventList
        {
            return 90
        }
        else
        {
            return 75
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblViewEventList
        {
            self.viewSelectTripBookDriver.isHidden = false
            self.viewSelectedTrip.isHidden = false
            self.objCareEventListData = arrEventDataList[indexPath.row]
            
            self.lblMonthPick.text = self.objCareEventListData?.month ?? ""
            self.lblMonthDrop.text = self.objCareEventListData?.month ?? ""
            
            self.lblDatePick.text = "\(self.objCareEventListData?.date ?? ""), \(self.objCareEventListData?.day ?? "") "
            self.lblDateDrop.text = "\(self.objCareEventListData?.date ?? ""), \(self.objCareEventListData?.day ?? "") "
            
            self.lblTimePick.text = self.objCareEventListData?.startTime ?? ""
            self.lblTimeDrop.text = self.objCareEventListData?.endTime ?? ""
            
            self.lblAddressPick.text = self.txtPickupLocation.text ?? ""
            self.lblAddressDrop.text = self.objCareEventListData?.location ?? ""
            selectedDriver = -1
            callViewDriverListEventAPI(keyword: "")
        }
        else
        {
            self.viewSelectedTrip.isHidden = true
            viewSelectedDriverInfo.isHidden = false
            
            let dicData = arrDriverDeatilsList[indexPath.row]
            objDriverDeatils = dicData
            
            var image = dicData.photo ?? ""
            image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "\(image)")
            imgSelectedDriverPhotoSS.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgSelectedDriverPhotoSS.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            
            var image1 = dicData.vehiclePhoto ?? ""
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "\(image1)")
            imgSelectedVehiclePhpotp.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgSelectedVehiclePhpotp.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
            
            lblSelectedDriverNameSS.text = dicData.name ?? ""
            lblSelectedRateSS.text = dicData.rating ?? ""
            
            lblSelectedAgeSS.text = dicData.rating ?? ""
            lblSelectedVehicleTypeSS.text = "\(dicData.vehicleColor ?? "") \(dicData.vehicleMake ?? "") \(dicData.vehicleType ?? ""), \(dicData.vehicleSeats ?? "")"
            lblSelectedVehicleYearSS.text = dicData.vehicleYear ?? ""
            lblSelectedPlateSS.text = dicData.lPlate ?? ""
        }
        
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        
        appDelagte.objLatitude = location.coordinate.latitude
        appDelagte.objLongitude = location.coordinate.longitude
        
        //        print("Current latitude: \(location.coordinate.latitude ?? 0.0)")
        //        print("Current longitude: \(location.coordinate.longitude ?? 0.0)")
        
        self.mapView.isMyLocationEnabled = false
        
        marker.icon = UIImage(named: "ic_map") // Marker icon
        marker.position = location.coordinate // CLLocationCoordinate2D
        
        DispatchQueue.main.async { // Setting marker on mapview in main thread.
            self.marker.map = self.mapView // Setting marker on Mapview
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 16.0)
        
        self.objLatitude = String((location.coordinate.latitude))
        self.objLongitude = String((location.coordinate.longitude))
        
        self.mapView?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        
        getAddressFromLatLong(pdblLatitude: String((location.coordinate.latitude)), withLongitude: String((location.coordinate.longitude)))
        
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            
            guard let placemark = placemarks?.first else {
                let errorString = error?.localizedDescription ?? "Unexpected Error"
                print("Unable to reverse geocode the given location. Error: \(errorString)")
                return
            }
            
            let reversedGeoLocation = ReversedGeoLocation(with: placemark)
            //  print(reversedGeoLocation.formattedAddress)
            
            appDelagte.objContry = reversedGeoLocation.country
            appDelagte.objState = reversedGeoLocation.state
            appDelagte.objCity = reversedGeoLocation.city
            
            //            print(appDelagte.objCity)
            //            print(appDelagte.objState)
            //            print(appDelagte.objContry)
        }
        
        locationManager.stopUpdatingLocation()
    }
}

extension UIViewController {
    func setUpStatus(color: UIColor) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = color
            self.view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: self.view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: self.view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: self.view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
    
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
