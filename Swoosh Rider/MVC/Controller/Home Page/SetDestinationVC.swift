//
//  SetDestinationVC.swift
//  Swoosh Rider
//
//  Created by Gabani King on 06/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import Alamofire

protocol DriverLocationDelegate
{
    func onDriverLocationReady(pickUpLocation_Name: String,pickUpLocation_Lat: String,pickUpLocation_Long: String,dropLocation_Name: String,dropLocation_Lat: String,dropsLocation_Long: String)
}
class SetDestinationVC: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var txtEnterPickupLocation: UITextField!
    @IBOutlet weak var txtWhereToNow: UITextField!
    @IBOutlet weak var hideSearchLocationview: UIView!
    
    @IBOutlet weak var viewMainAddLocation: UIView!
    @IBOutlet weak var txtEnterLocationNaemPopup: UITextField!
    @IBOutlet weak var txtEnterLocation2Popup: UITextField!
    
    @IBOutlet weak var tblViewSerchLocation: UITableView!
    @IBOutlet weak var tblViewPopUpSearch: UITableView!

    @IBOutlet weak var tblViewCoustomLocation: UITableView!
    @IBOutlet weak var tblViewHieghtCont: NSLayoutConstraint!
    
    
    var delegate: DriverLocationDelegate?
    
    var arrCustomLocation: [SRCustomLocationData] = [SRCustomLocationData]()
    
    var resultsArray = [PlaceData]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    var objStrCity: String?
    var objStrCuntry: String?
    var objStrPincode: String?
    var objStrState: String?
    
    
    var objAddNewLocation = ""
    var objAddNewLat = 0.0
    var objAddNewLong = 0.0
    
    
    var objPickUpLocation_Name = ""
    var objPickUpLocation_Lat = ""
    var objPickUpLocation_Long = ""
    
    var objDropLocation_Name = ""
    var objDropLocation_Lat = ""
    var objDropsLocation_Long = ""

    
    var isPickUpAddressStart = true
    var isSetDestination = true
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEnterPickupLocation.delegate = self
        txtWhereToNow.delegate = self
        
        self.txtEnterPickupLocation.text = appDelagte.objFullAddress
        self.objPickUpLocation_Lat = "\(appDelagte.objLatitude)"
        self.objPickUpLocation_Long = "\(appDelagte.objLongitude)"
        self.objPickUpLocation_Name = appDelagte.objFullAddress

        self.tblViewPopUpSearch.isHidden = true

        txtEnterLocation2Popup.delegate = self
        
        tblViewSerchLocation.delegate = self
        tblViewSerchLocation.dataSource = self
        
        tblViewPopUpSearch.delegate = self
        tblViewPopUpSearch.dataSource = self
        
        tblViewCoustomLocation.delegate = self
        tblViewCoustomLocation.dataSource = self
        
        viewMainAddLocation.isHidden = true
        setUpStatus(color: SWOOSH_BACK_BACK)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        hideSearchLocationview.isUserInteractionEnabled = true
        hideSearchLocationview.addGestureRecognizer(tap)
        
        callAddCustomLocationAPI(showIndicator: true)
        
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- IBAction
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
        viewMainAddLocation.isHidden = true
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedRemoveAddLocation2(_ sender: Any) {
        self.view.endEditing(true)
        txtEnterLocation2Popup.text = ""
    }
    
    @IBAction func clickedAddLocation2(_ sender: Any) {
        
        if txtEnterLocationNaemPopup.text == ""
        {
            self.view.makeToast("Enter Location Name")
        }
        else if txtEnterLocation2Popup.text == ""
        {
            self.view.makeToast("Enter Your Location")
        }
        else
        {
            self.view.endEditing(true)
            viewMainAddLocation.isHidden = true
            callAddCustomLocationAPI()
        }
        
    }
    
    @IBAction func clickedRemovedNow(_ sender: Any) {
        self.view.endEditing(true)
        txtWhereToNow.text = ""
    }
    
    @IBAction func clickedPickRemoved(_ sender: Any) {
        self.view.endEditing(true)
        txtEnterPickupLocation.text = ""
    }
    
    @IBAction func clickedAddNewLocation(_ sender: Any) {
        self.view.endEditing(true)
        viewMainAddLocation.isHidden = false
    }
    
    @IBAction func clickedGo(_ sender: Any) {
        
        if txtEnterPickupLocation.text == "" || txtWhereToNow.text == ""
        {
               
        }
        else
        {
               delegate?.onDriverLocationReady(pickUpLocation_Name: self.objPickUpLocation_Name, pickUpLocation_Lat: self.objPickUpLocation_Lat, pickUpLocation_Long: self.objPickUpLocation_Long, dropLocation_Name: self.objDropLocation_Name, dropLocation_Lat: self.objDropLocation_Lat, dropsLocation_Long: self.objDropsLocation_Long)
               self.navigationController?.popViewController(animated: true)
        }
     
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewCoustomLocation
        {
            return arrCustomLocation.count
        }
        else
        {
            if resultsArray != nil{
                return resultsArray.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblViewCoustomLocation
        {
            let cell = tblViewCoustomLocation.dequeueReusableCell(withIdentifier: "SetCutomLocationTableCell") as! SetCutomLocationTableCell
            
            let objData = arrCustomLocation[indexPath.row]
            
            cell.lblLName.text = objData.locationName ?? ""
            
            cell.lblAddress.text = objData.location ?? ""
            
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(clickedRemovedLocation(sender:)), for: .touchUpInside)
            
            return cell
        }
        else if tableView == tblViewPopUpSearch
        {
            let cell = tblViewPopUpSearch.dequeueReusableCell(withIdentifier: "SearchLocationCell") as! SearchLocationCell
            
            let objPlaceData = resultsArray[indexPath.row]
            cell.lblTitleName.text = objPlaceData.firstName
            cell.lblSubTitleName.text = objPlaceData.secondName
            
            return cell
        }
        else
        {
            let cell = tblViewSerchLocation.dequeueReusableCell(withIdentifier: "SetGglLocationTableCell") as! SetGglLocationTableCell
            
            let objPlaceData = resultsArray[indexPath.row]
            cell.lblLName.text = objPlaceData.firstName
            cell.lblAddress.text = objPlaceData.secondName
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblViewCoustomLocation
        {
            let objData = arrCustomLocation[indexPath.row]
            self.objDropLocation_Name = "\(objData.locationName ?? ""), \(objData.location ?? "")"
            self.objDropLocation_Lat = objData.lat ?? ""
            self.objDropsLocation_Long = objData.lang ?? ""
            self.txtWhereToNow.text = objData.location ?? ""

        }
        else if tableView == tblViewSerchLocation
        {
            
            if isPickUpAddressStart == true
            {
                let objPlaceData = resultsArray[indexPath.row]
                self.txtEnterPickupLocation.text = "\(objPlaceData.firstName ?? ""), \(objPlaceData.secondName ?? "")"
                self.objPickUpLocation_Name = objPlaceData.secondName ?? ""
                self.objPickUpLocation_Lat = "\(objPlaceData.lattitude ?? 0.0)"
                self.objPickUpLocation_Long = "\(objPlaceData.longitude ?? 0.0)"

            }
            else
            {
                let objPlaceData = resultsArray[indexPath.row]
                self.txtWhereToNow.text = "\(objPlaceData.firstName ?? ""), \(objPlaceData.secondName ?? "")"
                self.objDropLocation_Name = objPlaceData.secondName ?? ""
                self.objDropLocation_Lat = "\(objPlaceData.lattitude ?? 0.0)"
                self.objDropsLocation_Long = "\(objPlaceData.longitude ?? 0.0)"
            }
        }
        else
        {
            let objPlaceData = resultsArray[indexPath.row]
            self.txtEnterLocation2Popup.text = "\(objPlaceData.firstName ?? ""), \(objPlaceData.secondName ?? "")"
            self.objAddNewLocation = "\(objPlaceData.firstName ?? ""), \(objPlaceData.secondName ?? "")"
            self.objAddNewLat = objPlaceData.lattitude ?? 0.0
            self.objAddNewLong = objPlaceData.longitude ?? 0.0
            
            self.resultsArray.removeAll()
            self.tblViewPopUpSearch.reloadData()
            self.tblViewPopUpSearch.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblViewSerchLocation
        {
            return UITableView.automaticDimension
        }
        else
        {
            return 77
        }
    }
    
    
    @objc func clickedRemovedLocation(sender: UIButton)
    {
        let objData = arrCustomLocation[sender.tag]
        callRemoveCustomLocationAPI(location_id: objData.id ?? "")
    }
    
    //MARK:- API Calling
    func callAddCustomLocationAPI(showIndicator: Bool) {
        
        if showIndicator == true
        {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["user_id": appDelagte.dicLoginUserDetails?.userId ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(VIEW_LOCATION, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.arrCustomLocation.removeAll()
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            
                            for obj in arrData! {
                                let dicData = SRCustomLocationData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrCustomLocation.append(dicData)
                            }
                            
                            self.tblViewHieghtCont.constant = CGFloat(self.arrCustomLocation.count * 75) + 2.5
                            
                            self.tblViewCoustomLocation.reloadData()
                            
                        } else {
                            self.tblViewHieghtCont.constant = 5
                            self.arrCustomLocation.removeAll()
                            self.tblViewCoustomLocation.reloadData()
                            // self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        //  self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callRemoveCustomLocationAPI(location_id: String) {
        
        //  APIClient.sharedInstance.showIndicator()
        
        let param = ["location_id": location_id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(REMOVE_LOCATION, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.callAddCustomLocationAPI(showIndicator: false)
                            self.view.makeToast(data_Message)
                            
                        } else {
                            
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callAddCustomLocationAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["user_id": "\(appDelagte.dicLoginUserDetails?.userId ?? "")","location": objAddNewLocation,"lat": "\(objAddNewLat)","lang": "\(objAddNewLong)","location_name": txtEnterLocationNaemPopup.text ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ADD_LOCATION, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        if message == "success" {
                            
                            self.callAddCustomLocationAPI(showIndicator: false)
                            self.view.makeToast(data_Message)
                            
                        } else {
                            
                            self.view.makeToast(data_Message)
                        }
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}


extension SetDestinationVC : GMSAutocompleteFetcherDelegate{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        
        if isSetDestination == true
        {
            
            self.resultsArray.removeAll()
            self.tblViewSerchLocation.isHidden = true
            for prediction in predictions {
                var placeDataObj = PlaceData()
                if let prediction = prediction as GMSAutocompletePrediction?{
                    
                    let placeId = prediction.placeID
                    
                    let placeClient = GMSPlacesClient()
                    
                    placeClient.lookUpPlaceID(prediction.placeID) { (place, error) -> Void in
                        if let error = error {
                            //show error
                            return
                        }
                        
                        if let place = place {
                            place.coordinate.longitude //longitude
                            place.coordinate.latitude
                            placeDataObj.firstName = prediction.attributedPrimaryText.string
                            placeDataObj.secondName = prediction.attributedSecondaryText?.string
                            placeDataObj.lattitude = place.coordinate.latitude
                            placeDataObj.longitude = place.coordinate.longitude
                            placeDataObj.country_name = place.formattedAddress
                            placeDataObj.strCity = place.name
                            placeDataObj.strState = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
                            placeDataObj.strCuntry = place.addressComponents?.first(where: { $0.type == "country" })?.name
                            placeDataObj.strPincode = place.addressComponents?.first(where: { $0.type == "postal_code" })?.name
                            
                        } else {
                            //show error
                        }
                        self.resultsArray.append(placeDataObj)
                        self.tblViewSerchLocation.isHidden = false
                        DispatchQueue.main.async {
                            if self.resultsArray.count == 0 {
                                //self.viewNoLocation.isHidden = false
                                self.tblViewSerchLocation.isHidden = true
                            }
                            else
                            {
                                print(333333)
                                
                                if self.isPickUpAddressStart == true
                                {
                                    
                                    if self.txtEnterPickupLocation.text == "" {
                                        self.tblViewSerchLocation.isHidden = true
                                    }
                                    else
                                    {
                                        self.tblViewSerchLocation.isHidden = false
                                    }
                                }
                                else
                                {
                                    
                                    if self.txtWhereToNow.text == "" {
                                        self.tblViewSerchLocation.isHidden = true
                                    }
                                    else
                                    {
                                        self.tblViewSerchLocation.isHidden = false
                                    }
                                }
                               
                                
                            }
                            self.tblViewSerchLocation.reloadData()
                        }
                    }
                }
                
            }
        }
        else
        {
            
            self.resultsArray.removeAll()
            self.tblViewPopUpSearch.isHidden = true
            for prediction in predictions {
                var placeDataObj = PlaceData()
                if let prediction = prediction as GMSAutocompletePrediction?{
                    
                    let placeId = prediction.placeID
                    
                    let placeClient = GMSPlacesClient()
                    
                    placeClient.lookUpPlaceID(prediction.placeID) { (place, error) -> Void in
                        if let error = error {
                            //show error
                            return
                        }
                        
                        if let place = place {
                            place.coordinate.longitude //longitude
                            place.coordinate.latitude
                            placeDataObj.firstName = prediction.attributedPrimaryText.string
                            placeDataObj.secondName = prediction.attributedSecondaryText?.string
                            placeDataObj.lattitude = place.coordinate.latitude
                            placeDataObj.longitude = place.coordinate.longitude
                            placeDataObj.country_name = place.formattedAddress
                            placeDataObj.strCity = place.name
                            placeDataObj.strState = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
                            placeDataObj.strCuntry = place.addressComponents?.first(where: { $0.type == "country" })?.name
                            placeDataObj.strPincode = place.addressComponents?.first(where: { $0.type == "postal_code" })?.name
                            
                        } else {
                            //show error
                        }
                        self.resultsArray.append(placeDataObj)
                        self.tblViewPopUpSearch.isHidden = false
                        DispatchQueue.main.async {
                            if self.resultsArray.count == 0 {
                                //self.viewNoLocation.isHidden = false
                                self.tblViewPopUpSearch.isHidden = true
                            }
                            else
                            {
                                print(333333)
                                
                                if self.txtEnterLocation2Popup.text == "" {
                                    self.tblViewPopUpSearch.isHidden = true
                                }
                                else
                                {
                                    self.tblViewPopUpSearch.isHidden = false
                                }
                                
                            }
                            self.tblViewPopUpSearch.reloadData()
                        }
                    }
                }
                
            }
        }
        
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error.localizedDescription)
    }
    
    
}



extension SetDestinationVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtEnterPickupLocation
        {
            isPickUpAddressStart = true
            isSetDestination = true
            
        }
        else if textField == txtWhereToNow
        {
            isSetDestination = true
            isPickUpAddressStart = false
        }
        else if textField == txtEnterLocation2Popup
        {
            isSetDestination = false
        }
    }
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEnterLocation2Popup
        {
            if let text = textField.text,
               let textRange = Range(range, in: text) {
                //            let updatedText = text.replacingCharacters(in: textRange,
                //                                                       with: string)
                //            print("Text: \(updatedText)")
                //            print("Range: \(range)&\(text)")
                let mergeString = "\(textField.text!)\(string)"
                var finalString = ""
                if string != ""{
                    finalString = mergeString
                }else{
                    finalString = String(mergeString.dropLast())
                }
                print("FinalString\(finalString)")
                if finalString == ""{
                    resultsArray = [PlaceData]()
                    //                gmsFetcher.sourceTextHasChanged("")
                    DispatchQueue.main.async {
                        if self.resultsArray.count == 0 {
                            //  self.viewNoLocation.isHidden = false
                            
                            print(111111)
                            DispatchQueue.main.async {
                                self.tblViewPopUpSearch.isHidden = true
                            }
                        }
                        else
                        {
                            //  self.viewNoLocation.isHidden = true
                            print(2222222)
                            
                            self.tblViewPopUpSearch.isHidden = false
                        }
                        self.tblViewPopUpSearch.reloadData()
                    }
                    
                }else{
                    gmsFetcher.sourceTextHasChanged(finalString)
                }
                
            }
            else
            {
                
                
            }
            
            return true

        }
        else
        {
            if let text = textField.text,
               let textRange = Range(range, in: text) {
                //            let updatedText = text.replacingCharacters(in: textRange,
                //                                                       with: string)
                //            print("Text: \(updatedText)")
                //            print("Range: \(range)&\(text)")
                let mergeString = "\(textField.text!)\(string)"
                var finalString = ""
                if string != ""{
                    finalString = mergeString
                }else{
                    finalString = String(mergeString.dropLast())
                }
                print("FinalString\(finalString)")
                if finalString == ""{
                    resultsArray = [PlaceData]()
                    //                gmsFetcher.sourceTextHasChanged("")
                    DispatchQueue.main.async {
                        if self.resultsArray.count == 0 {
                            //  self.viewNoLocation.isHidden = false
                            
                            print(111111)
                            DispatchQueue.main.async {
                                self.tblViewSerchLocation.isHidden = true
                            }
                        }
                        else
                        {
                            //  self.viewNoLocation.isHidden = true
                            print(2222222)
                            
                            self.tblViewSerchLocation.isHidden = false
                        }
                        self.tblViewSerchLocation.reloadData()
                    }
                    
                }else{
                    gmsFetcher.sourceTextHasChanged(finalString)
                }
                
            }
            else
            {
                
                
            }
            
            return true

        }
    }
}
