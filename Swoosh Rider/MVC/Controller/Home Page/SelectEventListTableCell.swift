//
//  SelectEventListTableCell.swift
//  Swoosh Rider
//
//  Created by Gabani King on 26/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class SelectEventListTableCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblShortDes: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMopnth: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
