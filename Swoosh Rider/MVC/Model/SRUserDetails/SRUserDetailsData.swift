//
//	SRUserDetailsData.swift
//
//	Create by mac on 19/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRUserDetailsData : NSObject, NSCoding{

	var address : String!
	var cityId : String!
	var countryId : String!
	var email : String!
	var fname : String!
	var lname : String!
	var password : String!
	var phone : String!
	var slNo : String!
	var stateId : String!
	var status : String!
	var uId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		cityId = dictionary["city_id"] as? String == nil ? "" : dictionary["city_id"] as? String
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		fname = dictionary["fname"] as? String == nil ? "" : dictionary["fname"] as? String
		lname = dictionary["lname"] as? String == nil ? "" : dictionary["lname"] as? String
		password = dictionary["password"] as? String == nil ? "" : dictionary["password"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		slNo = dictionary["sl_no"] as? String == nil ? "" : dictionary["sl_no"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		uId = dictionary["u_id"] as? String == nil ? "" : dictionary["u_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if email != nil{
			dictionary["email"] = email
		}
		if fname != nil{
			dictionary["fname"] = fname
		}
		if lname != nil{
			dictionary["lname"] = lname
		}
		if password != nil{
			dictionary["password"] = password
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if slNo != nil{
			dictionary["sl_no"] = slNo
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if uId != nil{
			dictionary["u_id"] = uId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fname = aDecoder.decodeObject(forKey: "fname") as? String
         lname = aDecoder.decodeObject(forKey: "lname") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         slNo = aDecoder.decodeObject(forKey: "sl_no") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         uId = aDecoder.decodeObject(forKey: "u_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fname != nil{
			aCoder.encode(fname, forKey: "fname")
		}
		if lname != nil{
			aCoder.encode(lname, forKey: "lname")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if slNo != nil{
			aCoder.encode(slNo, forKey: "sl_no")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if uId != nil{
			aCoder.encode(uId, forKey: "u_id")
		}

	}

}
