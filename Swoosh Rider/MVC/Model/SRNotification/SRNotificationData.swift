//
//	SRNotificationData.swift
//
//	Create by mac on 19/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRNotificationData : NSObject, NSCoding{

	var athleteId : String!
	var dateTime : String!
	var descriptionField : String!
	var notifyId : String!
	var status : String!
	var teamId : String!
	var title : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		athleteId = dictionary["athlete_id"] as? String == nil ? "" : dictionary["athlete_id"] as? String
		dateTime = dictionary["date_time"] as? String == nil ? "" : dictionary["date_time"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		notifyId = dictionary["notify_id"] as? String == nil ? "" : dictionary["notify_id"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		teamId = dictionary["team_id"] as? String == nil ? "" : dictionary["team_id"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if athleteId != nil{
			dictionary["athlete_id"] = athleteId
		}
		if dateTime != nil{
			dictionary["date_time"] = dateTime
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if notifyId != nil{
			dictionary["notify_id"] = notifyId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if teamId != nil{
			dictionary["team_id"] = teamId
		}
		if title != nil{
			dictionary["title"] = title
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         athleteId = aDecoder.decodeObject(forKey: "athlete_id") as? String
         dateTime = aDecoder.decodeObject(forKey: "date_time") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         notifyId = aDecoder.decodeObject(forKey: "notify_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         teamId = aDecoder.decodeObject(forKey: "team_id") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if athleteId != nil{
			aCoder.encode(athleteId, forKey: "athlete_id")
		}
		if dateTime != nil{
			aCoder.encode(dateTime, forKey: "date_time")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if notifyId != nil{
			aCoder.encode(notifyId, forKey: "notify_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if teamId != nil{
			aCoder.encode(teamId, forKey: "team_id")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}
