//
//	SRClubListUserData.swift
//
//	Create by mac on 20/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRClubListUserData : NSObject, NSCoding{

	var clubName : String!
	var content : String!
	var country : String!
	var id : String!
	var image : String!
	var sportsType : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		clubName = dictionary["club_name"] as? String == nil ? "" : dictionary["club_name"] as? String
		content = dictionary["content"] as? String == nil ? "" : dictionary["content"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		sportsType = dictionary["sports_type"] as? String == nil ? "" : dictionary["sports_type"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if clubName != nil{
			dictionary["club_name"] = clubName
		}
		if content != nil{
			dictionary["content"] = content
		}
		if country != nil{
			dictionary["country"] = country
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if sportsType != nil{
			dictionary["sports_type"] = sportsType
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         clubName = aDecoder.decodeObject(forKey: "club_name") as? String
         content = aDecoder.decodeObject(forKey: "content") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         sportsType = aDecoder.decodeObject(forKey: "sports_type") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if clubName != nil{
			aCoder.encode(clubName, forKey: "club_name")
		}
		if content != nil{
			aCoder.encode(content, forKey: "content")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if sportsType != nil{
			aCoder.encode(sportsType, forKey: "sports_type")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}