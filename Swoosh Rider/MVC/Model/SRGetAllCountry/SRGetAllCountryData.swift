//
//	SRGetAllCountryData.swift
//
//	Create by mac on 15/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRGetAllCountryData : NSObject, NSCoding{

	var id : String!
	var image : String!
	var name : String!
	var phonecode : String!
	var sortname : String!
	var status : String!
    var country_id: String!
    var state_id: String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phonecode = dictionary["phonecode"] as? String == nil ? "" : dictionary["phonecode"] as? String
		sortname = dictionary["sortname"] as? String == nil ? "" : dictionary["sortname"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
        country_id = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
        state_id = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phonecode != nil{
			dictionary["phonecode"] = phonecode
		}
		if sortname != nil{
			dictionary["sortname"] = sortname
		}
		if status != nil{
			dictionary["status"] = status
		}
        if country_id != nil{
            dictionary["country_id"] = country_id
        }
        if state_id != nil{
            dictionary["state_id"] = state_id
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phonecode = aDecoder.decodeObject(forKey: "phonecode") as? String
         sortname = aDecoder.decodeObject(forKey: "sortname") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
        country_id = aDecoder.decodeObject(forKey: "country_id") as? String
        state_id = aDecoder.decodeObject(forKey: "state_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phonecode != nil{
			aCoder.encode(phonecode, forKey: "phonecode")
		}
		if sortname != nil{
			aCoder.encode(sortname, forKey: "sortname")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if country_id != nil{
            aCoder.encode(country_id, forKey: "country_id")
        }
        if state_id != nil{
            aCoder.encode(state_id, forKey: "state_id")
        }
	}

}
