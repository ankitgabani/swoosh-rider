//
//	SREventCalenderData.swift
//
//	Create by mac on 24/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SREventCalenderData : NSObject, NSCoding{

	var arrivalTime : String!
	var athleteId : String!
	var athleteName : String!
	var endTime : String!
	var eventDate : String!
	var eventDate2 : String!
	var eventDay : String!
	var eventId : String!
	var eventMonth : String!
	var eventName : String!
	var eventStatus : String!
	var goingStatus : String!
	var location : String!
	var locationName : String!
	var month : String!
	var notes : String!
	var startTime : String!
	var status : String!
	var year : String!
	var year2 : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		arrivalTime = dictionary["arrival_time"] as? String == nil ? "" : dictionary["arrival_time"] as? String
		athleteId = dictionary["athlete_id"] as? String == nil ? "" : dictionary["athlete_id"] as? String
		athleteName = dictionary["athlete_name"] as? String == nil ? "" : dictionary["athlete_name"] as? String
		endTime = dictionary["end_time"] as? String == nil ? "" : dictionary["end_time"] as? String
		eventDate = dictionary["event_date"] as? String == nil ? "" : dictionary["event_date"] as? String
		eventDate2 = dictionary["event_date2"] as? String == nil ? "" : dictionary["event_date2"] as? String
		eventDay = dictionary["event_day"] as? String == nil ? "" : dictionary["event_day"] as? String
		eventId = dictionary["event_id"] as? String == nil ? "" : dictionary["event_id"] as? String
		eventMonth = dictionary["event_month"] as? String == nil ? "" : dictionary["event_month"] as? String
		eventName = dictionary["event_name"] as? String == nil ? "" : dictionary["event_name"] as? String
		eventStatus = dictionary["event_status"] as? String == nil ? "" : dictionary["event_status"] as? String
		goingStatus = dictionary["going_status"] as? String == nil ? "" : dictionary["going_status"] as? String
		location = dictionary["location"] as? String == nil ? "" : dictionary["location"] as? String
		locationName = dictionary["location_name"] as? String == nil ? "" : dictionary["location_name"] as? String
		month = dictionary["month"] as? String == nil ? "" : dictionary["month"] as? String
		notes = dictionary["notes"] as? String == nil ? "" : dictionary["notes"] as? String
		startTime = dictionary["start_time"] as? String == nil ? "" : dictionary["start_time"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		year = dictionary["year"] as? String == nil ? "" : dictionary["year"] as? String
		year2 = dictionary["year2"] as? String == nil ? "" : dictionary["year2"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if arrivalTime != nil{
			dictionary["arrival_time"] = arrivalTime
		}
		if athleteId != nil{
			dictionary["athlete_id"] = athleteId
		}
		if athleteName != nil{
			dictionary["athlete_name"] = athleteName
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if eventDate != nil{
			dictionary["event_date"] = eventDate
		}
		if eventDate2 != nil{
			dictionary["event_date2"] = eventDate2
		}
		if eventDay != nil{
			dictionary["event_day"] = eventDay
		}
		if eventId != nil{
			dictionary["event_id"] = eventId
		}
		if eventMonth != nil{
			dictionary["event_month"] = eventMonth
		}
		if eventName != nil{
			dictionary["event_name"] = eventName
		}
		if eventStatus != nil{
			dictionary["event_status"] = eventStatus
		}
		if goingStatus != nil{
			dictionary["going_status"] = goingStatus
		}
		if location != nil{
			dictionary["location"] = location
		}
		if locationName != nil{
			dictionary["location_name"] = locationName
		}
		if month != nil{
			dictionary["month"] = month
		}
		if notes != nil{
			dictionary["notes"] = notes
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if status != nil{
			dictionary["status"] = status
		}
		if year != nil{
			dictionary["year"] = year
		}
		if year2 != nil{
			dictionary["year2"] = year2
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalTime = aDecoder.decodeObject(forKey: "arrival_time") as? String
         athleteId = aDecoder.decodeObject(forKey: "athlete_id") as? String
         athleteName = aDecoder.decodeObject(forKey: "athlete_name") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? String
         eventDate = aDecoder.decodeObject(forKey: "event_date") as? String
         eventDate2 = aDecoder.decodeObject(forKey: "event_date2") as? String
         eventDay = aDecoder.decodeObject(forKey: "event_day") as? String
         eventId = aDecoder.decodeObject(forKey: "event_id") as? String
         eventMonth = aDecoder.decodeObject(forKey: "event_month") as? String
         eventName = aDecoder.decodeObject(forKey: "event_name") as? String
         eventStatus = aDecoder.decodeObject(forKey: "event_status") as? String
         goingStatus = aDecoder.decodeObject(forKey: "going_status") as? String
         location = aDecoder.decodeObject(forKey: "location") as? String
         locationName = aDecoder.decodeObject(forKey: "location_name") as? String
         month = aDecoder.decodeObject(forKey: "month") as? String
         notes = aDecoder.decodeObject(forKey: "notes") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         year = aDecoder.decodeObject(forKey: "year") as? String
         year2 = aDecoder.decodeObject(forKey: "year2") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "arrival_time")
		}
		if athleteId != nil{
			aCoder.encode(athleteId, forKey: "athlete_id")
		}
		if athleteName != nil{
			aCoder.encode(athleteName, forKey: "athlete_name")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if eventDate != nil{
			aCoder.encode(eventDate, forKey: "event_date")
		}
		if eventDate2 != nil{
			aCoder.encode(eventDate2, forKey: "event_date2")
		}
		if eventDay != nil{
			aCoder.encode(eventDay, forKey: "event_day")
		}
		if eventId != nil{
			aCoder.encode(eventId, forKey: "event_id")
		}
		if eventMonth != nil{
			aCoder.encode(eventMonth, forKey: "event_month")
		}
		if eventName != nil{
			aCoder.encode(eventName, forKey: "event_name")
		}
		if eventStatus != nil{
			aCoder.encode(eventStatus, forKey: "event_status")
		}
		if goingStatus != nil{
			aCoder.encode(goingStatus, forKey: "going_status")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if locationName != nil{
			aCoder.encode(locationName, forKey: "location_name")
		}
		if month != nil{
			aCoder.encode(month, forKey: "month")
		}
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if year != nil{
			aCoder.encode(year, forKey: "year")
		}
		if year2 != nil{
			aCoder.encode(year2, forKey: "year2")
		}

	}

}