//
//	SRInstantBookingData.swift
//
//	Create by Mac M1 on 23/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInstantBookingData : NSObject, NSCoding{

	var baseFare : Int!
	var costPerKm : Int!
	var costPerMin : Int!
	var femaleRiderDiscount : String!
	var km : String!
	var kmCost : Double!
	var min : Double!
	var minCost : Double!
	var totalPrice : Double!
	var userCost : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		baseFare = dictionary["base_fare"] as? Int == nil ? 0 : dictionary["base_fare"] as? Int
		costPerKm = dictionary["cost_per_km"] as? Int == nil ? 0 : dictionary["cost_per_km"] as? Int
		costPerMin = dictionary["cost_per_min"] as? Int == nil ? 0 : dictionary["cost_per_min"] as? Int
		femaleRiderDiscount = dictionary["female_rider_discount"] as? String == nil ? "" : dictionary["female_rider_discount"] as? String
		km = dictionary["km"] as? String == nil ? "" : dictionary["km"] as? String
        kmCost = dictionary["km_cost"] as? Double == nil ? 0.0 : dictionary["km_cost"] as? Double
        min = dictionary["min"] as? Double == nil ? 0.0 : dictionary["min"] as? Double
        minCost = dictionary["min_cost"] as? Double == nil ? 0.0 : dictionary["min_cost"] as? Double
        totalPrice = dictionary["total_price"] as? Double == nil ? 0.0 : dictionary["total_price"] as? Double
		userCost = dictionary["user_cost"] as? String == nil ? "" : dictionary["user_cost"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if baseFare != nil{
			dictionary["base_fare"] = baseFare
		}
		if costPerKm != nil{
			dictionary["cost_per_km"] = costPerKm
		}
		if costPerMin != nil{
			dictionary["cost_per_min"] = costPerMin
		}
		if femaleRiderDiscount != nil{
			dictionary["female_rider_discount"] = femaleRiderDiscount
		}
		if km != nil{
			dictionary["km"] = km
		}
		if kmCost != nil{
			dictionary["km_cost"] = kmCost
		}
		if min != nil{
			dictionary["min"] = min
		}
		if minCost != nil{
			dictionary["min_cost"] = minCost
		}
		if totalPrice != nil{
			dictionary["total_price"] = totalPrice
		}
		if userCost != nil{
			dictionary["user_cost"] = userCost
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         baseFare = aDecoder.decodeObject(forKey: "base_fare") as? Int
         costPerKm = aDecoder.decodeObject(forKey: "cost_per_km") as? Int
         costPerMin = aDecoder.decodeObject(forKey: "cost_per_min") as? Int
         femaleRiderDiscount = aDecoder.decodeObject(forKey: "female_rider_discount") as? String
         km = aDecoder.decodeObject(forKey: "km") as? String
         kmCost = aDecoder.decodeObject(forKey: "km_cost") as? Double
         min = aDecoder.decodeObject(forKey: "min") as? Double
         minCost = aDecoder.decodeObject(forKey: "min_cost") as? Double
         totalPrice = aDecoder.decodeObject(forKey: "total_price") as? Double
         userCost = aDecoder.decodeObject(forKey: "user_cost") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if baseFare != nil{
			aCoder.encode(baseFare, forKey: "base_fare")
		}
		if costPerKm != nil{
			aCoder.encode(costPerKm, forKey: "cost_per_km")
		}
		if costPerMin != nil{
			aCoder.encode(costPerMin, forKey: "cost_per_min")
		}
		if femaleRiderDiscount != nil{
			aCoder.encode(femaleRiderDiscount, forKey: "female_rider_discount")
		}
		if km != nil{
			aCoder.encode(km, forKey: "km")
		}
		if kmCost != nil{
			aCoder.encode(kmCost, forKey: "km_cost")
		}
		if min != nil{
			aCoder.encode(min, forKey: "min")
		}
		if minCost != nil{
			aCoder.encode(minCost, forKey: "min_cost")
		}
		if totalPrice != nil{
			aCoder.encode(totalPrice, forKey: "total_price")
		}
		if userCost != nil{
			aCoder.encode(userCost, forKey: "user_cost")
		}

	}

}
