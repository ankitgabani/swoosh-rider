//
//	SRSelectCarData.swift
//
//	Create by Mac M1 on 23/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRSelectCarData : NSObject, NSCoding{

	var carImage : String!
	var carName : String!
	var carType : String!
	var cartypeTableId : String!
	var cityTableId : String!
	var configTableId : String!
	var endLang : String!
	var endLat : String!
	var endLoc : String!
	var estimatedFare : Double!
	var rideType : String!
	var ridetypeTableId : String!
	var startLang : String!
	var startLat : String!
	var startLoc : String!
	var usertypeTableId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		carImage = dictionary["car_image"] as? String == nil ? "" : dictionary["car_image"] as? String
		carName = dictionary["car_name"] as? String == nil ? "" : dictionary["car_name"] as? String
		carType = dictionary["car_type"] as? String == nil ? "" : dictionary["car_type"] as? String
		cartypeTableId = dictionary["cartype_table_id"] as? String == nil ? "" : dictionary["cartype_table_id"] as? String
		cityTableId = dictionary["city_table_id"] as? String == nil ? "" : dictionary["city_table_id"] as? String
		configTableId = dictionary["config_table_id"] as? String == nil ? "" : dictionary["config_table_id"] as? String
		endLang = dictionary["end_lang"] as? String == nil ? "" : dictionary["end_lang"] as? String
		endLat = dictionary["end_lat"] as? String == nil ? "" : dictionary["end_lat"] as? String
		endLoc = dictionary["end_loc"] as? String == nil ? "" : dictionary["end_loc"] as? String
        estimatedFare = dictionary["estimated_fare"] as? Double == nil ? 0.0 : dictionary["estimated_fare"] as? Double
		rideType = dictionary["ride_type"] as? String == nil ? "" : dictionary["ride_type"] as? String
		ridetypeTableId = dictionary["ridetype_table_id"] as? String == nil ? "" : dictionary["ridetype_table_id"] as? String
		startLang = dictionary["start_lang"] as? String == nil ? "" : dictionary["start_lang"] as? String
		startLat = dictionary["start_lat"] as? String == nil ? "" : dictionary["start_lat"] as? String
		startLoc = dictionary["start_loc"] as? String == nil ? "" : dictionary["start_loc"] as? String
		usertypeTableId = dictionary["usertype_table_id"] as? String == nil ? "" : dictionary["usertype_table_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if carImage != nil{
			dictionary["car_image"] = carImage
		}
		if carName != nil{
			dictionary["car_name"] = carName
		}
		if carType != nil{
			dictionary["car_type"] = carType
		}
		if cartypeTableId != nil{
			dictionary["cartype_table_id"] = cartypeTableId
		}
		if cityTableId != nil{
			dictionary["city_table_id"] = cityTableId
		}
		if configTableId != nil{
			dictionary["config_table_id"] = configTableId
		}
		if endLang != nil{
			dictionary["end_lang"] = endLang
		}
		if endLat != nil{
			dictionary["end_lat"] = endLat
		}
		if endLoc != nil{
			dictionary["end_loc"] = endLoc
		}
		if estimatedFare != nil{
			dictionary["estimated_fare"] = estimatedFare
		}
		if rideType != nil{
			dictionary["ride_type"] = rideType
		}
		if ridetypeTableId != nil{
			dictionary["ridetype_table_id"] = ridetypeTableId
		}
		if startLang != nil{
			dictionary["start_lang"] = startLang
		}
		if startLat != nil{
			dictionary["start_lat"] = startLat
		}
		if startLoc != nil{
			dictionary["start_loc"] = startLoc
		}
		if usertypeTableId != nil{
			dictionary["usertype_table_id"] = usertypeTableId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         carImage = aDecoder.decodeObject(forKey: "car_image") as? String
         carName = aDecoder.decodeObject(forKey: "car_name") as? String
         carType = aDecoder.decodeObject(forKey: "car_type") as? String
         cartypeTableId = aDecoder.decodeObject(forKey: "cartype_table_id") as? String
         cityTableId = aDecoder.decodeObject(forKey: "city_table_id") as? String
         configTableId = aDecoder.decodeObject(forKey: "config_table_id") as? String
         endLang = aDecoder.decodeObject(forKey: "end_lang") as? String
         endLat = aDecoder.decodeObject(forKey: "end_lat") as? String
         endLoc = aDecoder.decodeObject(forKey: "end_loc") as? String
         estimatedFare = aDecoder.decodeObject(forKey: "estimated_fare") as? Double
         rideType = aDecoder.decodeObject(forKey: "ride_type") as? String
         ridetypeTableId = aDecoder.decodeObject(forKey: "ridetype_table_id") as? String
         startLang = aDecoder.decodeObject(forKey: "start_lang") as? String
         startLat = aDecoder.decodeObject(forKey: "start_lat") as? String
         startLoc = aDecoder.decodeObject(forKey: "start_loc") as? String
         usertypeTableId = aDecoder.decodeObject(forKey: "usertype_table_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if carImage != nil{
			aCoder.encode(carImage, forKey: "car_image")
		}
		if carName != nil{
			aCoder.encode(carName, forKey: "car_name")
		}
		if carType != nil{
			aCoder.encode(carType, forKey: "car_type")
		}
		if cartypeTableId != nil{
			aCoder.encode(cartypeTableId, forKey: "cartype_table_id")
		}
		if cityTableId != nil{
			aCoder.encode(cityTableId, forKey: "city_table_id")
		}
		if configTableId != nil{
			aCoder.encode(configTableId, forKey: "config_table_id")
		}
		if endLang != nil{
			aCoder.encode(endLang, forKey: "end_lang")
		}
		if endLat != nil{
			aCoder.encode(endLat, forKey: "end_lat")
		}
		if endLoc != nil{
			aCoder.encode(endLoc, forKey: "end_loc")
		}
		if estimatedFare != nil{
			aCoder.encode(estimatedFare, forKey: "estimated_fare")
		}
		if rideType != nil{
			aCoder.encode(rideType, forKey: "ride_type")
		}
		if ridetypeTableId != nil{
			aCoder.encode(ridetypeTableId, forKey: "ridetype_table_id")
		}
		if startLang != nil{
			aCoder.encode(startLang, forKey: "start_lang")
		}
		if startLat != nil{
			aCoder.encode(startLat, forKey: "start_lat")
		}
		if startLoc != nil{
			aCoder.encode(startLoc, forKey: "start_loc")
		}
		if usertypeTableId != nil{
			aCoder.encode(usertypeTableId, forKey: "usertype_table_id")
		}

	}

}
