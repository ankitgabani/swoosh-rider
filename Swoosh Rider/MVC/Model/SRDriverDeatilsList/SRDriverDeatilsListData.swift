//
//	Data.swift
//
//	Create by mac on 27/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRDriverDeatilsListData : NSObject, NSCoding{

	var age : String!
	var dId : String!
	var lPlate : String!
	var name : String!
	var phone : String!
	var photo : String!
	var prebookStatus : String!
	var rating : String!
	var status : String!
	var vehicleColor : String!
	var vehicleMake : String!
	var vehiclePhoto : String!
	var vehicleSeats : String!
	var vehicleType : String!
	var vehicleYear : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		age = dictionary["age"] as? String == nil ? "" : dictionary["age"] as? String
		dId = dictionary["d_id"] as? String == nil ? "" : dictionary["d_id"] as? String
		lPlate = dictionary["l_plate"] as? String == nil ? "" : dictionary["l_plate"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		photo = dictionary["photo"] as? String == nil ? "" : dictionary["photo"] as? String
		prebookStatus = dictionary["prebook_status"] as? String == nil ? "" : dictionary["prebook_status"] as? String
		rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		vehicleColor = dictionary["vehicle_color"] as? String == nil ? "" : dictionary["vehicle_color"] as? String
		vehicleMake = dictionary["vehicle_make"] as? String == nil ? "" : dictionary["vehicle_make"] as? String
		vehiclePhoto = dictionary["vehicle_photo"] as? String == nil ? "" : dictionary["vehicle_photo"] as? String
		vehicleSeats = dictionary["vehicle_seats"] as? String == nil ? "" : dictionary["vehicle_seats"] as? String
		vehicleType = dictionary["vehicle_type"] as? String == nil ? "" : dictionary["vehicle_type"] as? String
		vehicleYear = dictionary["vehicle_year"] as? String == nil ? "" : dictionary["vehicle_year"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if age != nil{
			dictionary["age"] = age
		}
		if dId != nil{
			dictionary["d_id"] = dId
		}
		if lPlate != nil{
			dictionary["l_plate"] = lPlate
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if photo != nil{
			dictionary["photo"] = photo
		}
		if prebookStatus != nil{
			dictionary["prebook_status"] = prebookStatus
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if status != nil{
			dictionary["status"] = status
		}
		if vehicleColor != nil{
			dictionary["vehicle_color"] = vehicleColor
		}
		if vehicleMake != nil{
			dictionary["vehicle_make"] = vehicleMake
		}
		if vehiclePhoto != nil{
			dictionary["vehicle_photo"] = vehiclePhoto
		}
		if vehicleSeats != nil{
			dictionary["vehicle_seats"] = vehicleSeats
		}
		if vehicleType != nil{
			dictionary["vehicle_type"] = vehicleType
		}
		if vehicleYear != nil{
			dictionary["vehicle_year"] = vehicleYear
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         age = aDecoder.decodeObject(forKey: "age") as? String
         dId = aDecoder.decodeObject(forKey: "d_id") as? String
         lPlate = aDecoder.decodeObject(forKey: "l_plate") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         photo = aDecoder.decodeObject(forKey: "photo") as? String
         prebookStatus = aDecoder.decodeObject(forKey: "prebook_status") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         vehicleColor = aDecoder.decodeObject(forKey: "vehicle_color") as? String
         vehicleMake = aDecoder.decodeObject(forKey: "vehicle_make") as? String
         vehiclePhoto = aDecoder.decodeObject(forKey: "vehicle_photo") as? String
         vehicleSeats = aDecoder.decodeObject(forKey: "vehicle_seats") as? String
         vehicleType = aDecoder.decodeObject(forKey: "vehicle_type") as? String
         vehicleYear = aDecoder.decodeObject(forKey: "vehicle_year") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if dId != nil{
			aCoder.encode(dId, forKey: "d_id")
		}
		if lPlate != nil{
			aCoder.encode(lPlate, forKey: "l_plate")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if photo != nil{
			aCoder.encode(photo, forKey: "photo")
		}
		if prebookStatus != nil{
			aCoder.encode(prebookStatus, forKey: "prebook_status")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if vehicleColor != nil{
			aCoder.encode(vehicleColor, forKey: "vehicle_color")
		}
		if vehicleMake != nil{
			aCoder.encode(vehicleMake, forKey: "vehicle_make")
		}
		if vehiclePhoto != nil{
			aCoder.encode(vehiclePhoto, forKey: "vehicle_photo")
		}
		if vehicleSeats != nil{
			aCoder.encode(vehicleSeats, forKey: "vehicle_seats")
		}
		if vehicleType != nil{
			aCoder.encode(vehicleType, forKey: "vehicle_type")
		}
		if vehicleYear != nil{
			aCoder.encode(vehicleYear, forKey: "vehicle_year")
		}

	}

}
