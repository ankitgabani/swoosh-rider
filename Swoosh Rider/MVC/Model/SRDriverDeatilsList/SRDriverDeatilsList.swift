//
//	SRDriverDeatilsList.swift
//
//	Create by mac on 27/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRDriverDeatilsList : NSObject, NSCoding{

	var balance : String!
	var data : [SRDriverDeatilsListData]!
	var estimatedCost : String!
	var message : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		balance = dictionary["balance"] as? String == nil ? "" : dictionary["balance"] as? String
		data = [SRDriverDeatilsListData]()
		if let dataArray = dictionary["data"] as? [NSDictionary]{
			for dic in dataArray{
				let value = SRDriverDeatilsListData(fromDictionary: dic)
				data.append(value)
			}
		}
		estimatedCost = dictionary["estimated_cost"] as? String == nil ? "" : dictionary["estimated_cost"] as? String
		message = dictionary["message"] as? String == nil ? "" : dictionary["message"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if balance != nil{
			dictionary["balance"] = balance
		}
		if data != nil{
			var dictionaryElements = [NSDictionary]()
			for dataElement in data {
				dictionaryElements.append(dataElement.toDictionary())
			}
			dictionary["data"] = dictionaryElements
		}
		if estimatedCost != nil{
			dictionary["estimated_cost"] = estimatedCost
		}
		if message != nil{
			dictionary["message"] = message
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         balance = aDecoder.decodeObject(forKey: "balance") as? String
         data = aDecoder.decodeObject(forKey: "data") as? [SRDriverDeatilsListData]
         estimatedCost = aDecoder.decodeObject(forKey: "estimated_cost") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if balance != nil{
			aCoder.encode(balance, forKey: "balance")
		}
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if estimatedCost != nil{
			aCoder.encode(estimatedCost, forKey: "estimated_cost")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
