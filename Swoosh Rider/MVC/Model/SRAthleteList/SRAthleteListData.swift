//
//	SRAthleteListData.swift
//
//	Create by mac on 19/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRAthleteListData : NSObject, NSCoding{

	var address : String!
	var birthday : String!
	var city : String!
	var country : String!
	var email : String!
	var firstName : String!
	var gender : String!
	var id : String!
	var image : String!
	var jercyNumber : String!
	var lastName : String!
	var phone : String!
	var pincode : String!
	var position : String!
	var state : String!
	var status : String!
	var uId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		birthday = dictionary["birthday"] as? String == nil ? "" : dictionary["birthday"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		firstName = dictionary["first_name"] as? String == nil ? "" : dictionary["first_name"] as? String
		gender = dictionary["gender"] as? String == nil ? "" : dictionary["gender"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		jercyNumber = dictionary["jercy_number"] as? String == nil ? "" : dictionary["jercy_number"] as? String
		lastName = dictionary["last_name"] as? String == nil ? "" : dictionary["last_name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		position = dictionary["position"] as? String == nil ? "" : dictionary["position"] as? String
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		uId = dictionary["u_id"] as? String == nil ? "" : dictionary["u_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if birthday != nil{
			dictionary["birthday"] = birthday
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if email != nil{
			dictionary["email"] = email
		}
		if firstName != nil{
			dictionary["first_name"] = firstName
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if jercyNumber != nil{
			dictionary["jercy_number"] = jercyNumber
		}
		if lastName != nil{
			dictionary["last_name"] = lastName
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if position != nil{
			dictionary["position"] = position
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if uId != nil{
			dictionary["u_id"] = uId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         birthday = aDecoder.decodeObject(forKey: "birthday") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         jercyNumber = aDecoder.decodeObject(forKey: "jercy_number") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         position = aDecoder.decodeObject(forKey: "position") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         uId = aDecoder.decodeObject(forKey: "u_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if birthday != nil{
			aCoder.encode(birthday, forKey: "birthday")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if jercyNumber != nil{
			aCoder.encode(jercyNumber, forKey: "jercy_number")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if position != nil{
			aCoder.encode(position, forKey: "position")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if uId != nil{
			aCoder.encode(uId, forKey: "u_id")
		}

	}

}