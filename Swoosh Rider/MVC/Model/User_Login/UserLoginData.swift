//
//	UserLoginData.swift
//
//	Create by mac on 14/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserLoginData : NSObject, NSCoding{

	var countryId : String!
	var created : String!
	var email : String!
	var name : String!
	var password : String!
	var phone : String!
	var photo : String!
	var type : String!
	var userGender : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		created = dictionary["created"] as? String == nil ? "" : dictionary["created"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		password = dictionary["password"] as? String == nil ? "" : dictionary["password"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		photo = dictionary["photo"] as? String == nil ? "" : dictionary["photo"] as? String
		type = dictionary["type"] as? String == nil ? "" : dictionary["type"] as? String
		userGender = dictionary["user_gender"] as? String == nil ? "" : dictionary["user_gender"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if created != nil{
			dictionary["created"] = created
		}
		if email != nil{
			dictionary["email"] = email
		}
		if name != nil{
			dictionary["name"] = name
		}
		if password != nil{
			dictionary["password"] = password
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if photo != nil{
			dictionary["photo"] = photo
		}
		if type != nil{
			dictionary["type"] = type
		}
		if userGender != nil{
			dictionary["user_gender"] = userGender
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         created = aDecoder.decodeObject(forKey: "created") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         photo = aDecoder.decodeObject(forKey: "photo") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         userGender = aDecoder.decodeObject(forKey: "user_gender") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if photo != nil{
			aCoder.encode(photo, forKey: "photo")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if userGender != nil{
			aCoder.encode(userGender, forKey: "user_gender")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}
