//
//	SRCareBookingData.swift
//
//	Create by mac on 26/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRCareBookingData : NSObject, NSCoding{

	var athleteName : String!
	var bookingId : String!
	var clubName : String!
	var cost : String!
	var dateTime : String!
	var driverId : String!
	var driverName : String!
	var driverPhoto : String!
	var driverRating : String!
	var endLang : String!
	var endLat : String!
	var eventDateTime : String!
	var fromLocation : String!
	var phone : String!
	var showbookingId : String!
	var startLang : String!
	var startLat : String!
	var status : String!
	var teamName : String!
	var toLocation : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		athleteName = dictionary["athlete_name"] as? String == nil ? "" : dictionary["athlete_name"] as? String
		bookingId = dictionary["booking_id"] as? String == nil ? "" : dictionary["booking_id"] as? String
		clubName = dictionary["club_name"] as? String == nil ? "" : dictionary["club_name"] as? String
		cost = dictionary["cost"] as? String == nil ? "" : dictionary["cost"] as? String
		dateTime = dictionary["date_time"] as? String == nil ? "" : dictionary["date_time"] as? String
		driverId = dictionary["driver_id"] as? String == nil ? "" : dictionary["driver_id"] as? String
		driverName = dictionary["driver_name"] as? String == nil ? "" : dictionary["driver_name"] as? String
		driverPhoto = dictionary["driver_photo"] as? String == nil ? "" : dictionary["driver_photo"] as? String
		driverRating = dictionary["driver_rating"] as? String == nil ? "" : dictionary["driver_rating"] as? String
		endLang = dictionary["end_lang"] as? String == nil ? "" : dictionary["end_lang"] as? String
		endLat = dictionary["end_lat"] as? String == nil ? "" : dictionary["end_lat"] as? String
		eventDateTime = dictionary["event_date_time"] as? String == nil ? "" : dictionary["event_date_time"] as? String
		fromLocation = dictionary["from_location"] as? String == nil ? "" : dictionary["from_location"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		showbookingId = dictionary["showbooking_id"] as? String == nil ? "" : dictionary["showbooking_id"] as? String
		startLang = dictionary["start_lang"] as? String == nil ? "" : dictionary["start_lang"] as? String
		startLat = dictionary["start_lat"] as? String == nil ? "" : dictionary["start_lat"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		teamName = dictionary["team_name"] as? String == nil ? "" : dictionary["team_name"] as? String
		toLocation = dictionary["to_location"] as? String == nil ? "" : dictionary["to_location"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if athleteName != nil{
			dictionary["athlete_name"] = athleteName
		}
		if bookingId != nil{
			dictionary["booking_id"] = bookingId
		}
		if clubName != nil{
			dictionary["club_name"] = clubName
		}
		if cost != nil{
			dictionary["cost"] = cost
		}
		if dateTime != nil{
			dictionary["date_time"] = dateTime
		}
		if driverId != nil{
			dictionary["driver_id"] = driverId
		}
		if driverName != nil{
			dictionary["driver_name"] = driverName
		}
		if driverPhoto != nil{
			dictionary["driver_photo"] = driverPhoto
		}
		if driverRating != nil{
			dictionary["driver_rating"] = driverRating
		}
		if endLang != nil{
			dictionary["end_lang"] = endLang
		}
		if endLat != nil{
			dictionary["end_lat"] = endLat
		}
		if eventDateTime != nil{
			dictionary["event_date_time"] = eventDateTime
		}
		if fromLocation != nil{
			dictionary["from_location"] = fromLocation
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if showbookingId != nil{
			dictionary["showbooking_id"] = showbookingId
		}
		if startLang != nil{
			dictionary["start_lang"] = startLang
		}
		if startLat != nil{
			dictionary["start_lat"] = startLat
		}
		if status != nil{
			dictionary["status"] = status
		}
		if teamName != nil{
			dictionary["team_name"] = teamName
		}
		if toLocation != nil{
			dictionary["to_location"] = toLocation
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         athleteName = aDecoder.decodeObject(forKey: "athlete_name") as? String
         bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
         clubName = aDecoder.decodeObject(forKey: "club_name") as? String
         cost = aDecoder.decodeObject(forKey: "cost") as? String
         dateTime = aDecoder.decodeObject(forKey: "date_time") as? String
         driverId = aDecoder.decodeObject(forKey: "driver_id") as? String
         driverName = aDecoder.decodeObject(forKey: "driver_name") as? String
         driverPhoto = aDecoder.decodeObject(forKey: "driver_photo") as? String
         driverRating = aDecoder.decodeObject(forKey: "driver_rating") as? String
         endLang = aDecoder.decodeObject(forKey: "end_lang") as? String
         endLat = aDecoder.decodeObject(forKey: "end_lat") as? String
         eventDateTime = aDecoder.decodeObject(forKey: "event_date_time") as? String
         fromLocation = aDecoder.decodeObject(forKey: "from_location") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         showbookingId = aDecoder.decodeObject(forKey: "showbooking_id") as? String
         startLang = aDecoder.decodeObject(forKey: "start_lang") as? String
         startLat = aDecoder.decodeObject(forKey: "start_lat") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         teamName = aDecoder.decodeObject(forKey: "team_name") as? String
         toLocation = aDecoder.decodeObject(forKey: "to_location") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if athleteName != nil{
			aCoder.encode(athleteName, forKey: "athlete_name")
		}
		if bookingId != nil{
			aCoder.encode(bookingId, forKey: "booking_id")
		}
		if clubName != nil{
			aCoder.encode(clubName, forKey: "club_name")
		}
		if cost != nil{
			aCoder.encode(cost, forKey: "cost")
		}
		if dateTime != nil{
			aCoder.encode(dateTime, forKey: "date_time")
		}
		if driverId != nil{
			aCoder.encode(driverId, forKey: "driver_id")
		}
		if driverName != nil{
			aCoder.encode(driverName, forKey: "driver_name")
		}
		if driverPhoto != nil{
			aCoder.encode(driverPhoto, forKey: "driver_photo")
		}
		if driverRating != nil{
			aCoder.encode(driverRating, forKey: "driver_rating")
		}
		if endLang != nil{
			aCoder.encode(endLang, forKey: "end_lang")
		}
		if endLat != nil{
			aCoder.encode(endLat, forKey: "end_lat")
		}
		if eventDateTime != nil{
			aCoder.encode(eventDateTime, forKey: "event_date_time")
		}
		if fromLocation != nil{
			aCoder.encode(fromLocation, forKey: "from_location")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if showbookingId != nil{
			aCoder.encode(showbookingId, forKey: "showbooking_id")
		}
		if startLang != nil{
			aCoder.encode(startLang, forKey: "start_lang")
		}
		if startLat != nil{
			aCoder.encode(startLat, forKey: "start_lat")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if teamName != nil{
			aCoder.encode(teamName, forKey: "team_name")
		}
		if toLocation != nil{
			aCoder.encode(toLocation, forKey: "to_location")
		}

	}

}