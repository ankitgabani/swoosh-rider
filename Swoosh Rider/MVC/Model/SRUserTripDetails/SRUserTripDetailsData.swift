//
//	SRUserTripDetailsData.swift
//
//	Create by mac on 28/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRUserTripDetailsData : NSObject, NSCoding{

	var athleteName : String!
	var bookingDate : String!
	var bookingDevice : String!
	var bookingId : String!
	var bookingTime : String!
	var bookingWayType : String!
	var carName : String!
	var customerId : String!
	var driverName : String!
	var driverPhoto : String!
	var endDate : String!
	var endLang : String!
	var endLat : String!
	var endLocation : String!
	var endTime : String!
	var estimatedFare : String!
	var eventName : String!
	var paymentMethod : String!
	var rideName : String!
	var showBookingId : String!
	var startDate : String!
	var startLang : String!
	var startLat : String!
	var startLocation : String!
	var startTime : String!
	var status : String!
	var tax : String!
	var taxRate : String!
	var tollTax : String!
	var totalDistance : String!
	var totalFare : String!
	var totalTime : String!
	var type : String!
	var waitingCost : String!
	var waitingTime : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		athleteName = dictionary["athlete_name"] as? String == nil ? "" : dictionary["athlete_name"] as? String
		bookingDate = dictionary["booking_date"] as? String == nil ? "" : dictionary["booking_date"] as? String
		bookingDevice = dictionary["booking_device"] as? String == nil ? "" : dictionary["booking_device"] as? String
		bookingId = dictionary["booking_id"] as? String == nil ? "" : dictionary["booking_id"] as? String
		bookingTime = dictionary["booking_time"] as? String == nil ? "" : dictionary["booking_time"] as? String
		bookingWayType = dictionary["booking_way_type"] as? String == nil ? "" : dictionary["booking_way_type"] as? String
		carName = dictionary["car_name"] as? String == nil ? "" : dictionary["car_name"] as? String
		customerId = dictionary["customer_id"] as? String == nil ? "" : dictionary["customer_id"] as? String
		driverName = dictionary["driver_name"] as? String == nil ? "" : dictionary["driver_name"] as? String
		driverPhoto = dictionary["driver_photo"] as? String == nil ? "" : dictionary["driver_photo"] as? String
		endDate = dictionary["end_date"] as? String == nil ? "" : dictionary["end_date"] as? String
		endLang = dictionary["end_lang"] as? String == nil ? "" : dictionary["end_lang"] as? String
		endLat = dictionary["end_lat"] as? String == nil ? "" : dictionary["end_lat"] as? String
		endLocation = dictionary["end_location"] as? String == nil ? "" : dictionary["end_location"] as? String
		endTime = dictionary["end_time"] as? String == nil ? "" : dictionary["end_time"] as? String
		estimatedFare = dictionary["estimated_fare"] as? String == nil ? "" : dictionary["estimated_fare"] as? String
		eventName = dictionary["event_name"] as? String == nil ? "" : dictionary["event_name"] as? String
		paymentMethod = dictionary["payment_method"] as? String == nil ? "" : dictionary["payment_method"] as? String
		rideName = dictionary["ride_name"] as? String == nil ? "" : dictionary["ride_name"] as? String
		showBookingId = dictionary["show_booking_id"] as? String == nil ? "" : dictionary["show_booking_id"] as? String
		startDate = dictionary["start_date"] as? String == nil ? "" : dictionary["start_date"] as? String
		startLang = dictionary["start_lang"] as? String == nil ? "" : dictionary["start_lang"] as? String
		startLat = dictionary["start_lat"] as? String == nil ? "" : dictionary["start_lat"] as? String
		startLocation = dictionary["start_location"] as? String == nil ? "" : dictionary["start_location"] as? String
		startTime = dictionary["start_time"] as? String == nil ? "" : dictionary["start_time"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		tax = dictionary["tax"] as? String == nil ? "" : dictionary["tax"] as? String
		taxRate = dictionary["tax_rate"] as? String == nil ? "" : dictionary["tax_rate"] as? String
		tollTax = dictionary["toll_tax"] as? String == nil ? "" : dictionary["toll_tax"] as? String
		totalDistance = dictionary["total_distance"] as? String == nil ? "" : dictionary["total_distance"] as? String
		totalFare = dictionary["total_fare"] as? String == nil ? "" : dictionary["total_fare"] as? String
		totalTime = dictionary["total_time"] as? String == nil ? "" : dictionary["total_time"] as? String
		type = dictionary["type"] as? String == nil ? "" : dictionary["type"] as? String
		waitingCost = dictionary["waiting_cost"] as? String == nil ? "" : dictionary["waiting_cost"] as? String
		waitingTime = dictionary["waiting_time"] as? String == nil ? "" : dictionary["waiting_time"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if athleteName != nil{
			dictionary["athlete_name"] = athleteName
		}
		if bookingDate != nil{
			dictionary["booking_date"] = bookingDate
		}
		if bookingDevice != nil{
			dictionary["booking_device"] = bookingDevice
		}
		if bookingId != nil{
			dictionary["booking_id"] = bookingId
		}
		if bookingTime != nil{
			dictionary["booking_time"] = bookingTime
		}
		if bookingWayType != nil{
			dictionary["booking_way_type"] = bookingWayType
		}
		if carName != nil{
			dictionary["car_name"] = carName
		}
		if customerId != nil{
			dictionary["customer_id"] = customerId
		}
		if driverName != nil{
			dictionary["driver_name"] = driverName
		}
		if driverPhoto != nil{
			dictionary["driver_photo"] = driverPhoto
		}
		if endDate != nil{
			dictionary["end_date"] = endDate
		}
		if endLang != nil{
			dictionary["end_lang"] = endLang
		}
		if endLat != nil{
			dictionary["end_lat"] = endLat
		}
		if endLocation != nil{
			dictionary["end_location"] = endLocation
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if estimatedFare != nil{
			dictionary["estimated_fare"] = estimatedFare
		}
		if eventName != nil{
			dictionary["event_name"] = eventName
		}
		if paymentMethod != nil{
			dictionary["payment_method"] = paymentMethod
		}
		if rideName != nil{
			dictionary["ride_name"] = rideName
		}
		if showBookingId != nil{
			dictionary["show_booking_id"] = showBookingId
		}
		if startDate != nil{
			dictionary["start_date"] = startDate
		}
		if startLang != nil{
			dictionary["start_lang"] = startLang
		}
		if startLat != nil{
			dictionary["start_lat"] = startLat
		}
		if startLocation != nil{
			dictionary["start_location"] = startLocation
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if status != nil{
			dictionary["status"] = status
		}
		if tax != nil{
			dictionary["tax"] = tax
		}
		if taxRate != nil{
			dictionary["tax_rate"] = taxRate
		}
		if tollTax != nil{
			dictionary["toll_tax"] = tollTax
		}
		if totalDistance != nil{
			dictionary["total_distance"] = totalDistance
		}
		if totalFare != nil{
			dictionary["total_fare"] = totalFare
		}
		if totalTime != nil{
			dictionary["total_time"] = totalTime
		}
		if type != nil{
			dictionary["type"] = type
		}
		if waitingCost != nil{
			dictionary["waiting_cost"] = waitingCost
		}
		if waitingTime != nil{
			dictionary["waiting_time"] = waitingTime
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         athleteName = aDecoder.decodeObject(forKey: "athlete_name") as? String
         bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
         bookingDevice = aDecoder.decodeObject(forKey: "booking_device") as? String
         bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
         bookingTime = aDecoder.decodeObject(forKey: "booking_time") as? String
         bookingWayType = aDecoder.decodeObject(forKey: "booking_way_type") as? String
         carName = aDecoder.decodeObject(forKey: "car_name") as? String
         customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
         driverName = aDecoder.decodeObject(forKey: "driver_name") as? String
         driverPhoto = aDecoder.decodeObject(forKey: "driver_photo") as? String
         endDate = aDecoder.decodeObject(forKey: "end_date") as? String
         endLang = aDecoder.decodeObject(forKey: "end_lang") as? String
         endLat = aDecoder.decodeObject(forKey: "end_lat") as? String
         endLocation = aDecoder.decodeObject(forKey: "end_location") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? String
         estimatedFare = aDecoder.decodeObject(forKey: "estimated_fare") as? String
         eventName = aDecoder.decodeObject(forKey: "event_name") as? String
         paymentMethod = aDecoder.decodeObject(forKey: "payment_method") as? String
         rideName = aDecoder.decodeObject(forKey: "ride_name") as? String
         showBookingId = aDecoder.decodeObject(forKey: "show_booking_id") as? String
         startDate = aDecoder.decodeObject(forKey: "start_date") as? String
         startLang = aDecoder.decodeObject(forKey: "start_lang") as? String
         startLat = aDecoder.decodeObject(forKey: "start_lat") as? String
         startLocation = aDecoder.decodeObject(forKey: "start_location") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         tax = aDecoder.decodeObject(forKey: "tax") as? String
         taxRate = aDecoder.decodeObject(forKey: "tax_rate") as? String
         tollTax = aDecoder.decodeObject(forKey: "toll_tax") as? String
         totalDistance = aDecoder.decodeObject(forKey: "total_distance") as? String
         totalFare = aDecoder.decodeObject(forKey: "total_fare") as? String
         totalTime = aDecoder.decodeObject(forKey: "total_time") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         waitingCost = aDecoder.decodeObject(forKey: "waiting_cost") as? String
         waitingTime = aDecoder.decodeObject(forKey: "waiting_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if athleteName != nil{
			aCoder.encode(athleteName, forKey: "athlete_name")
		}
		if bookingDate != nil{
			aCoder.encode(bookingDate, forKey: "booking_date")
		}
		if bookingDevice != nil{
			aCoder.encode(bookingDevice, forKey: "booking_device")
		}
		if bookingId != nil{
			aCoder.encode(bookingId, forKey: "booking_id")
		}
		if bookingTime != nil{
			aCoder.encode(bookingTime, forKey: "booking_time")
		}
		if bookingWayType != nil{
			aCoder.encode(bookingWayType, forKey: "booking_way_type")
		}
		if carName != nil{
			aCoder.encode(carName, forKey: "car_name")
		}
		if customerId != nil{
			aCoder.encode(customerId, forKey: "customer_id")
		}
		if driverName != nil{
			aCoder.encode(driverName, forKey: "driver_name")
		}
		if driverPhoto != nil{
			aCoder.encode(driverPhoto, forKey: "driver_photo")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "end_date")
		}
		if endLang != nil{
			aCoder.encode(endLang, forKey: "end_lang")
		}
		if endLat != nil{
			aCoder.encode(endLat, forKey: "end_lat")
		}
		if endLocation != nil{
			aCoder.encode(endLocation, forKey: "end_location")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if estimatedFare != nil{
			aCoder.encode(estimatedFare, forKey: "estimated_fare")
		}
		if eventName != nil{
			aCoder.encode(eventName, forKey: "event_name")
		}
		if paymentMethod != nil{
			aCoder.encode(paymentMethod, forKey: "payment_method")
		}
		if rideName != nil{
			aCoder.encode(rideName, forKey: "ride_name")
		}
		if showBookingId != nil{
			aCoder.encode(showBookingId, forKey: "show_booking_id")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "start_date")
		}
		if startLang != nil{
			aCoder.encode(startLang, forKey: "start_lang")
		}
		if startLat != nil{
			aCoder.encode(startLat, forKey: "start_lat")
		}
		if startLocation != nil{
			aCoder.encode(startLocation, forKey: "start_location")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if tax != nil{
			aCoder.encode(tax, forKey: "tax")
		}
		if taxRate != nil{
			aCoder.encode(taxRate, forKey: "tax_rate")
		}
		if tollTax != nil{
			aCoder.encode(tollTax, forKey: "toll_tax")
		}
		if totalDistance != nil{
			aCoder.encode(totalDistance, forKey: "total_distance")
		}
		if totalFare != nil{
			aCoder.encode(totalFare, forKey: "total_fare")
		}
		if totalTime != nil{
			aCoder.encode(totalTime, forKey: "total_time")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if waitingCost != nil{
			aCoder.encode(waitingCost, forKey: "waiting_cost")
		}
		if waitingTime != nil{
			aCoder.encode(waitingTime, forKey: "waiting_time")
		}

	}

}