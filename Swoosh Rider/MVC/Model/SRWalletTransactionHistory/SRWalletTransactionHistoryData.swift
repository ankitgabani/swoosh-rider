//
//	SRWalletTransactionHistoryData.swift
//
//	Create by mac on 26/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRWalletTransactionHistoryData : NSObject, NSCoding{

	var amountCharge : String!
	var amountSpend : String!
	var cusId : String!
	var dateTime : String!
	var id : String!
	var payAdmin : String!
	var status : String!
	var transactionId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		amountCharge = dictionary["amount_charge"] as? String == nil ? "" : dictionary["amount_charge"] as? String
		amountSpend = dictionary["amount_spend"] as? String == nil ? "" : dictionary["amount_spend"] as? String
		cusId = dictionary["cus_id"] as? String == nil ? "" : dictionary["cus_id"] as? String
		dateTime = dictionary["date_time"] as? String == nil ? "" : dictionary["date_time"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		payAdmin = dictionary["pay_admin"] as? String == nil ? "" : dictionary["pay_admin"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		transactionId = dictionary["transaction_id"] as? String == nil ? "" : dictionary["transaction_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if amountCharge != nil{
			dictionary["amount_charge"] = amountCharge
		}
		if amountSpend != nil{
			dictionary["amount_spend"] = amountSpend
		}
		if cusId != nil{
			dictionary["cus_id"] = cusId
		}
		if dateTime != nil{
			dictionary["date_time"] = dateTime
		}
		if id != nil{
			dictionary["id"] = id
		}
		if payAdmin != nil{
			dictionary["pay_admin"] = payAdmin
		}
		if status != nil{
			dictionary["status"] = status
		}
		if transactionId != nil{
			dictionary["transaction_id"] = transactionId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         amountCharge = aDecoder.decodeObject(forKey: "amount_charge") as? String
         amountSpend = aDecoder.decodeObject(forKey: "amount_spend") as? String
         cusId = aDecoder.decodeObject(forKey: "cus_id") as? String
         dateTime = aDecoder.decodeObject(forKey: "date_time") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         payAdmin = aDecoder.decodeObject(forKey: "pay_admin") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         transactionId = aDecoder.decodeObject(forKey: "transaction_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if amountCharge != nil{
			aCoder.encode(amountCharge, forKey: "amount_charge")
		}
		if amountSpend != nil{
			aCoder.encode(amountSpend, forKey: "amount_spend")
		}
		if cusId != nil{
			aCoder.encode(cusId, forKey: "cus_id")
		}
		if dateTime != nil{
			aCoder.encode(dateTime, forKey: "date_time")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if payAdmin != nil{
			aCoder.encode(payAdmin, forKey: "pay_admin")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if transactionId != nil{
			aCoder.encode(transactionId, forKey: "transaction_id")
		}

	}

}