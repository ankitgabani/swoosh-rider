//
//	SRTeamUserData.swift
//
//	Create by mac on 23/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRTeamUserData : NSObject, NSCoding{

	var age : String!
	var city : String!
	var club : String!
	var coachId : String!
	var coachName : String!
	var country : String!
	var id : String!
	var pincode : String!
	var sportsType : String!
	var state : String!
	var status : String!
	var teamCode : String!
	var teamName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		age = dictionary["age"] as? String == nil ? "" : dictionary["age"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		club = dictionary["club"] as? String == nil ? "" : dictionary["club"] as? String
		coachId = dictionary["coach_id"] as? String == nil ? "" : dictionary["coach_id"] as? String
		coachName = dictionary["coach_name"] as? String == nil ? "" : dictionary["coach_name"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		sportsType = dictionary["sports_type"] as? String == nil ? "" : dictionary["sports_type"] as? String
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		teamCode = dictionary["team_code"] as? String == nil ? "" : dictionary["team_code"] as? String
		teamName = dictionary["team_name"] as? String == nil ? "" : dictionary["team_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if age != nil{
			dictionary["age"] = age
		}
		if city != nil{
			dictionary["city"] = city
		}
		if club != nil{
			dictionary["club"] = club
		}
		if coachId != nil{
			dictionary["coach_id"] = coachId
		}
		if coachName != nil{
			dictionary["coach_name"] = coachName
		}
		if country != nil{
			dictionary["country"] = country
		}
		if id != nil{
			dictionary["id"] = id
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if sportsType != nil{
			dictionary["sports_type"] = sportsType
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if teamCode != nil{
			dictionary["team_code"] = teamCode
		}
		if teamName != nil{
			dictionary["team_name"] = teamName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         age = aDecoder.decodeObject(forKey: "age") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         club = aDecoder.decodeObject(forKey: "club") as? String
         coachId = aDecoder.decodeObject(forKey: "coach_id") as? String
         coachName = aDecoder.decodeObject(forKey: "coach_name") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         sportsType = aDecoder.decodeObject(forKey: "sports_type") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         teamCode = aDecoder.decodeObject(forKey: "team_code") as? String
         teamName = aDecoder.decodeObject(forKey: "team_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if club != nil{
			aCoder.encode(club, forKey: "club")
		}
		if coachId != nil{
			aCoder.encode(coachId, forKey: "coach_id")
		}
		if coachName != nil{
			aCoder.encode(coachName, forKey: "coach_name")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if sportsType != nil{
			aCoder.encode(sportsType, forKey: "sports_type")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if teamCode != nil{
			aCoder.encode(teamCode, forKey: "team_code")
		}
		if teamName != nil{
			aCoder.encode(teamName, forKey: "team_name")
		}

	}

}