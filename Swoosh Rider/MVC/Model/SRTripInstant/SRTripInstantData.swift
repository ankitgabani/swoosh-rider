//
//	SRTripInstantData.swift
//
//	Create by mac on 29/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRTripInstantData : NSObject, NSCoding{

	var bookingDate : String!
	var bookingId : String!
	var bookingTime : String!
	var customerId : String!
	var driverAddress : String!
	var driverCity : String!
	var driverCountry : String!
	var driverEmail : String!
	var driverId : String!
	var driverLPlate : String!
	var driverName : String!
	var driverPhone : String!
	var driverPhoto : String!
	var endDate : String!
	var endLang : String!
	var endLat : String!
	var endLocation : String!
	var endOtp : String!
	var endTime : String!
	var estimatedFare : String!
	var locationName : String!
	var showBookingId : String!
	var startDate : String!
	var startLang : String!
	var startLat : String!
	var startLocation : String!
	var startOtp : String!
	var startTime : String!
	var status : String!
	var totalFare : String!
	var type : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		bookingDate = dictionary["booking_date"] as? String == nil ? "" : dictionary["booking_date"] as? String
		bookingId = dictionary["booking_id"] as? String == nil ? "" : dictionary["booking_id"] as? String
		bookingTime = dictionary["booking_time"] as? String == nil ? "" : dictionary["booking_time"] as? String
		customerId = dictionary["customer_id"] as? String == nil ? "" : dictionary["customer_id"] as? String
		driverAddress = dictionary["driver_address"] as? String == nil ? "" : dictionary["driver_address"] as? String
		driverCity = dictionary["driver_city"] as? String == nil ? "" : dictionary["driver_city"] as? String
		driverCountry = dictionary["driver_country"] as? String == nil ? "" : dictionary["driver_country"] as? String
		driverEmail = dictionary["driver_email"] as? String == nil ? "" : dictionary["driver_email"] as? String
		driverId = dictionary["driver_id"] as? String == nil ? "" : dictionary["driver_id"] as? String
		driverLPlate = dictionary["driver_l_plate"] as? String == nil ? "" : dictionary["driver_l_plate"] as? String
		driverName = dictionary["driver_name"] as? String == nil ? "" : dictionary["driver_name"] as? String
		driverPhone = dictionary["driver_phone"] as? String == nil ? "" : dictionary["driver_phone"] as? String
		driverPhoto = dictionary["driver_photo"] as? String == nil ? "" : dictionary["driver_photo"] as? String
		endDate = dictionary["end_date"] as? String == nil ? "" : dictionary["end_date"] as? String
		endLang = dictionary["end_lang"] as? String == nil ? "" : dictionary["end_lang"] as? String
		endLat = dictionary["end_lat"] as? String == nil ? "" : dictionary["end_lat"] as? String
		endLocation = dictionary["end_location"] as? String == nil ? "" : dictionary["end_location"] as? String
		endOtp = dictionary["end_otp"] as? String == nil ? "" : dictionary["end_otp"] as? String
		endTime = dictionary["end_time"] as? String == nil ? "" : dictionary["end_time"] as? String
		estimatedFare = dictionary["estimated_fare"] as? String == nil ? "" : dictionary["estimated_fare"] as? String
		locationName = dictionary["location_name"] as? String == nil ? "" : dictionary["location_name"] as? String
		showBookingId = dictionary["show_booking_id"] as? String == nil ? "" : dictionary["show_booking_id"] as? String
		startDate = dictionary["start_date"] as? String == nil ? "" : dictionary["start_date"] as? String
		startLang = dictionary["start_lang"] as? String == nil ? "" : dictionary["start_lang"] as? String
		startLat = dictionary["start_lat"] as? String == nil ? "" : dictionary["start_lat"] as? String
		startLocation = dictionary["start_location"] as? String == nil ? "" : dictionary["start_location"] as? String
		startOtp = dictionary["start_otp"] as? String == nil ? "" : dictionary["start_otp"] as? String
		startTime = dictionary["start_time"] as? String == nil ? "" : dictionary["start_time"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		totalFare = dictionary["total_fare"] as? String == nil ? "" : dictionary["total_fare"] as? String
		type = dictionary["type"] as? String == nil ? "" : dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if bookingDate != nil{
			dictionary["booking_date"] = bookingDate
		}
		if bookingId != nil{
			dictionary["booking_id"] = bookingId
		}
		if bookingTime != nil{
			dictionary["booking_time"] = bookingTime
		}
		if customerId != nil{
			dictionary["customer_id"] = customerId
		}
		if driverAddress != nil{
			dictionary["driver_address"] = driverAddress
		}
		if driverCity != nil{
			dictionary["driver_city"] = driverCity
		}
		if driverCountry != nil{
			dictionary["driver_country"] = driverCountry
		}
		if driverEmail != nil{
			dictionary["driver_email"] = driverEmail
		}
		if driverId != nil{
			dictionary["driver_id"] = driverId
		}
		if driverLPlate != nil{
			dictionary["driver_l_plate"] = driverLPlate
		}
		if driverName != nil{
			dictionary["driver_name"] = driverName
		}
		if driverPhone != nil{
			dictionary["driver_phone"] = driverPhone
		}
		if driverPhoto != nil{
			dictionary["driver_photo"] = driverPhoto
		}
		if endDate != nil{
			dictionary["end_date"] = endDate
		}
		if endLang != nil{
			dictionary["end_lang"] = endLang
		}
		if endLat != nil{
			dictionary["end_lat"] = endLat
		}
		if endLocation != nil{
			dictionary["end_location"] = endLocation
		}
		if endOtp != nil{
			dictionary["end_otp"] = endOtp
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if estimatedFare != nil{
			dictionary["estimated_fare"] = estimatedFare
		}
		if locationName != nil{
			dictionary["location_name"] = locationName
		}
		if showBookingId != nil{
			dictionary["show_booking_id"] = showBookingId
		}
		if startDate != nil{
			dictionary["start_date"] = startDate
		}
		if startLang != nil{
			dictionary["start_lang"] = startLang
		}
		if startLat != nil{
			dictionary["start_lat"] = startLat
		}
		if startLocation != nil{
			dictionary["start_location"] = startLocation
		}
		if startOtp != nil{
			dictionary["start_otp"] = startOtp
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if status != nil{
			dictionary["status"] = status
		}
		if totalFare != nil{
			dictionary["total_fare"] = totalFare
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
         bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
         bookingTime = aDecoder.decodeObject(forKey: "booking_time") as? String
         customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
         driverAddress = aDecoder.decodeObject(forKey: "driver_address") as? String
         driverCity = aDecoder.decodeObject(forKey: "driver_city") as? String
         driverCountry = aDecoder.decodeObject(forKey: "driver_country") as? String
         driverEmail = aDecoder.decodeObject(forKey: "driver_email") as? String
         driverId = aDecoder.decodeObject(forKey: "driver_id") as? String
         driverLPlate = aDecoder.decodeObject(forKey: "driver_l_plate") as? String
         driverName = aDecoder.decodeObject(forKey: "driver_name") as? String
         driverPhone = aDecoder.decodeObject(forKey: "driver_phone") as? String
         driverPhoto = aDecoder.decodeObject(forKey: "driver_photo") as? String
         endDate = aDecoder.decodeObject(forKey: "end_date") as? String
         endLang = aDecoder.decodeObject(forKey: "end_lang") as? String
         endLat = aDecoder.decodeObject(forKey: "end_lat") as? String
         endLocation = aDecoder.decodeObject(forKey: "end_location") as? String
         endOtp = aDecoder.decodeObject(forKey: "end_otp") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? String
         estimatedFare = aDecoder.decodeObject(forKey: "estimated_fare") as? String
         locationName = aDecoder.decodeObject(forKey: "location_name") as? String
         showBookingId = aDecoder.decodeObject(forKey: "show_booking_id") as? String
         startDate = aDecoder.decodeObject(forKey: "start_date") as? String
         startLang = aDecoder.decodeObject(forKey: "start_lang") as? String
         startLat = aDecoder.decodeObject(forKey: "start_lat") as? String
         startLocation = aDecoder.decodeObject(forKey: "start_location") as? String
         startOtp = aDecoder.decodeObject(forKey: "start_otp") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         totalFare = aDecoder.decodeObject(forKey: "total_fare") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if bookingDate != nil{
			aCoder.encode(bookingDate, forKey: "booking_date")
		}
		if bookingId != nil{
			aCoder.encode(bookingId, forKey: "booking_id")
		}
		if bookingTime != nil{
			aCoder.encode(bookingTime, forKey: "booking_time")
		}
		if customerId != nil{
			aCoder.encode(customerId, forKey: "customer_id")
		}
		if driverAddress != nil{
			aCoder.encode(driverAddress, forKey: "driver_address")
		}
		if driverCity != nil{
			aCoder.encode(driverCity, forKey: "driver_city")
		}
		if driverCountry != nil{
			aCoder.encode(driverCountry, forKey: "driver_country")
		}
		if driverEmail != nil{
			aCoder.encode(driverEmail, forKey: "driver_email")
		}
		if driverId != nil{
			aCoder.encode(driverId, forKey: "driver_id")
		}
		if driverLPlate != nil{
			aCoder.encode(driverLPlate, forKey: "driver_l_plate")
		}
		if driverName != nil{
			aCoder.encode(driverName, forKey: "driver_name")
		}
		if driverPhone != nil{
			aCoder.encode(driverPhone, forKey: "driver_phone")
		}
		if driverPhoto != nil{
			aCoder.encode(driverPhoto, forKey: "driver_photo")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "end_date")
		}
		if endLang != nil{
			aCoder.encode(endLang, forKey: "end_lang")
		}
		if endLat != nil{
			aCoder.encode(endLat, forKey: "end_lat")
		}
		if endLocation != nil{
			aCoder.encode(endLocation, forKey: "end_location")
		}
		if endOtp != nil{
			aCoder.encode(endOtp, forKey: "end_otp")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if estimatedFare != nil{
			aCoder.encode(estimatedFare, forKey: "estimated_fare")
		}
		if locationName != nil{
			aCoder.encode(locationName, forKey: "location_name")
		}
		if showBookingId != nil{
			aCoder.encode(showBookingId, forKey: "show_booking_id")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "start_date")
		}
		if startLang != nil{
			aCoder.encode(startLang, forKey: "start_lang")
		}
		if startLat != nil{
			aCoder.encode(startLat, forKey: "start_lat")
		}
		if startLocation != nil{
			aCoder.encode(startLocation, forKey: "start_location")
		}
		if startOtp != nil{
			aCoder.encode(startOtp, forKey: "start_otp")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if totalFare != nil{
			aCoder.encode(totalFare, forKey: "total_fare")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}
