//
//	SRCareEventListData.swift
//
//	Create by mac on 27/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRCareEventListData : NSObject, NSCoding{

	var date : String!
	var day : String!
	var endTime : String!
	var eventId : String!
	var eventName : String!
	var location : String!
	var month : String!
	var startTime : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		date = dictionary["date"] as? String == nil ? "" : dictionary["date"] as? String
		day = dictionary["day"] as? String == nil ? "" : dictionary["day"] as? String
		endTime = dictionary["end_time"] as? String == nil ? "" : dictionary["end_time"] as? String
		eventId = dictionary["event_id"] as? String == nil ? "" : dictionary["event_id"] as? String
		eventName = dictionary["event_name"] as? String == nil ? "" : dictionary["event_name"] as? String
		location = dictionary["location"] as? String == nil ? "" : dictionary["location"] as? String
		month = dictionary["month"] as? String == nil ? "" : dictionary["month"] as? String
		startTime = dictionary["start_time"] as? String == nil ? "" : dictionary["start_time"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if date != nil{
			dictionary["date"] = date
		}
		if day != nil{
			dictionary["day"] = day
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if eventId != nil{
			dictionary["event_id"] = eventId
		}
		if eventName != nil{
			dictionary["event_name"] = eventName
		}
		if location != nil{
			dictionary["location"] = location
		}
		if month != nil{
			dictionary["month"] = month
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "date") as? String
         day = aDecoder.decodeObject(forKey: "day") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? String
         eventId = aDecoder.decodeObject(forKey: "event_id") as? String
         eventName = aDecoder.decodeObject(forKey: "event_name") as? String
         location = aDecoder.decodeObject(forKey: "location") as? String
         month = aDecoder.decodeObject(forKey: "month") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if day != nil{
			aCoder.encode(day, forKey: "day")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if eventId != nil{
			aCoder.encode(eventId, forKey: "event_id")
		}
		if eventName != nil{
			aCoder.encode(eventName, forKey: "event_name")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if month != nil{
			aCoder.encode(month, forKey: "month")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}