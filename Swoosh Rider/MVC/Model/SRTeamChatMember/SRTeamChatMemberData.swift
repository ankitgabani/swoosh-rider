//
//	SRTeamChatMemberData.swift
//
//	Create by mac on 20/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRTeamChatMemberData : NSObject, NSCoding{

	var check : String!
	var dateTime : String!
	var id : String!
	var message : String!
	var name : String!
	var phone : String!
	var photo : String!
	var rowId : String!
	var status : String!
	var userType : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		check = dictionary["check"] as? String == nil ? "" : dictionary["check"] as? String
		dateTime = dictionary["date_time"] as? String == nil ? "" : dictionary["date_time"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		message = dictionary["message"] as? String == nil ? "" : dictionary["message"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		photo = dictionary["photo"] as? String == nil ? "" : dictionary["photo"] as? String
		rowId = dictionary["row_id"] as? String == nil ? "" : dictionary["row_id"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		userType = dictionary["user_type"] as? String == nil ? "" : dictionary["user_type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if check != nil{
			dictionary["check"] = check
		}
		if dateTime != nil{
			dictionary["date_time"] = dateTime
		}
		if id != nil{
			dictionary["id"] = id
		}
		if message != nil{
			dictionary["message"] = message
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if photo != nil{
			dictionary["photo"] = photo
		}
		if rowId != nil{
			dictionary["row_id"] = rowId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userType != nil{
			dictionary["user_type"] = userType
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         check = aDecoder.decodeObject(forKey: "check") as? String
         dateTime = aDecoder.decodeObject(forKey: "date_time") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         photo = aDecoder.decodeObject(forKey: "photo") as? String
         rowId = aDecoder.decodeObject(forKey: "row_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         userType = aDecoder.decodeObject(forKey: "user_type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if check != nil{
			aCoder.encode(check, forKey: "check")
		}
		if dateTime != nil{
			aCoder.encode(dateTime, forKey: "date_time")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if photo != nil{
			aCoder.encode(photo, forKey: "photo")
		}
		if rowId != nil{
			aCoder.encode(rowId, forKey: "row_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if userType != nil{
			aCoder.encode(userType, forKey: "user_type")
		}

	}

}