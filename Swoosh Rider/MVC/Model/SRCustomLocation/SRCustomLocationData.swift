//
//	SRCustomLocationData.swift
//
//	Create by mac on 19/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRCustomLocationData : NSObject, NSCoding{

	var id : String!
	var lang : String!
	var lat : String!
	var location : String!
	var locationName : String!
	var status : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		lang = dictionary["lang"] as? String == nil ? "" : dictionary["lang"] as? String
		lat = dictionary["lat"] as? String == nil ? "" : dictionary["lat"] as? String
		location = dictionary["location"] as? String == nil ? "" : dictionary["location"] as? String
		locationName = dictionary["location_name"] as? String == nil ? "" : dictionary["location_name"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["id"] = id
		}
		if lang != nil{
			dictionary["lang"] = lang
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if location != nil{
			dictionary["location"] = location
		}
		if locationName != nil{
			dictionary["location_name"] = locationName
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         lang = aDecoder.decodeObject(forKey: "lang") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? String
         location = aDecoder.decodeObject(forKey: "location") as? String
         locationName = aDecoder.decodeObject(forKey: "location_name") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lang != nil{
			aCoder.encode(lang, forKey: "lang")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if locationName != nil{
			aCoder.encode(locationName, forKey: "location_name")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}