//
//	SRChatToCareRideData.swift
//
//	Create by mac on 23/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRChatToCareRideData : NSObject, NSCoding{

	var athleteName : String!
	var caochName : String!
	var dateChange : String!
	var dateTime : String!
	var message : String!
	var receiveAthleteName : String!
	var sendAthleteName : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		athleteName = dictionary["athlete_name"] as? String == nil ? "" : dictionary["athlete_name"] as? String
		caochName = dictionary["caoch_name"] as? String == nil ? "" : dictionary["caoch_name"] as? String
		dateChange = dictionary["date_change"] as? String == nil ? "" : dictionary["date_change"] as? String
		dateTime = dictionary["date_time"] as? String == nil ? "" : dictionary["date_time"] as? String
		message = dictionary["message"] as? String == nil ? "" : dictionary["message"] as? String
		receiveAthleteName = dictionary["receive_athlete_name"] as? String == nil ? "" : dictionary["receive_athlete_name"] as? String
		sendAthleteName = dictionary["send_athlete_name"] as? String == nil ? "" : dictionary["send_athlete_name"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if athleteName != nil{
			dictionary["athlete_name"] = athleteName
		}
		if caochName != nil{
			dictionary["caoch_name"] = caochName
		}
		if dateChange != nil{
			dictionary["date_change"] = dateChange
		}
		if dateTime != nil{
			dictionary["date_time"] = dateTime
		}
		if message != nil{
			dictionary["message"] = message
		}
		if receiveAthleteName != nil{
			dictionary["receive_athlete_name"] = receiveAthleteName
		}
		if sendAthleteName != nil{
			dictionary["send_athlete_name"] = sendAthleteName
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         athleteName = aDecoder.decodeObject(forKey: "athlete_name") as? String
         caochName = aDecoder.decodeObject(forKey: "caoch_name") as? String
         dateChange = aDecoder.decodeObject(forKey: "date_change") as? String
         dateTime = aDecoder.decodeObject(forKey: "date_time") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         receiveAthleteName = aDecoder.decodeObject(forKey: "receive_athlete_name") as? String
         sendAthleteName = aDecoder.decodeObject(forKey: "send_athlete_name") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if athleteName != nil{
			aCoder.encode(athleteName, forKey: "athlete_name")
		}
		if caochName != nil{
			aCoder.encode(caochName, forKey: "caoch_name")
		}
		if dateChange != nil{
			aCoder.encode(dateChange, forKey: "date_change")
		}
		if dateTime != nil{
			aCoder.encode(dateTime, forKey: "date_time")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if receiveAthleteName != nil{
			aCoder.encode(receiveAthleteName, forKey: "receive_athlete_name")
		}
		if sendAthleteName != nil{
			aCoder.encode(sendAthleteName, forKey: "send_athlete_name")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}