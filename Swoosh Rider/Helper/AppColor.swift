//
//  AppColor.swift
//  Swoosh Rider
//
//  Created by Gabani King on 01/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation
import UIKit

//<color name="secondary_text">#727272</color>
let SECONDARY_TEXT_COLOR = UIColor(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)


//<color name="grey">#d3d3d3</color>
let GRAY_COLOR = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)


//<color name="greyLight">#dfdfdf</color>
let GRAY_LIGHT_COLOR = UIColor(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)


//<color name="greyLight2">#F3F3F3</color> // background
let GRAY_LIGHT_2_COLOR = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)


//<color name="greyLight3">#e5e6e9</color>
let GRAY_LIGHT_3_COLOR = UIColor(red: 229/255, green: 230/255, blue: 233/255, alpha: 1)


//<color name="colorhint">#555</color>
let COLOR_HINT = UIColor(red: 85/255, green: 80/255, blue: 0/255, alpha: 1)


//<color name="textColor">#656565</color> // Dark gray
let TEXT_COLOR = UIColor(red: 101/255, green: 101/255, blue: 101/255, alpha: 1)


//<color name="colorSwooshBack">#efefef</color> 
let COLOR_SWOOSH_BACK = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)


//<color name="colorSwoosh">#F59D0E</color> // ORANGE COLOR
let COLOR_SWOOSH = UIColor(red: 245/255, green: 157/255, blue: 14/255, alpha: 1)


//<color name="swoosh_back_back">#1DA1F2</color> // BG COLOR BULE
let SWOOSH_BACK_BACK = UIColor(red: 29/255, green: 161/255, blue: 242/255, alpha: 1)


//<color name="swoosh_back">#0077B5</color> // DARK BULE
let SWOOSH_BACK = UIColor(red: 0/255, green: 119/255, blue: 181/255, alpha: 1)


//<color name="swoosh_front">#F59D0F</color> // ORANGE COLOR
let SWOOSH_FRONT = UIColor(red: 245/255, green: 157/255, blue: 15/255, alpha: 1)


//<color name="swoosh_front_front">#FFEB89</color> // YELLOW
let SWOOSH_FRONT_FRONT = UIColor(red: 255/255, green: 235/255, blue: 137/255, alpha: 1)


//<color name="lightGreen">#017C8B</color>   // GREEN
let LIGHT_GREEN = UIColor(red: 1/255, green: 124/255, blue: 139/255, alpha: 1)


//<color name="swoosh_red">#8B0101</color>  // RED
let SWOOSH_RED = UIColor(red: 139/255, green: 1/255, blue: 1/255, alpha: 1)


//<color name="swoosh_red2">#C40101</color>  // RED
let SWOOSH_RED2 = UIColor(red: 196/255, green: 1/255, blue: 1/255, alpha: 1)


// <color name="chatcal1">#6AF59D0E</color>
// <color name="chatcal2">#581DA1F2</color>
// <color name="chatcal3">#FF93E4A5</color>

//hex color picker
