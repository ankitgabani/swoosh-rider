//
//  AppDelegate.swift
//  Swoosh Rider
//
//  Created by Gabani King on 31/03/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//
//com.swooshrider
//com.swooshrider.OneSignalNotificationServiceExtension
import UIKit
import IQKeyboardManager
import LGSideMenuController
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import OneSignal
import Stripe
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OSPermissionObserver, OSSubscriptionObserver {
    
    var window: UIWindow?
    var dicLoginUserDetails: UserLoginData?
    var dicMainData = NSDictionary()
    
    var objLatitude = 0.0
    var objLongitude = 0.0
    var objCurrentAddress = ""
    
    var objContry = ""
    var objState = ""
    var objCity = ""
    var objFullAddress = ""
    var strDeviceToken = ""
    
    var objStart_lang_Map = ""
    var objStart_lat_Map = ""
    
    var objEnd_lat_Map = ""
    var objEnd_lang_Map = ""
    
    var isViewMap = false
    
    var isSuccessWallet = false
    var isFromCareRideViewMenu = false
    var isFromCareRideViewMenu_BookingID = ""
    
    var isDriverOnTheWay = false
    var isDriverisArried = false
    
    var isYourTripStarted = false
    var isYourTripCompleted = false


    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch
        
        Stripe.setDefaultPublishableKey(STRIPE_KET)
        
        GMSServices.provideAPIKey(PROVIDE_API_KEY)
        
        GMSPlacesClient.provideAPIKey(PROVIDE_API_KEY)
        
        
        IQKeyboardManager.shared().isEnabled = true
        
        self.dicLoginUserDetails = getCurrentUserData()
        
        let isUserLogin = UserDefaults.standard.value(forKey: "isUserLogin") as? Bool
        
        if isUserLogin == true {
            setUpSideMenu()
        }
        
        self.setupPush()
        
        // For debugging
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            print("Received Notification: ", notification!.payload.notificationID!)
            print("launchURL: ", notification?.payload.launchURL ?? "No Launch Url")
            print("content_available = \(notification?.payload.contentAvailable ?? false)")
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload? = result?.notification.payload
            
            print("Message: ", payload!.body!)
            print("badge number: ", payload?.badge ?? 0)
            print("notification sound: ", payload?.sound ?? "No sound")
            
            if let additionalData = result!.notification.payload!.additionalData {
                print("additionalData: ", additionalData)
                
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: true, ]
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: OneSignalKey, handleNotificationReceived: notificationReceivedBlock, handleNotificationAction: notificationOpenedBlock, settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        // Add your AppDelegate as an obsserver
        OneSignal.add(self as OSPermissionObserver)
        
        OneSignal.add(self as OSSubscriptionObserver)
        
        return true
    }
    
    func setupPush()
    {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Handle user allowing / declining notification permission. Example:
            if (granted) {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            } else {
                print("User declined notification permissions")
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            // UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current()
            // center.delegate  = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
            }
        }
        else{
            
            let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
            
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    // Add this new method
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        
        // Example of detecting answering the permission prompt
        
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: ", stateChanges!)
    }
    
    // Output:
    /*
     Thanks for accepting notifications!
     PermissionStateChanges:
     Optional(<OSSubscriptionStateChanges:
     from: <OSPermissionState: hasPrompted: 0, status: NotDetermined>,
     to:   <OSPermissionState: hasPrompted: 1, status: Authorized>
     >
     */
    
    // TODO: update docs to change method name
    // Add this new method
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: ", stateChanges!)
        if let playerId = stateChanges.to.userId{
            print(playerId)
            
            strDeviceToken = playerId
            
            UserDefaults.standard.set(strDeviceToken, forKey: "PlayerID")
            UserDefaults.standard.synchronize()
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Notification Data \(userInfo)")
        
        if let dict = userInfo["aps"] as? NSDictionary {

            let bookingType = dict.value(forKey: "alert") as? String
            
            if bookingType == "Your driver has arrived"
            {
                isDriverOnTheWay = true
            }
            else if bookingType == "Driver is on the way for pickup"
            {
                isDriverisArried = true
            }
            else if bookingType == "Your Trip has been successfully  Started...."
            {
                isYourTripStarted = true
            }
            else if bookingType == "Your Instant Booking  has been  Successfully Completed." {
                isYourTripCompleted = true
            }
        }
        
        if let dict = userInfo["custom"] as? NSDictionary {
            
            if let dicObject = dict.value(forKey: "a") as? NSDictionary{
                
                let bookingType = dicObject.value(forKey: "booking_type") as? String
                
                if bookingType == "instant_booking"{
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setDriverDetails"), object: dicObject, userInfo: nil)
                }
            }
        }
        
        
        completionHandler(.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        
        print("Notification Data \(userInfo)")
        
        if let dict = userInfo["aps"] as? NSDictionary {

            let bookingType = dict.value(forKey: "alert") as? String
            
            if bookingType == "Your driver has arrived"
            {
                isDriverOnTheWay = true
            }
            else if bookingType == "Driver is on the way for pickup"
            {
                isDriverisArried = true
            }
            else if bookingType == "Your Trip has been successfully  Started...."
            {
                isYourTripStarted = true
            }
            else if bookingType == "Your Instant Booking  has been  Successfully Completed." {
                isYourTripCompleted = true
            }
        }
        
        if let dict = userInfo["custom"] as? NSDictionary {
            
            if let dicObject = dict.value(forKey: "a") as? NSDictionary{
                
                let bookingType = dicObject.value(forKey: "booking_type") as? String
                
                if bookingType == "instant_booking"{
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setDriverDetails"), object: dicObject, userInfo: nil)
                }
            }
        }
        
        completionHandler(.alert)
        
    }
    
    
    func setUpSideMenu()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        let sideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        
        let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: sideMenuVC, rightViewController: nil)
        controller.leftViewWidth = home.view.frame.width - 80
        
        homeNavigation.navigationBar.isHidden = true
        
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }
    
    func saveCurrentUserData(dic: UserLoginData)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "currentUserData")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> UserLoginData
    {
        if let data = UserDefaults.standard.object(forKey: "currentUserData"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! UserLoginData
        }
        return UserLoginData.init()
    }
}

struct ReversedGeoLocation {
    let name: String            // eg. Apple Inc.
    let streetNumber: String    // eg. 1
    let streetName: String      // eg. Infinite Loop
    let city: String            // eg. Cupertino
    let state: String           // eg. CA
    let zipCode: String         // eg. 95014
    let country: String         // eg. United States
    let isoCountryCode: String  // eg. US
    
    var formattedAddress: String {
        return """
        \(name), \(streetNumber) \(streetName), \(city), \(state) \(zipCode) \(country)
        """
    }
    
    // Handle optionals as needed
    init(with placemark: CLPlacemark) {
        self.name           = placemark.name ?? ""
        self.streetNumber   = placemark.subThoroughfare ?? ""
        self.streetName     = placemark.thoroughfare ?? ""
        self.city           = placemark.locality ?? ""
        self.state          = placemark.administrativeArea ?? ""
        self.zipCode        = placemark.postalCode ?? ""
        self.country        = placemark.country ?? ""
        self.isoCountryCode = placemark.isoCountryCode ?? ""
    }
}
