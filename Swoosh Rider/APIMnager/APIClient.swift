//
//  APIClient.swift
//  Luxury Library
//
//  Created by Gabani on 08/06/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
import UIKit

class APIClient: NSObject {
    
    class var sharedInstance: APIClient {
        
        struct Static {
            static let instance: APIClient = APIClient()
        }
        return Static.instance
    }
    
    var responseData: NSMutableData!
    
    func showLogoutAlert(completion:@escaping ((_ tapped:Bool)->Void))
    {
        
    }
    
    
    func MakeAPICallWithAuthHeaderPostCrate(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(IMG_BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
//            let user_token = UserDefaults.standard.value(forKey: "user_token") as? String
//
//            let authorization = "Bearer \(user_token!)"
            
            let headers: HTTPHeaders = ["Keydata" :KEY_DATA]

            
            AF.request(IMG_BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
            
                case .success:
                    if response.value != nil{
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    func MakeAPICallWithAuthHeaderPost(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
//            let user_token = UserDefaults.standard.value(forKey: "user_token") as? String
//
//            let authorization = "Bearer \(user_token!)"
            
            let headers: HTTPHeaders = ["Keydata" :KEY_DATA]

            
            AF.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
            
                case .success:
                    if response.value != nil{
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    func MakeAPICallWithoutAuthHeaderGet(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            var headers: HTTPHeaders = [:]
            if let user_token = UserDefaults.standard.value(forKey: "user_token") as? String {
                let authorization = "Bearer \(user_token)"
                headers = ["Authorization":authorization]
            }
            
            AF.request(BASE_URL + url, method: .get, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.value != nil{
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                case .failure:
                    print(response.error!)
                    completionHandler(nil, response.error, response.response?.statusCode)
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            //pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    func showIndicator(){
        SVProgressHUD.show()
    }
    
    func hideIndicator(){
        SVProgressHUD.dismiss()
    }
    
    func showSuccessIndicator(message: String){
        SVProgressHUD.showSuccess(withStatus: message)
    }
}

