//
//  Constants.swift
//  Swoosh Rider
//
//  Created by Gabani King on 10/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation

let BASE_URL = "https://swooshride.com/api/"

let IMG_BASE_URL = "https://swooshride.com/"

let KEY_DATA = "2554457717675"

// NEW
let PROVIDE_API_KEY = "AIzaSyDHTPV6rMVL5tVhstWL1PsAT_OTr0slzas"

let STRIPE_KET = "sk_test_51H8TGSCHNHyAh634owRCPM7wNNDc9H2E5LfU2RCQEhaYgxZYCo4AZecApWSxexMotz7KqpkpGSxHjxVs1gcx34Rt0027k0NXoe"

let OneSignalKey = "b610653d-f24a-4d56-a5f4-dab2e3dbc7d6"

// ********************** Login page && Sign Up page **********************
let LOG_IN = "login"

let GET_NEAREST_TIMEZONE_USER = "get_nearest_timezone_user/"

let GET_COUNTRY = "getallcountry"

let SIGNUP_USER = "signup_user"

let GET_STATES = "getallstates"

let GET_CITYS = "getallcitys"

let CHECHING_OTP = "checking_otp"

let CHANGE_PASSWORD = "change_password"

let FORGOT_PASSWORD = "forgot_password"

 let VALIDATE_NUMBER = "validate_number"


// **********************  Profile **********************
let USER_DETAILS = "user_details"

let UPDATE_USER = "update_user"

let CHANGE_PASSWORD_USER = "change_password_user"

let UPLOAD_IMAGE = "upload_image"


// **********************  Notification **********************
let NOTIFICATION_ = "notification"


// **********************  Add Athlete **********************
let CREATE_ATHLETE = "create_athlete"


// **********************  Athletes **********************
let MY_ATHLETE_LIST = "my_athlete_list"

let MY_ATHLETE_REMOVE = "my_athlete_remove"

let EDIT_ATHLETE = "edit_athlete"

let UPDATE_ATHLETE = "update_athlete"


// **********************  Join Team **********************
let SPORTS_TYPE = "sports_type"

let CLUB_LIST_USER = "club_list_user"

let TEAM_LIST_USER = "team_list_user"

 let ADD_TEAM_USER = "add_teams_user"


// ********************** Teams **********************
let TEAM_USER = "team_user"

let TEAM_REMOVE_USER = "team_remove_user"

let TEAM_MEMBER_LIST = "team_member_list"


// **********************  Calendar **********************
let TEAM_USER_CALENDER = "team_user_calender"

let EVENT_CALENDER = "event_calander"

 let NOTIFY_COACH = "notify_coach"


// **********************  Sos **********************
let SOS_RIDER = "sos_rider"


// **********************  Care Rides **********************
let CARE_BOOKING_CANCEL = "care_booking_cancel"

//let RESTART_CARE_BOOKING = "restart_care_booking"

let BOOKING_DATA = "booking_data"

//let USER_TRIP_DETAILS = "user_trip_details"


// **********************  Chat **********************
let ONE_TO_ONE_CHAT = "one_to_one_chat"

let TEAM_CHAT_CHECK = "team_chat_check"

let ONE_TO_ONE_CHAT_CARE_RIDER = "one_to_one_chat_care_rider"

let ONE_TO_ONE_CHAT_CARE_RIDER_COACH = "one_to_one_chat_care_rider_coach"

let USER_FEEDBACK = "user_feedback"


// **********************  Messages **********************

//let TEAM_CHAT = "team_chat_default"

let TEAM_CHAT = "team_chat"

let TEAM_USERS = "team_users"

let USER_MESSAGE_ALL = "user_message_all"




// **********************  Wallet **********************
let WALLET_BALANCE_SETTIGNS = "wallet_balance_settings"

let WALET_TRANSACTION_HISTORY = "wallet_transaction_history"

let STRIPE_RESPONSE = "stripe_response"

let STRIPE_CREATE = "stripe1/create.php"


// **********************  Trip History (care) **********************
let VIEW_CARE_BOOKING = "view_care_booking"

let USER_TRIP_DETAILS = "user_trip_details"


// **********************  Trip History (Instant) **********************
let USER_TRIP_INSTANT = "user_trip_instant"

let CANCEL_REQUEST_INSTANT = "cancelrequest_instant"

let START_OTP = "start_otp"



// **********************  Promotions **********************
let APPLY_COUPON = "apply_coupon"



// **********************  Invite **********************
let INVITE = "invite"



// **********************  Help & Feedback **********************
let CONTACT_INFO = "contact_info"

let SUBMIT_QUERY_USER = "submit_query_user"


// **********************  Book Care Ride **********************

let EVENT_USER = "event_user"

let VEHICLE_SEATS = "vehicle_seats"

let VIEW_DRIVER_LIST_EVENT = "view_driver_list_event"

let CARE_BOOKING = "care_booking"

// **********************  Instant Ride **********************

let WALLET_BALANCE = "wallet_balance"

let SELECT_CAR = "select_car"

let INSTANT_BOOKING = "instant_booking"

let WALLET_DISCOUNT = "wallet_discount"

let BALANCE_CHECK = "balance_check"

let INSTANT_BOOKING_SUBMIT = "instant_booking_submit"



let ADD_HOME_LOCATION = "add_home_location"

let VIEW_HOME_LOCATION = "view_home_location"

let ADD_LOCATION = "add_location"

let VIEW_LOCATION = "view_location"

let REMOVE_LOCATION = "remove_location"





var selectedTeamFrom = false

var isOpenSideMenu = false

let appDelagte = UIApplication.shared.delegate as! AppDelegate

var objInvite = ""
